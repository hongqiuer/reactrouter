/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import Reflux from 'reflux'
Reflux.defineReact(React);

import { findDOMNode } from 'react-dom'
import { Link } from 'react-router'

import { Button,Table } from 'react-bootstrap'
import CheckBox from '../componentUI/js/checkBox'
import PageArea from '../componentUI/js/pageArea'
import Tips from '../componentUI/js/tips'
import '../store/myApps'


class ListTd extends React.Component{
    constructor(props){
        super(props);
        this.state = {s:false,s0:false,s1:false,secretKey:""};
        this.handleClick = this.handleClick.bind(this);
        this.showOrHidApiKey = this.showOrHidApiKey.bind(this);
        this.showOrHidSecretKey = this.showOrHidSecretKey.bind(this);
        this.del = this.del.bind(this);
        this.setMessageInfo = this.setMessageInfo.bind(this);
        this.reSecretKey = this.reSecretKey.bind(this);
    }
    componentDidMount(){
        if(this.props.showKeyName === "parms"){
            this.setState({secretKey:this.props.data.secretKey});
        }
    }
    componentWillReceiveProps(newProps){
        if(newProps.isInverse===2&&this.props.isInverse!=2){
            this.setState({s:false,s0:false,s1:false});
        }else if(newProps.isInverse!=2&&newProps.isInverse!=this.props.isInverse&&newProps.type===0&&newProps.showKeyName === "delCheck"){
            this.handleClick();
        }else if(newProps.data!=this.props.data){
            this.setState({s:false,s0:false,s1:false});
        }else if(newProps.reSecretKey!=""){
            this.setState({secretKey:newProps.reSecretKey});
        }
    }
    handleClick(){
        if(this.state.s){
            window.GLOBAL.action.myApps.del(this.props.data);
            this.setState({s:false});
        }else{
            window.GLOBAL.action.myApps.add(this.props.data);
            this.setState({s:true});
        }
    }
    showOrHidApiKey(){
        if(this.state.s0){
            this.setState({s0:false});
        }else{
            this.setState({s0:true});
        }
    }
    showOrHidSecretKey(){
        if(this.state.s1){
            this.setState({s1:false});
        }else{
            this.setState({s1:true});
        }
    }
    setMessageInfo(){
        window.GLOBAL.action.myApps.saveInfo();
    }
    reSecretKey(){
        window.GLOBAL.action.myApps.showTips(1,this.props.appId);
    }
    del(){
        window.GLOBAL.action.myApps.showTips(2,this.props.appId);
    }
    render(){
        let myApps = window.GLOBAL.pageData.myApps;
        return(
            <td className={this.props.className}>
                {this.props.showKeyName === "title"?(
                    <Link className="link" title={this.props.data.text} to={"/myAppsDetail"+this.props.data.url} onClick={this.setMessageInfo}>
                        <div className={this.props.type===1?"ShowTxt short":"ShowTxt"}>{this.props.data.text}</div>
                        {this.props.type===1?(
                            <div className="icon">
                                <div className="iconBg"></div>
                                <div className="iconText">试用</div>
                            </div>
                        ):""}
                    </Link>
                ):(
                    this.props.showKeyName === "delCheck"?(
                        this.props.type===0?(
                            <CheckBox name="delCheck" s={this.state.s} v={this.state.s?this.props.data:""} onClick={this.handleClick}/>
                        ):(
                            <CheckBox name="delCheck" s={this.state.s} v={this.state.s?this.props.data:""} disabled/>
                        )
                    ):(
                        this.props.showKeyName === "parms"?(
                            <div className="lineBox">
                                <div className="line">
                                    <div className="t1 fl">API Key: </div>
                                    <div className="t2 fl">{this.state.s0?this.props.data.apiKey:myApps.hid}</div>
                                    <div className={this.state.s0?"icon-eye fl":"icon-eye-close fl"} onClick={this.showOrHidApiKey}></div>
                                </div>
                                <div className="line">
                                    <div className="t1 fl">Secret Key: </div>
                                    <div className="t2 fl">{this.state.s1?this.state.secretKey:myApps.hid}</div>
                                    <div className={this.state.s1?"icon-eye fl":"icon-eye-close fl"} onClick={this.showOrHidSecretKey}></div>
                                    {this.props.type===0&&this.props.status===0?(
                                        <div className="icon-reApiKey fl" onClick={this.reSecretKey}></div>
                                    ):""}
                                </div>
                            </div>
                        ):(
                            this.props.showKeyName === "btns"?(
                                this.props.data.map((item,i) => (
                                    item.type===1?(
                                        <Link className="green" key={i} onClick={this.del}>{item.text}</Link>
                                    ):(
                                        <Link className="green" to={item.url} key={i}>{item.text}</Link>
                                    )
                                ))
                            ):(
                                this.props.showKeyName === "status"?(
                                    myApps.status[this.props.data]
                                ):(
                                    this.props.showKeyName === "date"?(
                                        <div className="blockTxt">{this.props.data}</div>
                                    ):(
                                        this.props.data
                                    )
                                )
                            )

                        )
                    )
                )}
            </td>
        )
    }
}
class ListTr extends React.Component{
    render(){
        return(
            <tr className={this.props.className}>
                {this.props.showKey.map((item,i) =>(
                    <ListTd key={i} data={this.props.data[item]} type={this.props.data['type']} status={this.props.data['status']} appId={this.props.data['delCheck']} showKeyName={item} className={"th"+i} isInverse={this.props.isInverse} reSecretKey={item=="parms"?this.props.reSecretKey:""}></ListTd>
                ))}
            </tr>
        )
    }
}
class DelMyApps extends React.Component{
    constructor(props){
        super(props);
        this.state = {s:0};
        this.Inverse = this.Inverse.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.data!=this.props.data){
            this.setState({s:2});
        }
    }
    Inverse(){
        if(this.state.s === 1){
            this.setState({s:0});
        }else{
            this.setState({s:1});
        }
    }
    render() {
        let data = [];
        if(this.props.data.length){
            this.props.data.map((element,index) => {
                data.push(<ListTr data={element} key={index} showKey={this.props.showKey} className={index%2===0?"bgGray":""} isInverse={this.state.s} reSecretKey={element.delCheck===this.props.showId?this.props.reSecretKey:""}></ListTr>)
            })
        }else{
            data.push(<tr key={0}><td className="noData" colSpan={this.props.title.length}>{this.props.noDataTips}</td></tr>);
        }
        return (
            <div className="delList">
                <Table striped hover responsive className="basicList">
                    <thead>
                    <tr>
                        {this.props.title.map((item,i) => (
                            item === "delAllCheck"?(
                                <th className={"th"+i} key={i}><CheckBox name="delAllCheck" s={this.state.s===1?true:false} onClick={this.Inverse}/></th>
                            ):(
                                <th className={"th"+i} key={i}>{item}</th>
                            )
                        ))}
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.data.length?(
                        this.props.data.map((element,index) => (
                            <ListTr data={element} key={index} showKey={this.props.showKey} className={index%2===0?"bgGray":""} isInverse={this.state.s} reSecretKey={element.delCheck===this.props.showId?this.props.reSecretKey:""}></ListTr>
                        ))
                    ):(
                        <tr><td className="noData" colSpan={this.props.title.length}>{this.props.noDataTips}</td></tr>
                    )}
                    </tbody>
                </Table>
            </div>
        )
    }
}
DelMyApps.propTypes = {
    title: React.PropTypes.array.isRequired,
    showKey: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired,
    noDataTips: React.PropTypes.string.isRequired
};
DelMyApps.defaultProps = {
    title:[],
    showKey:[],
    data:[],
    noDataTips:"暂无数据"
};

class myApps extends Reflux.Component{
    constructor(props) {
        super(props);
        this.state = {
            data:this.props.data,
            reSecretKey:""
        };
        this.store = window.GLOBAL.store.myApps;
        this.handleSelect = this.handleSelect.bind(this);
        this.handleDelAll = this.handleDelAll.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleShowTips = this.handleShowTips.bind(this);
        this.changeSecretKey =this.changeSecretKey.bind(this);
        this.setMessageInfo =this.setMessageInfo.bind(this);
    }
    componentDidMount(){
        window.GLOBAL.action.myApps.init();
        let parms = {pageNow:this.state.pageNow, token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("myApps",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("myApps");
        ajax.success=function(r){
            this.setState({totalPage:r.totalPage,data:r.data});
        }.bind(this);
        $.ajax(ajax);
    }
    handleSelect(pageNow){
        let parms = {pageNow:pageNow, token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("myApps",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("myApps");
        ajax.success=function(r){
            this.setState({pageNow:pageNow,totalPage:r.totalPage,data:r.data});
            window.GLOBAL.action.myApps.init();
        }.bind(this);
        $.ajax(ajax);
    }
    handleDelAll(){
        let parms = {delId:JSON.stringify(this.state.delId), token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("delMyApps",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("delMyApps");
        ajax.success=function(r){
            if(r.code){
                this.handleSelect(this.state.pageNow);
            }
        }.bind(this);
        $.ajax(ajax);
    }
    handleCancel(){
        this.setState({showTips:0});
    }
    handleShowTips(){
        this.setState({showTips:2});
    }
    changeSecretKey(){
        let parms = {appId:this.state.showId,token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("reSecretKey",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("reSecretKey");
        ajax.success=function(r){
            if(r.code){
                this.setState({reSecretKey:r.secretKey,showTips:0});
            }
        }.bind(this);
        $.ajax(ajax);
    }
    setMessageInfo(){
        window.GLOBAL.action.myApps.saveInfo();
    }
    render() {
        let myApps = window.GLOBAL.pageData.myApps;
        let dom;
        if(!this.props.children){
            let tips;
            if(this.state.showTips===1){
                tips = (<Tips className={myApps.reSecretKey.className} onSubmit={this.changeSecretKey} onCancel={this.handleCancel} text={myApps.reSecretKey.text}/>)
            }else if(this.state.showTips===2){
                tips = (<Tips className={myApps.delAll.className} onSubmit={this.handleDelAll} onCancel={this.handleCancel} text={myApps.delAll.text}/>)
            }
            dom =(<div className="myApps">
                <div className="MainTitle">
                    <h1>{myApps.title}</h1>
                    <Link to="/myAppsAdd" role="button" className="btn btn-success" onClick={this.setMessageInfo}>新建应用</Link>
                </div>
                <div className="whiteSpace">
                    <DelMyApps
                        title={this.props.title}
                        data={this.state.data}
                        showKey={this.props.showKey}
                        showId={this.state.showId}
                        reSecretKey={this.state.reSecretKey}
                    />
                    <div>
                        {this.state.showDel?(<Button className="DelAllBtn" onClick={this.handleShowTips}>批量删除</Button>):""}
                        <PageArea
                            totalPage={this.state.totalPage}
                            maxPage={3} pageNow={this.state.pageNow}
                            onSelect={this.handleSelect}
                            onSearch={this.handleSelect}
                        />
                    </div>
                </div>
                {tips}
            </div>);
        }
        return (
            <div>
                {dom}
                {this.props.children}
            </div>
        )
    }
}
myApps.propTypes = {
    title: React.PropTypes.array.isRequired,
    showKey: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired
};
myApps.defaultProps = {
    title:window.GLOBAL.pageData.myApps.listTitle,
    showKey:window.GLOBAL.pageData.myApps.showKey,
    data:[]
};
module.exports = myApps;