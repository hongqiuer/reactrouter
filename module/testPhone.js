/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import { Button } from 'react-bootstrap'
import Input from '../componentUI/js/Input'
import CheckTips from '../componentUI/js/checkTips'
import Tips from '../componentUI/js/tips'

class TestPhone extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            showTips:-1,
            delPhone:""
        };
        this._getInfo = this._getInfo.bind(this);
        this.checkNewPhone = this.checkNewPhone.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.showCheckTips = this.showCheckTips.bind(this);
        this.delTestPhone = this.delTestPhone.bind(this);
    }
    componentDidMount(){
        this._getInfo();
    }
    _getInfo(){
        window.GLOBAL.ajaxMap.setParm("testPhone",'token',window.GLOBAL.token);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("testPhone");
        ajax.success=function(r){
            this.setState({
                data:r.data,
                showTips:-1,
                delPhone:""
            });
        }.bind(this);
        $.ajax(ajax);
    }
    handleCancel(){
        this.setState({showTips:-1});
    }
    checkNewPhone(_obj){
        let parms = {phone:_obj.state.phone,key:_obj.state.key,token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("addTestPhone",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("addTestPhone");
        ajax.success=function(r){
            if(r.code){
                this.handleCancel();
                this._getInfo();
            }
        }.bind(this);
        $.ajax(ajax);
    }
    showCheckTips(){
        if(this.state.data.length === window.GLOBAL.pageData.testPhone.maxList){
            this.setState({showTips:2})
        }else{
            this.setState({showTips:0})
        }
    }
    delTestPhone(){
        let parms = {phone:this.state.delPhone,token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("delTestPhone",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("delTestPhone");
        ajax.success=function(r){
            if(r.code){
                this._getInfo();
            }
        }.bind(this);
        $.ajax(ajax);
    }
    selectDelPhone(_phone){
        this.setState({showTips:1,delPhone:_phone});
    }
    render() {
        let testPhone = window.GLOBAL.pageData.testPhone;
        let tips;
        if(this.state.showTips===0){
            tips = (<CheckTips onSubmit={this.checkNewPhone} onCancel={this.handleCancel} title={testPhone.checkTips.title} btns={testPhone.checkTips.btns}/>)
        }else if(this.state.showTips===1){
            tips = (<Tips {...testPhone.delPhone} onSubmit={this.delTestPhone} onCancel={this.handleCancel}/>)
        }else if(this.state.showTips===2){
            tips = (<Tips {...testPhone.unAdd} onCancel={this.handleCancel}/>)
        }
        return (
            <div className="testPhone">
                <div className="MainTitle">
                    <h1>{testPhone.title}</h1>
                </div>
                <div className="whiteSpace">
                    <Button bsStyle="primary" onClick={this.showCheckTips}>添加测试号码</Button>
                    <ul className="testPhoneBox">
                        {this.state.data.map((item,i) =>(
                            <li className={"item li"+i} key={i}>
                                <div className="index fl">{i+1}</div>
                                <Input readOnly disabled className={item.type===0?"disabled phone fl":"phone fl"} type="naturalNum" v={item.value} maxLength={testPhone.maxLength}/>
                                {item.type===0?(
                                    <div className="icon fl">
                                        <div className="iconBg"></div>
                                        <div className="iconText">注册</div>
                                    </div>
                                ):(
                                    <Button className="fl" bsStyle="info" onClick={this.selectDelPhone.bind(this,item.value)}>删除</Button>
                                )}
                            </li>
                        ))}
                    </ul>
                </div>
                {tips}
            </div>
        )
    }
}
module.exports = TestPhone;