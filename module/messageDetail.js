/**
 * Created by hqer on 2016/12/26.
 */
import React from 'react'
import { Link,browserHistory } from 'react-router'
import { Button } from 'react-bootstrap'

class MessageDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {addressee:"",date:"",title:"",text:"",preId:"",nextId:""};
        this.goToBack = this.goToBack.bind(this);
        this.getInfo = this.getInfo.bind(this);
        this.readMessage = this.readMessage.bind(this);
        this.del = this.del.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.params.messageId!=this.props.params.messageId){
            this.readMessage(this.props.params.messageId);
            this.getInfo(newProps.params.messageId);
        }
    }
    goToBack(){
        browserHistory.goBack();
    }
    componentDidMount(){
        this.readMessage(this.props.params.messageId);
        this.getInfo(this.props.params.messageId);
    }
    getInfo(_id){
        window.GLOBAL.ajaxMap.setParm("messageDetail",'messageId',_id);
        window.GLOBAL.ajaxMap.setParm("messageDetail",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("messageDetail");
        parms.success=function(r){
            this.setState({
                addressee:r.addressee,
                date:r.date,
                title:r.title,
                text:r.title,
                preId:r.preId,
                nextId:r.nextId
            });
        }.bind(this);
        $.ajax(parms);
    }
    readMessage(_id){
        window.GLOBAL.ajaxMap.setParm("readMessage",'messageId',_id);
        window.GLOBAL.ajaxMap.setParm("readMessage",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("readMessage");
        parms.success=function(r){
            if(r.code){
                window.GLOBAL.userInfo.get(function(){
                    let unRead = JSON.parse(sessionStorage['userInfo']).unRead;
                    window.GLOBAL.action.header.setUnRead(unRead);
                    window.GLOBAL.action.workBench.setUnRead(unRead);
                }.bind(this));
            }
        }.bind(this);
        $.ajax(parms);
    }
    del(){
        window.GLOBAL.ajaxMap.setParm("messageDel",'delId',[this.props.params.messageId]);
        window.GLOBAL.ajaxMap.setParm("messageDel",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("messageDel");
        parms.success=function(r){
            if(r.code){
                this.goToBack();
            }
        }.bind(this);
        $.ajax(parms);
    }
    render() {
        let preBtn;
        if(this.state.preId){
            preBtn = (<Link to={"/messageDetail/"+this.state.preId}><Button className="preBtn">上一页</Button></Link>);
        }
        let nextBtn;
        if(this.state.nextId){
            nextBtn = (<Link to={"/messageDetail/"+this.state.nextId}><Button className="preBtn">下一页</Button></Link>);
        }
        return (
            <div className="messageDetail">
                <div className="MainTitle">
                    <h1>{window.GLOBAL.pageData.message.title}</h1>
                    <Button className="backBtn" onClick={this.goToBack}>返回</Button>
                </div>
                <div className="whiteSpace">
                    <img src={window.GLOBAL.pageData.message.img} />
                    <div className="box">
                        <div>收件人: {this.state.addressee}</div>
                        <div>时间: {this.state.date}</div>
                    </div>
                    <div className="title">{this.state.title}</div>
                    <div className="content">{this.state.text}</div>
                    <Button className="delBtn" onClick={this.del}>删除</Button>
                    {preBtn}
                    {nextBtn}
                </div>
            </div>
        )
    }
}
module.exports = MessageDetail;