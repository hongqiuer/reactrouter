/**
 * Created by hqer on 2017/2/22.
 */
import React from 'react'
import { Link } from 'react-router'

class statisticsNoData extends React.Component{
    render() {
        let statistics = window.GLOBAL.pageData.statistics;
        return (
            <div className="statisticsNoData">
                <div className="MainTitle">
                    <h1>
                        {statistics.title}
                    </h1>
                </div>
                <div className="whiteSpace">
                    <div className="midArea">
                        <div style={{color:"red"}}>暂无数据</div>
                        <div>请 <Link className="link" to="/myServer">点击这里</Link> 试用或开通该能力</div>
                    </div>
                </div>
            </div>
        )
    }
}
module.exports = statisticsNoData;