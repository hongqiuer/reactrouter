/**
 * Created by hqer on 2016/12/8.
 */
import React from 'react'
import { Link,browserHistory } from 'react-router'
import { Table,Button } from 'react-bootstrap'
import Input from '../componentUI/js/Input'
import Tips from '../componentUI/js/tips'
import MD5 from "md5"

class order extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            totalNorms:0,
            totalCost:0,
            unPayOrder:0,
            showTips:false
        };
        this.handleCheckBox=this.handleCheckBox.bind(this);
        this._setInputVal = this._setInputVal.bind(this);
        this.del = this.del.bind(this);
        this.add = this.add.bind(this);
        this.submit =this.submit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    componentDidMount(){
        window.GLOBAL.ajaxMap.setParm("getUnPayOrder",'r',Math.random());
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("getUnPayOrder");
        parms.success=function(r){
            this.setState({unPayOrder:r.num});
            let parms1 = {
                eaId:this.props.location.query.eaId,
                token:window.GLOBAL.token
            };
            window.GLOBAL.ajaxMap.setParms("getOrderPackage",parms1);
            let ajax1 = window.GLOBAL.ajaxMap.getAjaxParms("getOrderPackage");
            ajax1.success=function(r){
                let list = r.data;
                list.map((item,i) =>(item.num = 0));
                this.setState({
                    data:list
                });
            }.bind(this);
            $.ajax(ajax1);
        }.bind(this);
        $.ajax(parms);
    }
    handleCheckBox(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        let newData = Object.assign([],this.state.data);
        let _cost = 0;
        let _norms = 0;
        newData.find(function(value, index, arr) {
            if(value.packageId===_val){
                if(value.num === 0){
                    value.num = 1;
                }else{
                    value.num = 0;
                }
            }
            _norms = _norms + (parseInt(value.norms)*100 * value.num)/100;
            _cost = _cost + (parseFloat(value.price)*100 * value.num)/100;
        });
        this.setState({
            data:newData,
            totalNorms:_norms,
            totalCost:_cost
        });
    }
    _setInputVal(_obj){
        let _val = _obj.state.v===""?0:_obj.state.v;
        let _id = _obj.props.title;
        let _cost = 0;
        let _norms = 0;
        let newData = Object.assign([],this.state.data);
        newData.find(function(value, index, arr) {
            if(value.packageId===_id){
                value.num = _val;
            }
            _norms = _norms + (parseInt(value.norms)*100 * value.num)/100;
            _cost = _cost + (parseFloat(value.price)*100 * value.num)/100;
        });
        this.setState({
            data:newData,
            totalNorms:_norms,
            totalCost:_cost
        });
    }
    del(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.title.split("_");
        if(_val[1]*1!=0){
            let _cost = 0;
            let _norms = 0;
            let newData = Object.assign([],this.state.data);
            newData.find(function(value, index, arr) {
                if(value.packageId===_val[0]){
                    value.num = _val[1]*1-1;
                }
                _norms = _norms + (parseInt(value.norms)*100 * value.num)/100;
                _cost = _cost + (parseFloat(value.price)*100 * value.num)/100;
            });
            this.setState({
                data:newData,
                totalNorms:_norms,
                totalCost:_cost
            });
        }
    }
    add(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.title.split("_");
        if(_val[1]*1!=0){
            let _cost = 0;
            let _norms = 0;
            let newData = Object.assign([],this.state.data);
            newData.find(function(value, index, arr) {
                if(value.packageId===_val[0]){
                    value.num = _val[1]*1+1;
                }
                _norms = _norms + (parseInt(value.norms)*100 * value.num)/100;
                _cost = _cost + (parseFloat(value.price)*100 * value.num)/100;
            });
            this.setState({
                data:newData,
                totalNorms:_norms,
                totalCost:_cost
            });
        }
    }
    submit(){
        if(this.state.data.find((n) => (n.num > 0))){
            let parms = {
                eaId:this.props.location.query.eaId,
                data:JSON.stringify(this.state.data),
                token:window.GLOBAL.token,
                key1:MD5(this.state.eaId+JSON.stringify(this.state.data)+window.GLOBAL.token+"devPortal")
            };
            window.GLOBAL.ajaxMap.setParms("postOrder",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("postOrder");
            ajax.success=function(r){
                if(r.code){
                    browserHistory.push("/orderToPay/"+r.orderId);
                }
            }.bind(this);
            $.ajax(ajax);
        }else{
            this.setState({showTips:true})
        }
    }
    handleCancel(){
        this.setState({showTips:false})
    }
    render() {
        let orderNew = window.GLOBAL.pageData.orderNew;
        return (
            <div className="order">
                <div className="whiteSpace">
                    <ul className="bigCircleProgress">
                        <li className="bigCircle select">
                            <div className="text">选择套餐</div>
                            <div className="num">1</div>
                        </li>
                        <li className="bigCircle">
                            <div className="text">确认支付</div>
                            <div className="num">2</div>
                        </li>
                        <li className="bigCircle">
                            <div className="text">支付成功</div>
                            <div className="num">3</div>
                        </li>
                    </ul>
                    {this.state.unPayOrder?<div className="orderTips">
                        <div className="icon-orderTips"></div>
                        <div className="text">您有 <font color="red">{this.state.unPayOrder}</font> 个未支付订单,请在 <Link to="/orderManage?status=1">订单管理</Link> 中完成支付或关闭该订单。</div>
                    </div>:""}
                    <div className="title">套餐选择</div>
                    <Table striped hover responsive className="basicList" style={{borderTop:"1px solid #ddd"}}>
                        <thead>
                            <tr>
                                {orderNew.listTitle.map((item,i) => (
                                    <th className={"th"+i} key={i}>{item}</th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.data.map((element,index) => (
                            <tr className={index%2===0?"bgGray":""} key={index}>
                                {orderNew.showKey.map((item,i) => (
                                    <td key={i} className={"th"+i}>
                                        {item!=="packageId"&&item!=="num"?(
                                            <div className="ShowTxt">{element[item]}</div>
                                        ):(item==="packageId"?(
                                            <input
                                                type="checkbox"
                                                className={element["num"]!==0?true:false?"checkBoxBtn s1":"checkBoxBtn"}
                                                checked={element["num"]!==0?true:false}
                                                value={element[item]}
                                                onClick={this.handleCheckBox}
                                            />
                                        ):(
                                            <div className="ShowTxt">
                                                <span className="fl glyphicon glyphicon-minus" title={element["packageId"]+"_"+element[item]} onClick={this.del}></span>
                                                <Input
                                                    className="numInput fl"
                                                    title={element["packageId"]}
                                                    type="positiveInt"
                                                    v={element[item]===0?"":element[item]}
                                                    onBlur={this._setInputVal}
                                                    maxValue={orderNew.numInput.maxValue}
                                                    maxLength={orderNew.numInput.maxLength}
                                                    disabled={element[item]===0?true:false}
                                                />
                                                <span className="fl glyphicon glyphicon-plus"  title={element["packageId"]+"_"+element[item]} onClick={this.add}></span>
                                            </div>
                                        ))}
                                    </td>
                                ))}
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                    <div className="inputLine">
                        <div className="fr">总计: {this.state.totalNorms}条 {this.state.totalCost}元</div>
                    </div>
                    <div className="inputLine">
                        <Button onClick={this.submit}>下一步</Button>
                    </div>
                </div>
                {this.state.showTips?<Tips {...orderNew.tips} onCancel={this.handleCancel}/>:""}
            </div>
        )
    }
}
module.exports = order;