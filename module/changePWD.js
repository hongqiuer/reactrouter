/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import { Button } from 'react-bootstrap'
import Tips from '../componentUI/js/tips'
import MD5 from "md5"

class changePWD extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            oldPWD:"",
            newPWD:"",
            reNewPWD:"",
            security:0,
            showTips:0
        };
        this.setOldPWD = this.setOldPWD.bind(this);
        this.checkOldPWD = this.checkOldPWD.bind(this);
        this.setNewPWD = this.setNewPWD.bind(this);
        this.checkNewPWD = this.checkNewPWD.bind(this);
        this.setReNewPWD = this.setReNewPWD.bind(this);
        this.checkReNewPWD = this.checkReNewPWD.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.submit = this.submit.bind(this);
        this._getKey = this._getKey.bind(this);
    }
    componentWillMount(){
        if(!window.GLOBAL.promise["getKey"]){
            window.GLOBAL.promise["getKey"] = $.Deferred();
        }
        if(!window.GLOBAL.promise["checkPWD"]){
            window.GLOBAL.promise["checkPWD"] = $.Deferred();
        }
    }
    componentWillUnmount(){
        if(window.GLOBAL.promise["getKey"]){
            delete window.GLOBAL.promise["getKey"];
        }
        if(window.GLOBAL.promise["checkPWD"]){
            delete window.GLOBAL.promise["checkPWD"];
        }
    }
    _getKey(){
        window.GLOBAL.ajaxMap.setParm("getRsaKey","token",window.GLOBAL.token);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("getRsaKey");
        ajax.success=function(r){
            window.GLOBAL.promise["getKey"].resolve(r);
        }.bind(this);
        $.ajax(ajax);
    }
    setOldPWD(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({oldPWD:_val});
    }
    checkOldPWD(){
        let bool = window.GLOBAL.checkFun.isPassWord($(".changePWD .oldPWD"),$(".changePWD .oldPWD").siblings(".errorTips"),"* 密码输入错误!");
        if(bool){
            this._getKey();
            $.when(window.GLOBAL.promise["getKey"]).done(function(r){
                if(r.key){
                    setMaxDigits(130);
                    let key = new RSAKeyPair("10001","",r.key);
                    let parms = {
                        oldPassWord:encryptedString(key,encodeURIComponent(this.state.oldPWD)),
                        token:window.GLOBAL.token,
                        key1:MD5(this.state.oldPWD+"devPortal")
                    };
                    window.GLOBAL.ajaxMap.setParms("checkPassWord",parms);
                    let ajax = window.GLOBAL.ajaxMap.getAjaxParms("checkPassWord");
                    ajax.success=function(r1){
                        window.GLOBAL.promise["checkPWD"].resolve(r1);
                        if(r1.code){
                            $(".changePWD .oldPWD").siblings(".errorTips").html("");
                        }else{
                            this.setState({oldPWD:""});
                            $(".changePWD .oldPWD").siblings(".errorTips").html("* 密码输入错误!");
                        }
                    }.bind(this);
                    $.ajax(ajax);
                }
            }.bind(this));
        }
    }
    setNewPWD(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        let _security = this.state.security;
        if(_val===""){
            _security = 0;
        }else if(/^[0-9]+$/.test(_val)||/^[a-zA-Z]+$/.test(_val)||/^[$#@^&_=+%<>{}?~!]+$/.test(_val)){
            _security = 1;
        }else if(/^[0-9a-zA-Z]+$/.test(_val)||/^[a-zA-Z$#@^&_=+%<>{}?~!]+$/.test(_val)||/^[0-9$#@^&_=+%<>{}?~!]+$/.test(_val)){
            _security = 2;
        }else if(/^[0-9a-zA-Z$#@^&_=+%<>{}?~!]+$/.test(_val)){
            _security = 3;
        }
        this.setState({newPWD:_val,security:_security});
    }
    checkNewPWD(){
        window.GLOBAL.checkFun.isPassWord($(".changePWD .newPWD"),$(".changePWD .newPWD").siblings(".errorTips"),"* 密码输入错误!");
    }
    setReNewPWD(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({reNewPWD:_val});
    }
    checkReNewPWD(){
        window.GLOBAL.checkFun.isVerifyPassWord($(".changePWD .reNewPWD"),$(".changePWD .reNewPWD").siblings(".errorTips"),$(".changePWD .newPWD"));
    }
    handleCancel(){
        this.setState({showTips:0});
    }
    submit(){
        let isOk = true;
        if(!window.GLOBAL.checkFun.isPassWord($(".changePWD .reNewPWD"),$(".changePWD .reNewPWD").siblings(".errorTips"),"* 密码输入错误!")){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isPassWord($(".changePWD .newPWD"),$(".changePWD .newPWD").siblings(".errorTips"),"* 密码输入错误!")){
            isOk = false;
        }
        if(isOk){
            $.when(window.GLOBAL.promise["checkPWD"]).done(function(r){
                if(r.code){
                    this._getKey();
                    $.when(window.GLOBAL.promise["getKey"]).done(function(r1){
                        if(r1.key){
                            setMaxDigits(130);
                            let key = new RSAKeyPair("10001","",r1.key);
                            let parms = {
                                oldPassWord:encryptedString(key,encodeURIComponent(this.state.oldPWD)),
                                newPassWord:encryptedString(key,encodeURIComponent(this.state.newPWD)),
                                token:window.GLOBAL.token,
                                key1:MD5(this.state.oldPWD+this.state.newPWD+"devPortal")
                            };
                            window.GLOBAL.ajaxMap.setParms("editPassWord",parms);
                            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("editPassWord");
                            ajax.success=function(r2){
                                if(r2.code){
                                    this.setState({showTips:1,oldPWD:"",newPWD:"",reNewPWD:"",security:0});
                                }else{
                                    this.setState({showTips:2,oldPWD:"",newPWD:"",reNewPWD:"",security:0});
                                }
                            }.bind(this);
                            $.ajax(ajax);
                        }
                    }.bind(this))
                }
            }.bind(this))
        }
    }
    render() {
        let changePWD = window.GLOBAL.pageData.changePWD;
        let security="";
        if(this.state.security!=0){
            let level = "";
            if(this.state.security===1){
                level = "弱";
            }else if(this.state.security===2){
                level = "中";
            }else if(this.state.security===3){
                level = "强";
            }
            security = (
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">密码强度:</div>
                    <div className="inputArea">
                        <div className={"level level"+this.state.security}>
                            <div className="level1"></div>
                            <div className="level2"></div>
                            <div className="level3"></div>
                        </div>
                        <div className="des">{level}</div>
                    </div>
                </div>
            )
        }
        let tips;
        if(this.state.showTips===1){
            tips = (<Tips className={changePWD.success.className} onCancel={this.handleCancel} text={changePWD.success.text} btns={changePWD.success.btns}/>)
        }else if(this.state.showTips===2){
            tips = (<Tips className={changePWD.failed.className} onCancel={this.handleCancel} text={changePWD.failed.text} btns={changePWD.failed.btns}/>)
        }
        return (
            <div className="changePWD">
                <div className="MainTitle">
                    <h1>{changePWD.title}</h1>
                </div>
                <div className="whiteSpace">
                    <div className="box">
                        <div className="inputLine fl" style={{width:"100%"}}>
                            <div className="fl">原密码:</div>
                            <input type="password" className="fl oldPWD" value={this.state.oldPWD} onChange={this.setOldPWD} onBlur={this.checkOldPWD} maxLength={changePWD.oldPWD.maxLength}/>
                            <div className="errorTips"></div>
                        </div>
                        <div className="inputLine fl" style={{width:"100%"}}>
                            <div className="fl">新密码:</div>
                            <input type="password" className="fl newPWD" value={this.state.newPWD} onChange={this.setNewPWD} onBlur={this.checkNewPWD} maxLength={changePWD.newPWD.maxLength}/>
                            <div className="errorTips"></div>
                        </div>
                        {security}
                        <div className="inputLine fl" style={{width:"100%"}}>
                            <div className="fl">再次输入密码:</div>
                            <input type="password" className="fl reNewPWD" value={this.state.reNewPWD} onChange={this.setReNewPWD} onBlur={this.checkReNewPWD} maxLength={changePWD.reNewPWD.maxLength}/>
                            <div className="errorTips"></div>
                        </div>
                        <div className="inputLine fl" style={{width:"100%"}}>
                            <div className="fl"></div>
                            <Button onClick={this.submit}>确定</Button>
                        </div>
                    </div>
                </div>
                {tips}
            </div>
        )
    }
}

module.exports = changePWD;