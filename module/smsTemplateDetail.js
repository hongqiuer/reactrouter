/**
 * Created by hqer on 2017/2/4.
 */
import React from 'react'
import { browserHistory } from 'react-router'
import { Button,Row,Col } from 'react-bootstrap'

class smsTemplateDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name:"",
            type:"",
            status:"",
            statusDes:"",
            time1:"",
            time2:"",
            content:"",
            parms:[],
            scene:""
        };
        this.getInfo = this.getInfo.bind(this);
        this.goToBack = this.goToBack.bind(this);
    }
    componentDidMount(){
        this.getInfo(this.props.params.smsTemplateId);
    }
    goToBack(){
        browserHistory.goBack()
    }
    getInfo(_id){
        window.GLOBAL.ajaxMap.setParm("smsTemplateDetail",'smsTemplateId',_id);
        window.GLOBAL.ajaxMap.setParm("smsTemplateDetail",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("smsTemplateDetail");
        parms.success=function(r){
            this.setState({
                name:r.name,
                type:r.type,
                status:r.status,
                statusDes:r.statusDes,
                time1:r.time1,
                time2:r.time2,
                content:r.content,
                parms:r.parms,
                scene:r.scene
            });
        }.bind(this);
        $.ajax(parms);
    }
    render() {
        let smsTemplateDetail = window.GLOBAL.pageData.smsTemplate.smsTemplateDetail;
        return (
            <div className="smsTemplateDetail">
                <div className="MainTitle">
                    <h1>{smsTemplateDetail.title}</h1>
                    <Button className="backBtn" onClick={this.goToBack}>返回</Button>
                </div>
                <div className="whiteSpace">
                    <Row>
                        <Col xs={4}>
                            短信模板ID：
                            <span className="color666">{this.props.params.smsTemplateId}</span>
                        </Col>
                        <Col xs={4}>
                            模板名称：
                            <span className="color666">{this.state.name}</span>
                        </Col>
                        <Col xs={4}>
                            模板类型：
                            <span className="color666">
                                {window.GLOBAL.pageData.smsTemplate.type.map((item,i) => (
                                    item.value===this.state.type?(item.text):""
                                ))}
                            </span>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={4}>
                            审核状态：
                            <span className="color666">{this.state.status}</span>
                            {this.state.statusDes?(
                                <div className="icon-help" style={{display:"inline-block"}}></div>
                            ):""}
                            {this.state.statusDes?(
                                <div className="titleTips">
                                    <div className="arrow">
                                    <em></em><span></span>
                                    </div>
                                    {this.state.statusDes}
                                </div>
                            ):""}
                        </Col>
                        <Col xs={4}>
                            提交时间：
                            <span className="color666">{this.state.time1}</span>
                        </Col>
                        <Col xs={4}>
                            审核时间：
                            <span className="color666">{this.state.time2}</span>
                        </Col>
                    </Row>
                    <div className="title">模板内容</div>
                    <div className="content color666">{this.state.content}</div>
                    <div className="title">模板参数列表</div>
                    {this.state.parms.map((item,i) =>(
                        <div key={i} className="item">
                            <Row>
                                <Col xs={4}>
                                    参数名称：
                                    <span className="color666">{item.name}</span>
                                </Col>
                                <Col xs={4}>
                                    参数类型：
                                    <span className="color666">{item.type}</span>
                                </Col>
                                <Col xs={4}>
                                    长度：
                                    <span className="color666">{item.length}</span>
                                </Col>
                            </Row>
                            <div className="text">
                                <span className="absolute">参数描述：</span>
                                <span className="color666">{item.des}</span>
                            </div>
                        </div>
                    ))}
                    <div className="title">模板使用场景</div>
                    <div className="content color666">{this.state.scene}</div>
                </div>
            </div>
        )
    }
}
module.exports = smsTemplateDetail;