/**
 * Created by hqer on 2016/12/16.
 */
import React from 'react'
import { browserHistory } from 'react-router'
import { Table,Button,Row,Col } from 'react-bootstrap'

class orderItemDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            status:"",
            times:[],
            payId:"",
            data:{},
            cost:0
        };
        this.goToBack = this.goToBack.bind(this);
    }
    componentDidMount(){
        let parms = {
            orderId:this.props.params.orderId,
            token:window.GLOBAL.token
        };
        window.GLOBAL.ajaxMap.setParms("orderItemDetail",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("orderItemDetail");
        ajax.success=function(r){
            this.setState({
                status:r.status,
                times:r.times,
                payId:r.payId,
                data:r.data,
                cost:r.cost
            });
        }.bind(this);
        $.ajax(ajax);
    }
    goToBack(){
        browserHistory.goBack();
    }
    render() {
        let orderItemDetail = window.GLOBAL.pageData.orderManage.orderItemDetail;
        return (
            <div className="orderItemDetail">
                <div className="MainTitle">
                    <h1>{orderItemDetail.title}</h1>
                    <Button className="backBtn" onClick={this.goToBack}>返回</Button>
                </div>
                <div className="whiteSpace">
                    <ul className="bigCircleProgress">
                        <li className={this.state.times&&this.state.times[0]?"bigCircle select":"bigCircle"}>
                            <div className="text">购买套餐</div>
                            <div className="num">1</div>
                            <div className="underTips">{this.state.times&&this.state.times[0]?this.state.times[0]:""}</div>
                        </li>
                        <li className={this.state.times&&this.state.times[1]?"bigCircle select":"bigCircle"}>
                            <div className="text">订单有效</div>
                            <div className="num">2</div>
                            <div className="underTips">{this.state.times&&this.state.times[1]?this.state.times[1]:""}</div>
                        </li>
                        <li className={this.state.times&&this.state.times[2]&&this.state.status==="失效"?"bigCircle select":"bigCircle"}>
                            <div className="text">订单失效</div>
                            <div className="num">3</div>
                            <div className="underTips">{this.state.times&&this.state.times[2]?this.state.times[2]:""}</div>
                        </li>
                    </ul>
                    <Row>
                        <Col xs={5}>订单编号: {this.props.params.orderId}</Col>
                        <Col xs={5}>交易流水号: {this.state.payId}</Col>
                        <Col xs={2}>订单状态: {this.state.status}</Col>
                    </Row>
                    <Row>
                        <Col xs={12}>有效时间: {this.state.times.length===3?this.state.times[1]+" - "+this.state.times[2]:"--"}</Col>
                    </Row>
                    <Table striped hover responsive className="basicList" style={{borderTop:"1px solid #ddd"}}>
                        <thead>
                        <tr>
                            {orderItemDetail.listTitle.map((item,i) => (
                                <th className={"th"+i} key={i}>{item}</th>
                            ))}
                        </tr>
                        </thead>
                        <tbody>
                            <tr key={0}>
                                {orderItemDetail.showKey.map((item,i) => (
                                    <td key={i} className={"th"+i}>
                                        {Object.prototype.toString.call(this.state.data[item])==='[object Array]'?(
                                            this.state.data[item].map((e,j) => (
                                                <div className="line" key={j}>{e}</div>
                                            ))
                                        ):(
                                            <div className="ShowTxt">{this.state.data[item]}</div>
                                        )}
                                    </td>
                                ))}
                            </tr>
                            <tr key={1}>
                                <td colSpan={orderItemDetail.listTitle.length}>
                                    <div style={{width:"100%",textAlign:"right"}}>
                                        实际消费: <font color="red">{this.state.cost}</font>元
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </div>
        )
    }
}
module.exports = orderItemDetail;