/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import { Link } from 'react-router'
import { Button,Table,ButtonToolbar,DropdownButton,Dropdown,MenuItem,Row,Col} from 'react-bootstrap'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import moment from 'moment'
import ReactHighcharts from 'react-highcharts'
import PageArea from '../componentUI/js/pageArea'

class ListTd extends React.Component{
    render(){
        return(
            <td className={this.props.className}>
                {this.props.showKeyName==="status"?(
                    this.props.data==="进行中"?(<font color="#57cea4">{text}</font>):(
                        this.props.data==="失败"?(<font color="red">{text}</font>):(
                            this.props.data==="关闭"?(<font color="#cccccc">{text}</font>):(this.props.data)
                        )
                    )
                ):(
                    this.props.showKeyName==="tradeTime"?(
                        <div className="blockTxt">{this.props.data}</div>
                    ):(
                        this.props.showKeyName==="operateType"?(
                            <div className="ShowTxt">{this.props.data}</div>
                        ):(this.props.data)
                    )
                )}
            </td>
        )
    }
}

class ListTr extends React.Component{
    render(){
        return(
            <tr className={this.props.className}>
                {this.props.showKey.map((item,i) => (
                    <ListTd data={this.props.data[item]} showKeyName={item} className={"th"+i} key={i}/>
                ))}
            </tr>
        )
    }
}

class ListBasic extends React.Component{
    render() {
        let data = [];
        if(this.props.data.length){
            this.props.data.map((element,index) => {
                data.push(<ListTr data={element} key={index} showKey={this.props.showKey} className={index%2===0?"bgGray":""}/>)
            })
        }else{
            data.push(<tr key={0}><td className="noData" colSpan={this.props.title.length}>{this.props.noDataTips}</td></tr>);
        }
        return (
            <Table striped hover responsive className="basicList">
                <thead>
                <tr>
                    {this.props.title.map((item,i) => (
                        <th className={"th"+i} key={i}>{item}</th>
                    ))}
                </tr>
                </thead>
                <tbody>
                {data}
                </tbody>
            </Table>
        )
    }
}
ListBasic.propTypes = {
    title: React.PropTypes.array.isRequired,
    showKey: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired,
    noDataTips: React.PropTypes.string.isRequired
};
ListBasic.defaultProps = {
    title:[],
    showKey:[],
    data:[],
    noDataTips:"暂无数据"
};


class MyAccountChild extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            operateType:"",
            status:"",
            paymentType:"",
            paymentSn:"",
            tradeTime:"",
            isSearch:false,
            pageNow:1,
            totalPage:10,
            data:[]
        };
        this._getInfo = this._getInfo.bind(this);
        this.setOperateType = this.setOperateType.bind(this);
        this.setPaymentSn = this.setPaymentSn.bind(this);
        this.statusSelectItem = this.statusSelectItem.bind(this);
        this.paymentTypeSelectItem = this.paymentTypeSelectItem.bind(this);
        this.handleEvent = this.handleEvent.bind(this);
        this.search = this.search.bind(this);
        this.clear = this.clear.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.month!=this.props.month){
            let parms = {
                month:newProps.month,
                operateType:"",
                status:"",
                paymentType:"",
                paymentSn:"",
                tradeTime:"",
                isSearch:false,
                pageNow:1,
                token:window.GLOBAL.token
            };
            this._getInfo(parms);
        }
    }
    componentDidMount(){
        let parms = {
            month:this.props.month,
            operateType:this.state.operateType,
            status:this.state.status,
            paymentType:this.state.paymentType,
            paymentSn:this.state.paymentSn,
            tradeTime:this.state.tradeTime,
            pageNow:this.state.pageNow,
            isSearch:this.state.isSearch,
            token:window.GLOBAL.token
        };
        this._getInfo(parms);
    }
    _getInfo(parms){
        window.GLOBAL.ajaxMap.setParms("searchAccountRecord",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("searchAccountRecord");
        ajax.success=function(r){
            this.setState({
                month:parms.month,
                operateType:parms.operateType,
                status:parms.status,
                paymentType:parms.paymentType,
                paymentSn:parms.paymentSn,
                tradeTime:parms.tradeTime,
                isSearch:parms.isSearch,
                pageNow:parms.pageNow,
                totalPage:r.totalPage,
                data:r.data
            });
        }.bind(this);
        $.ajax(ajax);
    }
    setOperateType(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({operateType:_val})
    }
    setPaymentSn(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({paymentSn:_val})
    }
    statusSelectItem(eventKey){
        this.setState({status:eventKey})
    }
    paymentTypeSelectItem(eventKey){
        this.setState({paymentType:eventKey})
    }
    handleEvent(event, picker){
        if(event.type==="apply"){
            this.setState({
                startDate: picker.startDate,
                endDate: picker.endDate,
                tradeTime: picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD')
            });
        }
    }
    search(){
        let parms ={
            eaId:this.props.month,
            operateType:this.state.operateType,
            status:this.state.status,
            paymentType:this.state.paymentType,
            paymentSn:this.state.paymentSn,
            tradeTime:this.state.tradeTime,
            isSearch:true,
            pageNow:1,
            token:window.GLOBAL.token
        };
        if(parms.operateType===""&&parms.status===""&&parms.paymentType===""&&parms.paymentSn===""&&parms.tradeTime===""){
            parms.isSearch = false;
        }
        this._getInfo(parms);
    }
    clear(){
        let parms ={
            eaId:this.props.month,
            operateType:"",
            status:"",
            paymentType:"",
            paymentSn:"",
            tradeTime:"",
            isSearch:false,
            pageNow:1,
            token:window.GLOBAL.token
        };
        this._getInfo(parms);
    }
    handleSelect(pageNow){
        let parms ={
            eaId:this.props.month,
            operateType:this.state.operateType,
            status:this.state.status,
            paymentType:this.state.paymentType,
            paymentSn:this.state.paymentSn,
            tradeTime:this.state.tradeTime,
            isSearch:this.state.isSearch,
            pageNow:pageNow,
            token:window.GLOBAL.token
        };
        this._getInfo(parms);
    }
    render() {
        let myAccount = window.GLOBAL.pageData.myAccount;
        let dateRangePickerData ={
            startDate:this.state.startDate ? moment(this.state.startDate) : moment(),
            endDate:this.state.endDate ? moment(this.state.endDate) : moment(),
            maxDate:moment(),
            locale: window.GLOBAL.pageData.dateRangePickerLocale
        };
        let status = "";
        myAccount.status.map((item,i) => {
            if(item.value===this.state.status){
                status = item.text;
            }
        });
        let paymentType = "";
        myAccount.paymentType.map((item,i) => {
            if(item.value===this.state.paymentType){
                paymentType = item.text;
            }
        });
        return (
            <div className="myAccountChild">
                <div className="searchArea">
                    <ButtonToolbar>
                        <Row>
                            <Col xs={4}>
                                <span className="text fl">交易明细：</span>
                                <input type="text" onChange={this.setOperateType} value={this.state.operateType}/>
                            </Col>
                            <Col xs={4}>
                                <span className="text fl">交易状态：</span>
                                <Dropdown id="status">
                                    <Dropdown.Toggle>
                                        <input className="text" placeholder="请选择" style={{width:"143px",backgroundColor:"transparent",border:"0"}} disabled value={status}/>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        {myAccount.status.map((item,i) => (
                                            <MenuItem eventKey={item.value} key={i} onSelect={this.statusSelectItem}>{item.text}</MenuItem>
                                        ))}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Col>
                            <Col xs={4}>
                                <span className="text fl">交易方式：</span>
                                    <Dropdown id="status">
                                        <Dropdown.Toggle>
                                            <input className="text" placeholder="请选择" style={{width:"143px",backgroundColor:"transparent",border:"0"}} disabled value={paymentType}/>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            {myAccount.paymentType.map((item,i) => (
                                                <MenuItem eventKey={item.value} key={i} onSelect={this.paymentTypeSelectItem}>{item.text}</MenuItem>
                                            ))}
                                        </Dropdown.Menu>
                                    </Dropdown>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4}>
                                <span className="text fl">交易编号：</span>
                                <input type="text"  onChange={this.setPaymentSn} value={this.state.paymentSn}/>
                            </Col>
                            <Col xs={4}>
                                <span className="text fl">交易时间：</span>
                                <DateRangePicker
                                    className="dateRangeBox fl"
                                    opens = "right"
                                    applyClass="btn-primary"
                                    dateLimit={dateRangePickerData.dateLimit}
                                    startDate={dateRangePickerData.startDate}
                                    endDate={dateRangePickerData.endDate}
                                    locale={dateRangePickerData.locale}
                                    maxDate={dateRangePickerData.maxDate}
                                    onEvent={this.handleEvent}>
                                    <input type="text" value={this.state.tradeTime} readOnly/>
                                </DateRangePicker>
                            </Col>
                            <Col xs={4} style={{textAlign:"center"}}>
                                <Button bsStyle="primary" onClick={this.search}>查询</Button>
                                {this.state.isSearch?(<Button bsStyle="link" onClick={this.clear}>清除</Button>):""}
                            </Col>
                        </Row>
                    </ButtonToolbar>
                </div>
                <ListBasic
                    title={myAccount.listTitle}
                    showKey={myAccount.showKey}
                    data={this.state.data}
                />
                <PageArea
                    totalPage={this.state.totalPage}
                    maxPage={3}
                    pageNow={this.state.pageNow}
                    onSelect={this.handleSelect}
                    onSearch={this.handleSelect}
                />
            </div>
        )
    }
}

class myAccount extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            balance:0.00,
            spend:0.00,
            record:window.GLOBAL.pageData.myAccount.record,
            horizon:window.GLOBAL.pageData.myAccount.horizon[0]
        };
        this.handleSelectItem = this.handleSelectItem.bind(this);
    }
    componentDidMount(){
        window.GLOBAL.ajaxMap.setParm("getMyAccount",'token',window.GLOBAL.token);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("getMyAccount");
        ajax.success=function(r){
            this.setState({
                balance:r.balance,
                spend:r.spend,
                record:r.record
            });
        }.bind(this);
        $.ajax(ajax);
    }
    handleSelectItem(eventKey){
        this.setState({horizon:eventKey});
    }
    render() {
        let myAccount = window.GLOBAL.pageData.myAccount;
        let monthList = window.GLOBAL.pageData.dateRangePickerLocale.monthNames;
        let horizon = Object.assign([],myAccount.horizon);
        for(let i=0;i <= moment().month();i++){
            horizon.push(monthList[i]);
        }
        let config = window.GLOBAL.pageData.highchartsConfig;
        if(this.state.record.series[0].data){
            config.xAxis={
                categories: this.state.record.categories,
                labels: {
                    step:1,
                    rotation:45
                }
            };
            config.yAxis = {
                lineWidth: 1,
                    title: {
                        text:"消费金额(元)"
                    },
                min: 0
            };
            config.series = this.state.record.series;
            config.tooltip = {
                formatter:function(){
                    return this.x + "<br> 消费总金额"+this.y+"元<br>购买套餐"+this.point.zOrderConsume+"元 账户充值"+this.point.zAccountConsume+"元<br>超出扣费"+this.point.zExceedConsume+"元";
                }
            };
        }
        return (
            <div className="myAccount">
                <div className="MainTitle">
                    <h1>{myAccount.title}</h1>
                </div>
                <div className="whiteSpace">
                    <div className="fl text1">账户余额：<font color="red">{this.state.balance}</font>元</div><Link className="btn btn-info fl" role="button" to="/recharge">充值</Link>
                    <div className="fl text2">昨日消费：{this.state.spend}元</div>
                </div>
                <div className="whiteSpace">
                    <h1>近15天的每日消费记录</h1>
                    <ReactHighcharts config={config}></ReactHighcharts>
                </div>
                <div className="whiteSpace">
                    <h1>
                        账户交易记录
                        <div className="icon-help"></div>
                        <div className="titleTips">
                            <div className="arrow">
                                <em></em><span></span>
                            </div>
                            <p>1、账户交易信息包括购买、充值等所有在平台进行消费的明细。</p>
                            <p>2、仅保留本年度消费月账单。</p>
                        </div>
                    </h1>
                    <div className="fl text3">{moment().year()}年</div>
                    <ButtonToolbar>
                        <Dropdown id="horizon" style={{marginTop:"3px"}}>
                            <Dropdown.Toggle>
                                <span className="text" style={{minWidth:"150px"}}>{this.state.horizon}</span>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {horizon.map((item,i) => (
                                    <MenuItem eventKey={item} key={i} onSelect={this.handleSelectItem}>{item}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <a className="fr btn btn-primary" style={{marginTop:"3px"}} href={myAccount.downloadUrl+"?download="+this.state.horizon} role="button" target="_blank">下载账单</a>
                    </ButtonToolbar>
                    <MyAccountChild month={this.state.horizon} />
                </div>
            </div>
        )
    }
}
module.exports = myAccount;