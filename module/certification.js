/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import Reflux from 'reflux'
Reflux.defineReact(React);

import { Button,ButtonToolbar,DropdownButton,Dropdown,MenuItem} from 'react-bootstrap'
import FileUpload from 'react-fileupload'

import '../store/certification'

class PersonSpan extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value.split("_");
        window.GLOBAL.action.certification.setType(_val[0]*1,_val[1]*1);
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        return (
            <div className="certificationArea">
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">所属地区:</div>
                    <div className="fl">{this.props.region}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">真实姓名:</div>
                    <div className="fl">{this.props.name}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件类型:</div>
                    <div className="fl">{this.props.caType}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件号码:</div>
                    <div className="fl">{this.props.idCard}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件照片:</div>
                    <img className="fl" src={certification.idCardImgUpload.idCardImgUrl+this.props.idCardImg} width="246" height="155"/>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <Button onClick={this.handleClick} value={certification.type[this.props.type].btn[0].value}>{certification.type[this.props.type].btn[0].text}</Button>
                </div>
            </div>
        )
    }
}
class PersonInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            type:this.props.type,
            region:this.props.type!==4?this.props.region:"",
            name:this.props.type!==4?this.props.name:"",
            caType:this.props.type!==4?this.props.caType:"",
            idCard:this.props.type!==4?this.props.idCard:"",
            idCardImg:this.props.type!==4?this.props.idCardImg:"",
            upload:{
                id:"",
                text:"选择文件",
                errorTips:"",
                status:this.props.type!==4?true:false
            }
        };
        this.setName =this.setName.bind(this);
        this.checkName = this.checkName.bind(this);
        this.caTypeSelectItem = this.caTypeSelectItem.bind(this);
        this.setIdCard = this.setIdCard.bind(this);
        this.checkIdCard =this.checkIdCard.bind(this);
        this.setIdCardImg =this.setIdCardImg.bind(this);
        this.submit = this.submit.bind(this);

    }
    componentWillReceiveProps(newProps){
        if(newProps.type!==4){
            this.setState({
                type:newProps.type,
                region:newProps.region,
                name:newProps.name,
                caType:newProps.caType,
                idCard:newProps.idCard,
                idCardImg:newProps.idCardImg,
                upload:{id:"", text:"选择文件", errorTips:"", status:true}
            })
        }
    }
    componentDidMount(){
        $(".certificationArea .region").citypicker();
        $(document).on("click",".city-picker-dropdown .city-select a",function(event){
            let region = $(".region").siblings(".city-picker-span").children(".title").text();
            if(!region){
                $(".certificationArea .region").siblings(".errorTips").html("* 输入项不能为空!");
            }else{
                $(".certificationArea .region").siblings(".errorTips").html("");
            }
            this.setState({region:region});
        }.bind(this));
    }
    setName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({name:_val});
    }
    checkName(){
        window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"));
    }
    caTypeSelectItem(eventKey){
        this.setState({caType:eventKey});
    }
    setIdCard(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCard:_val});
    }
    checkIdCard(){
        window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"));
    }
    setIdCardImg(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCardImg:_val});
    }
    submit(){
        let isOk = true;
        if(!window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"))){
            isOk = false;
        }
        if(!this.state.upload.status){
            isOk = false;
            if(!this.state.upload.errorTips){
                let _upload = this.state.upload;
                _upload.errorTips = "* 输入项不能为空!";
                this.setState({upload:_upload})
            }
        }
        if(!this.state.caType){
            $("#caType").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(!this.state.region){
            isOk = false;
            $(".certificationArea .region").siblings(".errorTips").html("* 输入项不能为空!")
        }
        if(isOk){
            let parms = {
                devType:0,
                name:this.state.name,
                caType:this.state.caType,
                idCard:this.state.idCard,
                idCardImg:this.state.idCardImg,
                region:this.state.region,
                token:window.GLOBAL.token
            };
            window.GLOBAL.ajaxMap.setParms("addUserInfo",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("addUserInfo");
            ajax.success=function(r){
                if(r.code){
                    window.GLOBAL.action.certification.init();
                }
            }.bind(this);
            $.ajax(ajax);
        }
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        let caType = "";
        certification.caType.map((item,i) => {
            if(item.value===this.state.caType){
                caType = item.text;
                $("#caType").parent().siblings(".errorTips").html("");
            }
        });
        let options = {
            baseUrl:certification.idCardImgUpload.baseUrl,
            param:{
                uploadToken:window.GLOBAL.uploadToken
            },
            chooseAndUpload:true,
            accept: certification.idCardImgUpload.accept,
            beforeChoose:function(){
                console.log('=====START======')
            }.bind(this),
            chooseFile:function(files){
                console.log('=====chooseFile======');
                if(typeof files == "String"){
                    this.setState({idCardImg:files})
                }else{
                    this.setState({idCardImg:files[0].name})
                }
            }.bind(this),
            beforeUpload:function(files , mill){
                console.log('=====beforeUpload======');
                if(typeof files == "String"){
                    return true;
                }
                let _upload = {id:"",text:"选择图片",errorTips:"",status:false};
                if(files[0].size === 0){
                    _upload.errorTips = "* 图片大小为0,请重新上传!";
                    this.setState({upload:_upload,idCardImg:""});
                    return false;
                }else if(files[0].size > certification.idCardImgUpload.maxSize){
                    _upload.errorTips = "* 图片过大,请重新上传!";
                    this.setState({upload:_upload,idCardImg:""});
                    return false;
                }
                return true;
            }.bind(this),
            doUpload : function(files,mill,xhrID){
                console.log('=====doUpload======')
                let _upload = {id:xhrID,text:"上传中...",errorTips:"",status:false};
                this.setState({upload:_upload});
            }.bind(this),
            uploadSuccess : function(resp){
                console.log('=====uploadSuccess======')
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传成功!",status:true};
                this.setState({upload:_upload});
            }.bind(this),
            uploadError : function(err){
                console.log("uploadError===="+err.message)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({upload:_upload,idCardImg:""});
            }.bind(this),
            uploadFail : function(resp){
                console.log("uploadFail===="+resp)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({upload:_upload,idCardImg:""});
            }.bind(this)
        }
        let img ="";
        if(this.state.upload.status&&this.state.idCardImg){
            img=(
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <img src={certification.idCardImgUpload.idCardImgUrl+this.state.idCardImg} width="246" height="155"/>
                </div>
            )
        }
        return (
            <div className="certificationArea">
                <ButtonToolbar>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">所属地区:</div>
                        <div className="fl" style={{position:"relative"}}>
                            <input className="region" style={{width:"268px"}} readOnly type="text" data-toggle="city-picker" data-level={certification.region.level} placeholder={certification.region.placeholder} value={this.state.region}/>
                            <div className="errorTips"></div>
                        </div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">真实姓名:</div>
                        <input className="name" style={{width:"268px"}} value={this.state.name} onChange={this.setName} onBlur={this.checkName} maxLength={certification.name.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件类型:</div>
                        <Dropdown id="caType">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={caType}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.caType.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.caTypeSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件号码:</div>
                        <input className="idCard" style={{width:"268px"}} value={this.state.idCard} onChange={this.setIdCard} onBlur={this.checkIdCard}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件照片:</div>
                        <input readOnly className="idCardImgUpload" value={this.state.idCardImg} onChange={this.setIdCardImg}/>
                        <FileUpload options={options}>
                            <button ref="chooseAndUpload">{this.state.upload.text}</button>
                        </FileUpload>
                        <div className="errorTips">{this.state.upload.errorTips}</div>
                    </div>
                    {img}
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl"></div>
                        <Button onClick={this.submit}>确定</Button>
                    </div>
                </ButtonToolbar>
        </div>
        )
    }
}
class CompanySpan extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value.split("_");
        window.GLOBAL.action.certification.setType(_val[0]*1,_val[1]*1);
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        return (
            <div className="certificationArea">
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">开发者类型:</div>
                    <div className="fl">公司</div>
                </div>
                <div className="titleLine fl" style={{width:"100%"}}>
                    <div className="fl">公司信息</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">公司名称:</div>
                    <div className="fl">{this.props.companyName}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">所属地区:</div>
                    <div className="fl">{this.props.region}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">详细地址:</div>
                    <div className="fl">{this.props.address}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">公司性质:</div>
                    <div className="fl">{this.props.companyType}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">所属行业:</div>
                    <div className="fl">{this.props.trade}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">公司规模:</div>
                    <div className="fl">{this.props.scale}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">营业执照编号:</div>
                    <div className="fl">{this.props.license}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">营业执照照片:</div>
                    <img className="fl" src={certification.licenseImgUpload.licenseImgUrl+this.props.licenseImg} width="246" height="155"/>
                </div>
                <div className="titleLine fl" style={{width:"100%"}}>
                    <div className="fl">管理员信息</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">真实姓名:</div>
                    <div className="fl">{this.props.name}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件类型:</div>
                    <div className="fl">{this.props.caType}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件号码:</div>
                    <div className="fl">{this.props.idCard}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件照片:</div>
                    <img className="fl" src={certification.idCardImgUpload.idCardImgUrl+this.props.idCardImg} width="246" height="155"/>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <Button onClick={this.handleClick} value={certification.type[this.props.type].btn[1].value}>{certification.type[this.props.type].btn[1].text}</Button>
                </div>
            </div>
        )
    }
}
class CompanyInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            type:this.props.type!==4?this.props.type:"",
            companyName:this.props.type!==4?this.props.companyName:"",
            address:this.props.type!==4?this.props.address:"",
            companyType:this.props.type!==4?this.props.companyType:"",
            trade:this.props.type!==4?this.props.trade:"",
            scale:this.props.type!==4?this.props.scale:"",
            license:this.props.type!==4?this.props.license:"",
            licenseImg:this.props.type!==4?this.props.licenseImg:"",
            region:this.props.type!==4?this.props.region:"",
            name:this.props.type!==4?this.props.name:"",
            caType:this.props.type!==4?this.props.caType:"",
            idCard:this.props.type!==4?this.props.idCard:"",
            idCardImg:this.props.type!==4?this.props.idCardImg:"",
            uploadIdCard:{
                id:"",
                text:"选择文件",
                errorTips:"",
                status:this.props.type!==4?true:false
            },
            uploadLicense:{
                id:"",
                text:"选择文件",
                errorTips:"",
                status:this.props.type!==4?true:false
            }
        };
        this.setName =this.setName.bind(this);
        this.checkName = this.checkName.bind(this);
        this.caTypeSelectItem = this.caTypeSelectItem.bind(this);
        this.setIdCard = this.setIdCard.bind(this);
        this.checkIdCard =this.checkIdCard.bind(this);
        this.setIdCardImg =this.setIdCardImg.bind(this);
        this.submit = this.submit.bind(this);
        this.setCompanyName = this.setCompanyName.bind(this);
        this.checkCompanyName = this.checkCompanyName.bind(this);
        this.setAddress = this.setAddress.bind(this);
        this.checkAddress = this.checkAddress.bind(this);
        this.companyTypeSelectItem = this.companyTypeSelectItem.bind(this);
        this.tradeSelectItem = this.tradeSelectItem.bind(this);
        this.scaleSelectItem = this.scaleSelectItem.bind(this);
        this.checkLicense = this.checkLicense.bind(this);
        this.setLicenseImg = this.setLicenseImg.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.type!==4){
            this.setState({
                type:newProps.type,
                companyName:newProps.companyName,
                address:newProps.address,
                companyType:newProps.companyType,
                trade:newProps.trade,
                scale:newProps.scale,
                license:newProps.license,
                licenseImg:newProps.licenseImg,
                region:newProps.region,
                name:newProps.name,
                caType:newProps.caType,
                idCard:newProps.idCard,
                idCardImg:newProps.idCardImg,
                uploadIdCard:{id:"", text:"选择文件", errorTips:"", status:true},
                uploadLicense:{id:"", text:"选择文件", errorTips:"", status:true}
            })
        }
    }
    componentDidMount(){
        $(".certificationArea .region").citypicker();
        $(document).on("click",".city-picker-dropdown .city-select a",function(event){
            let region = $(".region").siblings(".city-picker-span").children(".title").text();
            if(!region){
                $(".certificationArea .region").siblings(".errorTips").html("* 输入项不能为空!");
            }else{
                $(".certificationArea .region").siblings(".errorTips").html("");
            }
            this.setState({region:region});
        }.bind(this));
    }
    setName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({name:_val});
    }
    checkName(){
        window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"));
    }
    caTypeSelectItem(eventKey){
        this.setState({caType:eventKey});
    }
    setIdCard(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCard:_val});
    }
    checkIdCard(){
        window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"));
    }
    setIdCardImg(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCardImg:_val});
    }
    submit(){
        let isOk = true;
        if(!window.GLOBAL.checkFun.isCompanyName($(".certificationArea .companyName"),$(".certificationArea .companyName").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isNotEmpty($(".certificationArea .address"),$(".certificationArea .address").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isCompanyLicense($(".certificationArea .license"),$(".certificationArea .license").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"))){
            isOk = false;
        }
        if(!this.state.uploadIdCard.status){
            isOk = false;
            if(!this.state.uploadIdCard.errorTips){
                let _upload0 = this.state.uploadIdCard;
                _upload0.errorTips = "* 输入项不能为空!";
                this.setState({uploadIdCard:_upload0})
            }
        }
        if(!this.state.uploadLicense.status){
            isOk = false;
            if(!this.state.uploadLicense.errorTips){
                let _upload1 = this.state.uploadLicense;
                _upload1.errorTips = "* 输入项不能为空!";
                this.setState({uploadLicense:_upload1})
            }
        }
        if(!this.state.companyType){
            $("#companyType").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(!this.state.trade){
            $("#trade").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(!this.state.scale){
            $("#scale").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(!this.state.caType){
            $("#caType").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(!this.state.region){
            isOk = false;
            $(".certificationArea .region").siblings(".errorTips").html("* 输入项不能为空!")
        }
        if(isOk){
            let parms = {
                devType:0,
                name:this.state.name,
                caType:this.state.caType,
                idCard:this.state.idCard,
                idCardImg:this.state.idCardImg,
                region:this.state.region,
                token:window.GLOBAL.token
            };
            window.GLOBAL.ajaxMap.setParms("addUserInfo",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("addUserInfo");
            ajax.success=function(r){
                if(r.code){
                    window.GLOBAL.action.certification.init();
                }
            }.bind(this);
            $.ajax(ajax);
        }
    }
    setCompanyName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({companyName:_val});
    }
    checkCompanyName(){
        window.GLOBAL.checkFun.isCompanyName($(".certificationArea .companyName"),$(".certificationArea .companyName").siblings(".errorTips"));
    }
    setAddress(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({address:_val});
    }
    checkAddress(){
        window.GLOBAL.checkFun.isNotEmpty($(".certificationArea .address"),$(".certificationArea .address").siblings(".errorTips"));
    }
    companyTypeSelectItem(eventKey){
        this.setState({companyType:eventKey});
    }
    tradeSelectItem(eventKey){
        this.setState({trade:eventKey});
    }
    scaleSelectItem(eventKey){
        this.setState({scale:eventKey});
    }
    setLicense(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({license:_val});
    }
    checkLicense(){
        window.GLOBAL.checkFun.isCompanyLicense($(".certificationArea .license"),$(".certificationArea .license").siblings(".errorTips"));
    }
    setLicenseImg(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({licenseImg:_val});
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        let caType = "";
        certification.caType.map((item,i) => {
            if(item.value===this.state.caType){
                caType = item.text;
                $("#caType").parent().siblings(".errorTips").html("");
            }
        });
        let companyType = "";
        certification.companyType.map((item,i) => {
            if(item.value===this.state.companyType){
                companyType = item.text;
                $("#companyType").parent().siblings(".errorTips").html("");
            }
        });
        let trade = "";
        certification.trade.map((item,i) => {
            if(item.value===this.state.trade){
                trade = item.text;
                $("#trade").parent().siblings(".errorTips").html("");
            }
        });
        let scale = "";
        certification.scale.map((item,i) => {
            if(item.value===this.state.scale){
                scale = item.text;
                $("#scale").parent().siblings(".errorTips").html("");
            }
        });
        let options0 = {
            baseUrl:certification.idCardImgUpload.baseUrl,
            param:{
                uploadToken:window.GLOBAL.uploadToken
            },
            chooseAndUpload:true,
            accept: certification.idCardImgUpload.accept,
            beforeChoose:function(){
                console.log('=====START======')
            }.bind(this),
            chooseFile:function(files){
                console.log('=====chooseFile======');
                if(typeof files == "String"){
                    this.setState({idCardImg:files})
                }else{
                    this.setState({idCardImg:files[0].name})
                }
            }.bind(this),
            beforeUpload:function(files , mill){
                console.log('=====beforeUpload======');
                if(typeof files == "String"){
                    return true;
                }
                let _upload = {id:"",text:"选择图片",errorTips:"",status:false};
                if(files[0].size === 0){
                    _upload.errorTips = "* 图片大小为0,请重新上传!";
                    this.setState({uploadIdCard:_upload,idCardImg:""});
                    return false;
                }else if(files[0].size > certification.idCardImgUpload.maxSize){
                    _upload.errorTips = "* 图片过大,请重新上传!";
                    this.setState({uploadIdCard:_upload,idCardImg:""});
                    return false;
                }
                return true;
            }.bind(this),
            doUpload : function(files,mill,xhrID){
                console.log('=====doUpload======')
                let _upload = {id:xhrID,text:"上传中...",errorTips:"",status:false};
                this.setState({uploadIdCard:_upload});
            }.bind(this),
            uploadSuccess : function(resp){
                console.log('=====uploadSuccess======')
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传成功!",status:true};
                this.setState({uploadIdCard:_upload});
            }.bind(this),
            uploadError : function(err){
                console.log("uploadError===="+err.message)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({uploadIdCard:_upload,idCardImg:""});
            }.bind(this),
            uploadFail : function(resp){
                console.log("uploadFail===="+resp)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({uploadIdCard:_upload,idCardImg:""});
            }.bind(this)
        };
        let options1 = {
            baseUrl:certification.licenseImgUpload.baseUrl,
            param:{
                uploadToken:window.GLOBAL.uploadToken
            },
            chooseAndUpload:true,
            accept: certification.licenseImgUpload.accept,
            beforeChoose:function(){
                console.log('=====START======')
            }.bind(this),
            chooseFile:function(files){
                console.log('=====chooseFile======');
                if(typeof files == "String"){
                    this.setState({licenseImg:files})
                }else{
                    this.setState({licenseImg:files[0].name})
                }
            }.bind(this),
            beforeUpload:function(files , mill){
                console.log('=====beforeUpload======');
                if(typeof files == "String"){
                    return true;
                }
                let _upload = {id:"",text:"选择图片",errorTips:"",status:false};
                if(files[0].size === 0){
                    _upload.errorTips = "* 图片大小为0,请重新上传!";
                    this.setState({uploadLicense:_upload,licenseImg:""});
                    return false;
                }else if(files[0].size > certification.idCardImgUpload.maxSize){
                    _upload.errorTips = "* 图片过大,请重新上传!";
                    this.setState({uploadLicense:_upload,licenseImg:""});
                    return false;
                }
                return true;
            }.bind(this),
            doUpload : function(files,mill,xhrID){
                console.log('=====doUpload======')
                let _upload = {id:xhrID,text:"上传中...",errorTips:"",status:false};
                this.setState({uploadLicense:_upload});
            }.bind(this),
            uploadSuccess : function(resp){
                console.log('=====uploadSuccess======')
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传成功!",status:true};
                this.setState({uploadLicense:_upload});
            }.bind(this),
            uploadError : function(err){
                console.log("uploadError===="+err.message)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({uploadLicense:_upload,licenseImg:""});
            }.bind(this),
            uploadFail : function(resp){
                console.log("uploadFail===="+resp)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({uploadLicense:_upload,licenseImg:""});
            }.bind(this)
        };
        let idCardImg ="";
        if(this.state.uploadIdCard.status&&this.state.idCardImg){
            idCardImg=(
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <img src={certification.idCardImgUpload.idCardImgUrl+this.state.idCardImg} width="246" height="155"/>
                </div>
            )
        }
        let licenseImg ="";
        if(this.state.uploadLicense.status&&this.state.licenseImg){
            licenseImg=(
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <img src={certification.licenseImgUpload.licenseImgUrl+this.state.licenseImg} width="246" height="155"/>
                </div>
            )
        }
        return (
            <div className="certificationArea">
                <ButtonToolbar>
                    <div className="titleLine fl" style={{width:"100%"}}>
                        <div className="fl">公司信息</div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">公司名称:</div>
                        <input className="companyName" style={{width:"268px"}} value={this.state.companyName} onChange={this.setCompanyName} onBlur={this.checkCompanyName} maxLength={certification.companyName.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">所属地区:</div>
                        <div className="fl" style={{position:"relative"}}>
                            <input className="region" style={{width:"268px"}} readOnly type="text" data-toggle="city-picker" data-level={certification.region.level} placeholder={certification.region.placeholder} value={this.state.region}/>
                            <div className="errorTips"></div>
                        </div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">详细地址:</div>
                        <textarea className="address" style={{width:"268px"}} value={this.state.address} onChange={this.setAddress} onBlur={this.checkAddress} maxLength={certification.address.maxLength} placeholder={certification.address.placeholder}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">公司性质:</div>
                        <Dropdown id="companyType">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={companyType}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.companyType.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.companyTypeSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">所属行业:</div>
                        <Dropdown id="trade">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={trade}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.trade.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.tradeSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">公司规模:</div>
                        <Dropdown id="scale">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={scale}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.scale.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.scaleSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">营业执照编号:</div>
                        <input className="license" style={{width:"268px"}} value={this.state.license} onChange={this.setLicense} onBlur={this.checkLicense} maxLength={certification.license.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">营业执照照片:</div>
                        <input readOnly className="licenseImgUpload" value={this.state.licenseImg} onChange={this.setLicenseImg}/>
                        <FileUpload options={options1}>
                            <button ref="chooseAndUpload">{this.state.uploadLicense.text}</button>
                        </FileUpload>
                        <div className="errorTips">{this.state.uploadLicense.errorTips}</div>
                    </div>
                    {licenseImg}
                    <div className="titleLine fl" style={{width:"100%"}}>
                        <div className="fl">管理员信息</div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">真实姓名:</div>
                        <input className="name" style={{width:"268px"}} value={this.state.name} onChange={this.setName} onBlur={this.checkName} maxLength={certification.name.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件类型:</div>
                        <Dropdown id="caType">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={caType}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.caType.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.caTypeSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件号码:</div>
                        <input className="idCard" style={{width:"268px"}} value={this.state.idCard} onChange={this.setIdCard} onBlur={this.checkIdCard}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件照片:</div>
                        <input readOnly className="idCardImgUpload" value={this.state.idCardImg} onChange={this.setIdCardImg}/>
                        <FileUpload options={options0}>
                            <button ref="chooseAndUpload">{this.state.uploadIdCard.text}</button>
                        </FileUpload>
                        <div className="errorTips">{this.state.uploadIdCard.errorTips}</div>
                    </div>
                    {idCardImg}
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl"></div>
                        <Button onClick={this.submit}>确定</Button>
                    </div>
                </ButtonToolbar>
            </div>
        )
    }
}
class CompanySpanInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            type:this.props.type,
            name:this.props.name,
            caType:this.props.caType,
            idCard:this.props.idCard,
            idCardImg:this.props.idCardImg,
            upload:{
                id:"",
                text:"选择文件",
                errorTips:"",
                status:this.props.type!==4?true:false
            }
        };
        this.setName =this.setName.bind(this);
        this.checkName = this.checkName.bind(this);
        this.caTypeSelectItem = this.caTypeSelectItem.bind(this);
        this.setIdCard = this.setIdCard.bind(this);
        this.checkIdCard =this.checkIdCard.bind(this);
        this.setIdCardImg =this.setIdCardImg.bind(this);
        this.submit = this.submit.bind(this);
    }
    componentWillReceiveProps(newProps){
        this.setState({
            region:newProps.region,
            name:newProps.name,
            caType:newProps.caType,
            idCard:newProps.idCard,
            idCardImg:newProps.idCardImg,
            upload:{id:"", text:"选择文件", errorTips:"", status:true}
        })
    }
    setName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({name:_val});
    }
    checkName(){
        window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"));
    }
    caTypeSelectItem(eventKey){
        this.setState({caType:eventKey});
    }
    setIdCard(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCard:_val});
    }
    checkIdCard(){
        window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"));
    }
    setIdCardImg(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCardImg:_val});
    }
    submit(){
        let isOk = true;
        if(!window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"))){
            isOk = false;
        }
        if(!this.state.upload.status){
            isOk = false;
            if(!this.state.upload.errorTips){
                let _upload = this.state.upload;
                _upload.errorTips = "* 输入项不能为空!";
                this.setState({upload:_upload})
            }
        }
        if(!this.state.caType){
            $("#caType").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(isOk){
            let parms = {
                devType:0,
                name:this.state.name,
                caType:this.state.caType,
                idCard:this.state.idCard,
                idCardImg:this.state.idCardImg,
                region:this.state.region,
                token:window.GLOBAL.token
            };
            window.GLOBAL.ajaxMap.setParms("addUserInfo",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("addUserInfo");
            ajax.success=function(r){
                if(r.code){
                    window.GLOBAL.action.certification.init();
                }
            }.bind(this);
            $.ajax(ajax);
        }
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        let caType = "";
        certification.caType.map((item,i) => {
            if(item.value===this.state.caType){
                caType = item.text;
                $("#caType").parent().siblings(".errorTips").html("");
            }
        });
        let options = {
            baseUrl:certification.idCardImgUpload.baseUrl,
            param:{
                uploadToken:window.GLOBAL.uploadToken
            },
            chooseAndUpload:true,
            accept: certification.idCardImgUpload.accept,
            beforeChoose:function(){
                console.log('=====START======')
            }.bind(this),
            chooseFile:function(files){
                console.log('=====chooseFile======');
                if(typeof files == "String"){
                    this.setState({idCardImg:files})
                }else{
                    this.setState({idCardImg:files[0].name})
                }
            }.bind(this),
            beforeUpload:function(files , mill){
                console.log('=====beforeUpload======');
                if(typeof files == "String"){
                    return true;
                }
                let _upload = {id:"",text:"选择图片",errorTips:"",status:false};
                if(files[0].size === 0){
                    _upload.errorTips = "* 图片大小为0,请重新上传!";
                    this.setState({upload:_upload,idCardImg:""});
                    return false;
                }else if(files[0].size > certification.idCardImgUpload.maxSize){
                    _upload.errorTips = "* 图片过大,请重新上传!";
                    this.setState({upload:_upload,idCardImg:""});
                    return false;
                }
                return true;
            }.bind(this),
            doUpload : function(files,mill,xhrID){
                console.log('=====doUpload======')
                let _upload = {id:xhrID,text:"上传中...",errorTips:"",status:false};
                this.setState({upload:_upload});
            }.bind(this),
            uploadSuccess : function(resp){
                console.log('=====uploadSuccess======')
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传成功!",status:true};
                this.setState({upload:_upload});
            }.bind(this),
            uploadError : function(err){
                console.log("uploadError===="+err.message)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({upload:_upload,idCardImg:""});
            }.bind(this),
            uploadFail : function(resp){
                console.log("uploadFail===="+resp)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({upload:_upload,idCardImg:""});
            }.bind(this)
        }
        let img ="";
        if(this.state.upload.status&&this.state.idCardImg){
            img=(
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <img src={certification.idCardImgUpload.idCardImgUrl+this.state.idCardImg} width="246" height="155"/>
                </div>
            )
        }
        return (
            <div className="certificationArea">
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">开发者类型:</div>
                    <div className="fl">公司</div>
                </div>
                <div className="titleLine fl" style={{width:"100%"}}>
                    <div className="fl">公司信息</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">公司名称:</div>
                    <div className="fl">{this.props.companyName}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">所属地区:</div>
                    <div className="fl">{this.props.region}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">详细地址:</div>
                    <div className="fl">{this.props.address}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">公司性质:</div>
                    <div className="fl">{this.props.companyType}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">所属行业:</div>
                    <div className="fl">{this.props.trade}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">公司规模:</div>
                    <div className="fl">{this.props.scale}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">营业执照编号:</div>
                    <div className="fl">{this.props.license}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">营业执照照片:</div>
                    <img className="fl" src={certification.licenseImgUpload.licenseImgUrl+this.props.licenseImg} width="246" height="155"/>
                </div>
                <ButtonToolbar>
                    <div className="titleLine fl" style={{width:"100%"}}>
                        <div className="fl">管理员信息</div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">真实姓名:</div>
                        <input className="name" style={{width:"268px"}} value={this.state.name} onChange={this.setName} onBlur={this.checkName} maxLength={certification.name.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件类型:</div>
                        <Dropdown id="caType">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={caType}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.caType.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.caTypeSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件号码:</div>
                        <input className="idCard" style={{width:"268px"}} value={this.state.idCard} onChange={this.setIdCard} onBlur={this.checkIdCard}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件照片:</div>
                        <input readOnly className="idCardImgUpload" value={this.state.idCardImg} onChange={this.setIdCardImg}/>
                        <FileUpload options={options}>
                            <button ref="chooseAndUpload">{this.state.upload.text}</button>
                        </FileUpload>
                        <div className="errorTips">{this.state.upload.errorTips}</div>
                    </div>
                    {img}
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl"></div>
                        <Button onClick={this.submit}>确定</Button>
                    </div>
                </ButtonToolbar>
            </div>
        )
    }
}
class GovSpan extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value.split("_");
        window.GLOBAL.action.certification.setType(_val[0]*1,_val[1]*1);
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        return (
            <div className="certificationArea">
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">开发者类型:</div>
                    <div className="fl">政府机关或事业单位</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">机构全称:</div>
                    <div className="fl">{this.props.govName}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">组织机构代码:</div>
                    <div className="fl">{this.props.license}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">所属地区:</div>
                    <div className="fl">{this.props.region}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">详细地址:</div>
                    <div className="fl">{this.props.address}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">机构规模:</div>
                    <div className="fl">{this.props.scale}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">管理员真实姓名:</div>
                    <div className="fl">{this.props.name}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件类型:</div>
                    <div className="fl">{this.props.caType}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件号码:</div>
                    <div className="fl">{this.props.idCard}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">证件照片:</div>
                    <img className="fl" src={certification.idCardImgUpload.idCardImgUrl+this.props.idCardImg} width="246" height="155"/>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <Button onClick={this.handleClick} value={certification.type[this.props.type].btn[2].value}>{certification.type[this.props.type].btn[2].text}</Button>
                </div>
            </div>
        )
    }
}
class GovInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            type:this.props.type!==4?this.props.type:"",
            govName:this.props.type!==4?this.props.govName:"",
            license:this.props.type!==4?this.props.license:"",
            region:this.props.type!==4?this.props.region:"",
            address:this.props.type!==4?this.props.address:"",
            scale:this.props.type!==4?this.props.scale:"",
            name:this.props.type!==4?this.props.name:"",
            caType:this.props.type!==4?this.props.caType:"",
            idCard:this.props.type!==4?this.props.idCard:"",
            idCardImg:this.props.type!==4?this.props.idCardImg:"",
            uploadIdCard:{
                id:"",
                text:"选择文件",
                errorTips:"",
                status:this.props.type!==4?true:false
            }
        };
        this.setName =this.setName.bind(this);
        this.checkName = this.checkName.bind(this);
        this.caTypeSelectItem = this.caTypeSelectItem.bind(this);
        this.setIdCard = this.setIdCard.bind(this);
        this.checkIdCard =this.checkIdCard.bind(this);
        this.setIdCardImg =this.setIdCardImg.bind(this);
        this.submit = this.submit.bind(this);
        this.setGovName = this.setGovName.bind(this);
        this.checkGovName = this.checkGovName.bind(this);
        this.setAddress = this.setAddress.bind(this);
        this.checkAddress = this.checkAddress.bind(this);
        this.scaleSelectItem = this.scaleSelectItem.bind(this);
        this.checkLicense = this.checkLicense.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.type!==4){
            this.setState({
                type:newProps.type,
                companyName:newProps.companyName,
                address:newProps.address,
                companyType:newProps.companyType,
                trade:newProps.trade,
                scale:newProps.scale,
                license:newProps.license,
                licenseImg:newProps.licenseImg,
                region:newProps.region,
                name:newProps.name,
                caType:newProps.caType,
                idCard:newProps.idCard,
                idCardImg:newProps.idCardImg,
                uploadIdCard:{id:"", text:"选择文件", errorTips:"", status:true},
                uploadLicense:{id:"", text:"选择文件", errorTips:"", status:true}
            })
        }
    }
    componentDidMount(){
        $(".certificationArea .region").citypicker();
        $(document).on("click",".city-picker-dropdown .city-select a",function(event){
            let region = $(".region").siblings(".city-picker-span").children(".title").text();
            if(!region){
                $(".certificationArea .region").siblings(".errorTips").html("* 输入项不能为空!");
            }else{
                $(".certificationArea .region").siblings(".errorTips").html("");
            }
            this.setState({region:region});
        }.bind(this));
    }
    setName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({name:_val});
    }
    checkName(){
        window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"));
    }
    caTypeSelectItem(eventKey){
        this.setState({caType:eventKey});
    }
    setIdCard(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCard:_val});
    }
    checkIdCard(){
        window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"));
    }
    setIdCardImg(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCardImg:_val});
    }
    submit(){
        let isOk = true;
        if(!window.GLOBAL.checkFun.isCompanyName($(".certificationArea .govName"),$(".certificationArea .govName").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isNotEmpty($(".certificationArea .address"),$(".certificationArea .address").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isGovLicense($(".certificationArea .license"),$(".certificationArea .license").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"))){
            isOk = false;
        }
        if(!this.state.uploadIdCard.status){
            isOk = false;
            if(!this.state.uploadIdCard.errorTips){
                let _upload0 = this.state.uploadIdCard;
                _upload0.errorTips = "* 输入项不能为空!";
                this.setState({uploadIdCard:_upload0})
            }
        }
        if(!this.state.scale){
            $("#scale").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(!this.state.caType){
            $("#caType").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(!this.state.region){
            isOk = false;
            $(".certificationArea .region").siblings(".errorTips").html("* 输入项不能为空!")
        }
        if(isOk){
            let parms = {
                devType:0,
                name:this.state.name,
                caType:this.state.caType,
                idCard:this.state.idCard,
                idCardImg:this.state.idCardImg,
                region:this.state.region,
                token:window.GLOBAL.token
            };
            window.GLOBAL.ajaxMap.setParms("addUserInfo",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("addUserInfo");
            ajax.success=function(r){
                if(r.code){
                    window.GLOBAL.action.certification.init();
                }
            }.bind(this);
            $.ajax(ajax);
        }
    }
    setGovName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({govName:_val});
    }
    checkGovName(){
        window.GLOBAL.checkFun.isCompanyName($(".certificationArea .govName"),$(".certificationArea .govName").siblings(".errorTips"));
    }
    setAddress(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({address:_val});
    }
    checkAddress(){
        window.GLOBAL.checkFun.isNotEmpty($(".certificationArea .address"),$(".certificationArea .address").siblings(".errorTips"));
    }
    scaleSelectItem(eventKey){
        this.setState({scale:eventKey});
    }
    setLicense(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({license:_val});
    }
    checkLicense(){
        window.GLOBAL.checkFun.isGovLicense($(".certificationArea .license"),$(".certificationArea .license").siblings(".errorTips"));
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        let caType = "";
        certification.caType.map((item,i) => {
            if(item.value===this.state.caType){
                caType = item.text;
                $("#caType").parent().siblings(".errorTips").html("");
            }
        });
        let scale = "";
        certification.scale.map((item,i) => {
            if(item.value===this.state.scale){
                scale = item.text;
                $("#scale").parent().siblings(".errorTips").html("");
            }
        });
        let options0 = {
            baseUrl:certification.idCardImgUpload.baseUrl,
            param:{
                uploadToken:window.GLOBAL.uploadToken
            },
            chooseAndUpload:true,
            accept: certification.idCardImgUpload.accept,
            beforeChoose:function(){
                console.log('=====START======')
            }.bind(this),
            chooseFile:function(files){
                console.log('=====chooseFile======');
                if(typeof files == "String"){
                    this.setState({idCardImg:files})
                }else{
                    this.setState({idCardImg:files[0].name})
                }
            }.bind(this),
            beforeUpload:function(files , mill){
                console.log('=====beforeUpload======');
                if(typeof files == "String"){
                    return true;
                }
                let _upload = {id:"",text:"选择图片",errorTips:"",status:false};
                if(files[0].size === 0){
                    _upload.errorTips = "* 图片大小为0,请重新上传!";
                    this.setState({uploadIdCard:_upload,idCardImg:""});
                    return false;
                }else if(files[0].size > certification.idCardImgUpload.maxSize){
                    _upload.errorTips = "* 图片过大,请重新上传!";
                    this.setState({uploadIdCard:_upload,idCardImg:""});
                    return false;
                }
                return true;
            }.bind(this),
            doUpload : function(files,mill,xhrID){
                console.log('=====doUpload======')
                let _upload = {id:xhrID,text:"上传中...",errorTips:"",status:false};
                this.setState({uploadIdCard:_upload});
            }.bind(this),
            uploadSuccess : function(resp){
                console.log('=====uploadSuccess======')
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传成功!",status:true};
                this.setState({uploadIdCard:_upload});
            }.bind(this),
            uploadError : function(err){
                console.log("uploadError===="+err.message)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({uploadIdCard:_upload,idCardImg:""});
            }.bind(this),
            uploadFail : function(resp){
                console.log("uploadFail===="+resp)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({uploadIdCard:_upload,idCardImg:""});
            }.bind(this)
        };
        let idCardImg ="";
        if(this.state.uploadIdCard.status&&this.state.idCardImg){
            idCardImg=(
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <img src={certification.idCardImgUpload.idCardImgUrl+this.state.idCardImg} width="246" height="155"/>
                </div>
            )
        }
        return (
            <div className="certificationArea">
                <ButtonToolbar>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">机构全称:</div>
                        <input className="govName" style={{width:"268px"}} value={this.state.govName} onChange={this.setGovName} onBlur={this.checkGovName} maxLength={certification.companyName.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">组织机构代码:</div>
                        <input className="license" style={{width:"268px"}} value={this.state.license} onChange={this.setLicense} onBlur={this.checkLicense} maxLength={20}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">所属地区:</div>
                        <div className="fl" style={{position:"relative"}}>
                            <input className="region" style={{width:"268px"}} readOnly type="text" data-toggle="city-picker" data-level={certification.region.level} placeholder={certification.region.placeholder} value={this.state.region}/>
                            <div className="errorTips"></div>
                        </div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">详细地址:</div>
                        <textarea className="address" style={{width:"268px"}} value={this.state.address} onChange={this.setAddress} onBlur={this.checkAddress} maxLength={certification.address.maxLength} placeholder={certification.address.placeholder}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">公司规模:</div>
                        <Dropdown id="scale">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={scale}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.scale.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.scaleSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">管理员真实姓名:</div>
                        <input className="name" style={{width:"268px"}} value={this.state.name} onChange={this.setName} onBlur={this.checkName} maxLength={certification.name.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件类型:</div>
                        <Dropdown id="caType">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={caType}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.caType.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.caTypeSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件号码:</div>
                        <input className="idCard" style={{width:"268px"}} value={this.state.idCard} onChange={this.setIdCard} onBlur={this.checkIdCard}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件照片:</div>
                        <input readOnly className="idCardImgUpload" value={this.state.idCardImg} onChange={this.setIdCardImg}/>
                        <FileUpload options={options0}>
                            <button ref="chooseAndUpload">{this.state.uploadIdCard.text}</button>
                        </FileUpload>
                        <div className="errorTips">{this.state.uploadIdCard.errorTips}</div>
                    </div>
                    {idCardImg}
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl"></div>
                        <Button onClick={this.submit}>确定</Button>
                    </div>
                </ButtonToolbar>
            </div>
        )
    }
}
class GovSpanInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            type:this.props.type,
            name:this.props.name,
            caType:this.props.caType,
            idCard:this.props.idCard,
            idCardImg:this.props.idCardImg,
            upload:{
                id:"",
                text:"选择文件",
                errorTips:"",
                status:this.props.type!==4?true:false
            }
        };
        this.setName =this.setName.bind(this);
        this.checkName = this.checkName.bind(this);
        this.caTypeSelectItem = this.caTypeSelectItem.bind(this);
        this.setIdCard = this.setIdCard.bind(this);
        this.checkIdCard =this.checkIdCard.bind(this);
        this.setIdCardImg =this.setIdCardImg.bind(this);
        this.submit = this.submit.bind(this);
    }
    componentWillReceiveProps(newProps){
        this.setState({
            region:newProps.region,
            name:newProps.name,
            caType:newProps.caType,
            idCard:newProps.idCard,
            idCardImg:newProps.idCardImg,
            upload:{id:"", text:"选择文件", errorTips:"", status:true}
        })
    }
    setName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({name:_val});
    }
    checkName(){
        window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"));
    }
    caTypeSelectItem(eventKey){
        this.setState({caType:eventKey});
    }
    setIdCard(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCard:_val});
    }
    checkIdCard(){
        window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"));
    }
    setIdCardImg(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({idCardImg:_val});
    }
    submit(){
        let isOk = true;
        if(!window.GLOBAL.checkFun.isCompanyAdmin($(".certificationArea .name"),$(".certificationArea .name").siblings(".errorTips"))){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isIdentityCodeValid($(".certificationArea .idCard"),$(".certificationArea .idCard").siblings(".errorTips"))){
            isOk = false;
        }
        if(!this.state.upload.status){
            isOk = false;
            if(!this.state.upload.errorTips){
                let _upload = this.state.upload;
                _upload.errorTips = "* 输入项不能为空!";
                this.setState({upload:_upload})
            }
        }
        if(!this.state.caType){
            $("#caType").parent().siblings(".errorTips").html("* 输入项不能为空!");
        }
        if(isOk){
            let parms = {
                devType:0,
                name:this.state.name,
                caType:this.state.caType,
                idCard:this.state.idCard,
                idCardImg:this.state.idCardImg,
                region:this.state.region,
                token:window.GLOBAL.token
            };
            window.GLOBAL.ajaxMap.setParms("addUserInfo",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("addUserInfo");
            ajax.success=function(r){
                if(r.code){
                    window.GLOBAL.action.certification.init();
                }
            }.bind(this);
            $.ajax(ajax);
        }
    }
    render(){
        let certification = window.GLOBAL.pageData.certification;
        let caType = "";
        certification.caType.map((item,i) => {
            if(item.value===this.state.caType){
                caType = item.text;
                $("#caType").parent().siblings(".errorTips").html("");
            }
        });
        let options = {
            baseUrl:certification.idCardImgUpload.baseUrl,
            param:{
                uploadToken:window.GLOBAL.uploadToken
            },
            chooseAndUpload:true,
            accept: certification.idCardImgUpload.accept,
            beforeChoose:function(){
                console.log('=====START======')
            }.bind(this),
            chooseFile:function(files){
                console.log('=====chooseFile======');
                if(typeof files == "String"){
                    this.setState({idCardImg:files})
                }else{
                    this.setState({idCardImg:files[0].name})
                }
            }.bind(this),
            beforeUpload:function(files , mill){
                console.log('=====beforeUpload======');
                if(typeof files == "String"){
                    return true;
                }
                let _upload = {id:"",text:"选择图片",errorTips:"",status:false};
                if(files[0].size === 0){
                    _upload.errorTips = "* 图片大小为0,请重新上传!";
                    this.setState({upload:_upload,idCardImg:""});
                    return false;
                }else if(files[0].size > certification.idCardImgUpload.maxSize){
                    _upload.errorTips = "* 图片过大,请重新上传!";
                    this.setState({upload:_upload,idCardImg:""});
                    return false;
                }
                return true;
            }.bind(this),
            doUpload : function(files,mill,xhrID){
                console.log('=====doUpload======')
                let _upload = {id:xhrID,text:"上传中...",errorTips:"",status:false};
                this.setState({upload:_upload});
            }.bind(this),
            uploadSuccess : function(resp){
                console.log('=====uploadSuccess======')
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传成功!",status:true};
                this.setState({upload:_upload});
            }.bind(this),
            uploadError : function(err){
                console.log("uploadError===="+err.message)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({upload:_upload,idCardImg:""});
            }.bind(this),
            uploadFail : function(resp){
                console.log("uploadFail===="+resp)
                let _upload = {id:"",text:"选择图片",errorTips:"* 上传失败,请重新上传!",status:false};
                this.setState({upload:_upload,idCardImg:""});
            }.bind(this)
        }
        let img ="";
        if(this.state.upload.status&&this.state.idCardImg){
            img=(
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl"></div>
                    <img src={certification.idCardImgUpload.idCardImgUrl+this.state.idCardImg} width="246" height="155"/>
                </div>
            )
        }
        return (
            <div className="certificationArea">
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">开发者类型:</div>
                    <div className="fl">政府机关或事业单位</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">机构全称:</div>
                    <div className="fl">{this.props.govName}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">组织机构代码:</div>
                    <div className="fl">{this.props.license}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">所属地区:</div>
                    <div className="fl">{this.props.region}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">详细地址:</div>
                    <div className="fl">{this.props.address}</div>
                </div>
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">机构规模:</div>
                    <div className="fl">{this.props.scale}</div>
                </div>
                <ButtonToolbar>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">管理员真实姓名:</div>
                        <input className="name" style={{width:"268px"}} value={this.state.name} onChange={this.setName} onBlur={this.checkName} maxLength={certification.name.maxLength}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件类型:</div>
                        <Dropdown id="caType">
                            <Dropdown.Toggle>
                                <input className="text" placeholder="请选择" style={{minWidth:"208px",backgroundColor:"transparent",border:"0"}} disabled value={caType}/>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {certification.caType.map((item,i) => (
                                    <MenuItem eventKey={item.value} key={i} onSelect={this.caTypeSelectItem}>{item.text}</MenuItem>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件号码:</div>
                        <input className="idCard" style={{width:"268px"}} value={this.state.idCard} onChange={this.setIdCard} onBlur={this.checkIdCard}/>
                        <div className="errorTips"></div>
                    </div>
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl">证件照片:</div>
                        <input readOnly className="idCardImgUpload" value={this.state.idCardImg} onChange={this.setIdCardImg}/>
                        <FileUpload options={options}>
                            <button ref="chooseAndUpload">{this.state.upload.text}</button>
                        </FileUpload>
                        <div className="errorTips">{this.state.upload.errorTips}</div>
                    </div>
                    {img}
                    <div className="inputLine fl" style={{width:"100%"}}>
                        <div className="fl"></div>
                        <Button onClick={this.submit}>确定</Button>
                    </div>
                </ButtonToolbar>
            </div>
        )
    }
}

class certification extends Reflux.Component{
    constructor(props) {
        super(props);
        this.state = {};
        this.store = window.GLOBAL.store.certification;
        this.setDevType = this.setDevType.bind(this);
    }
    componentDidMount(){
        window.GLOBAL.action.certification.init();
    }
    setDevType(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value*1;
        window.GLOBAL.action.certification.setType(this.state.type,_val);
    }
    render() {
        let certification = window.GLOBAL.pageData.certification;
        let radio ="";
        if(certification.type[this.state.type].isSHowRadio){
            radio =(
                <div className="inputLine fl" style={{width:"100%"}}>
                    <div className="fl">开发者类型:</div>
                    <ul>
                        <li>
                            <Button className={this.state.devType===0&&this.state.type!==8?"circle active":"circle"} onClick={this.setDevType} value={0} disabled={this.state.type===8?true:false}></Button>
                            <span>个人</span>
                        </li>
                        <li>
                            <Button className={this.state.devType===1?"circle active":"circle"} onClick={this.setDevType} value={1}></Button>
                            <span>公司</span>
                        </li>
                        <li>
                            <Button className={this.state.devType===2?"circle active":"circle"} onClick={this.setDevType} value={2}></Button>
                            <span>政府机关或事业单位</span>
                        </li>
                    </ul>
                </div>
            );
        }
        let dom = "";
        let person = {
            region:this.state.region,
            name:this.state.name,
            caType:this.state.caType,
            idCard:this.state.idCard,
            idCardImg:this.state.idCardImg
        };
        let company = {
            companyName:this.state.companyName,
            region:this.state.region,
            address:this.state.address,
            companyType:this.state.companyType,
            trade:this.state.trade,
            scale:this.state.scale,
            license:this.state.license,
            licenseImg:this.state.licenseImg,
            name:this.state.name,
            caType:this.state.caType,
            idCard:this.state.idCard,
            idCardImg:this.state.idCardImg
        };
        let government = {
            govName:this.state.govName,
            license:this.state.license,
            region:this.state.region,
            address:this.state.address,
            scale:this.state.scale,
            name:this.state.name,
            caType:this.state.caType,
            idCard:this.state.idCard,
            idCardImg:this.state.idCardImg
        };
        if(certification.type[this.state.type].showType===0){
            if(this.state.devType===0){
                dom = (<PersonInput {...person} type={this.state.type} />)
            }else if(this.state.devType===1){
                dom = (<CompanyInput {...company} type={this.state.type} />);
            }else if(this.state.devType===2){
                dom = (<GovInput {...government} type={this.state.type} />);
            }
        }else if(certification.type[this.state.type].showType===1){
            if(this.state.devType===0){
                dom = (<PersonSpan {...person} type={this.state.type} />)
            }else if(this.state.devType===1){
                dom = (<CompanySpan {...company} type={this.state.type} />);
            }else if(this.state.devType===2){
                dom = (<GovSpan {...government} type={this.state.type} />);
            }
        }else if(certification.type[this.state.type].showType===2){
            if(this.state.devType===1){
                dom = (<CompanySpanInput {...company} type={this.state.type} />);
            }else if(this.state.devType===2){
                dom = (<GovSpanInput {...government} type={this.state.type} />);
            }
        }
        return (
            <div className="certification">
                <div className="MainTitle">
                    <h1>{certification.title}</h1>
                    <div className="fr">状态: {certification.type[this.state.type].tips}</div>
                </div>
                <div className="whiteSpace">
                    <div className="box">
                        {radio}
                        {dom}
                    </div>
                </div>
            </div>
        )
    }
}

module.exports = certification;