/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import { Button } from 'react-bootstrap'
import Tips from '../componentUI/js/tips'
import CheckTips from '../componentUI/js/checkTips'
class accountManage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            userName:"",
            phone:"",
            email:"",
            isDisabled:true,
            btnName:"修改",
            showTips:0,
            loginEmail:""
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.reLoad = this.reLoad.bind(this);
        this.goToEmail = this.goToEmail.bind(this);
        this.showCheckTips = this.showCheckTips.bind(this);
        this.checkNewPhone = this.checkNewPhone.bind(this);
    }
    componentDidMount(){
        window.GLOBAL.ajaxMap.setParm("getAccountInfo",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("getAccountInfo");
        parms.success=function(r){
            this.setState({
                userName:r.userName,
                phone:r.phone,
                email:r.email
            });
        }.bind(this);
        $.ajax(parms);
    }
    handleClick(){
        if(this.state.isDisabled){
            this.setState({isDisabled:false,btnName:"提交"});
        }else{
            let check = window.GLOBAL.checkFun.isEmail($(".accountManage .email"),$(".accountManage .email").siblings(".errorTips"));
            if(check){
                let parms = {email:this.state.email,token:window.GLOBAL.token};
                window.GLOBAL.ajaxMap.setParms("modfiyEmail",parms);
                let ajax = window.GLOBAL.ajaxMap.getAjaxParms("modfiyEmail");
                ajax.success=function(r){
                    if(r.code){
                        if(r.url){
                            this.setState({showTips:1,isDisabled:true,btnName:"修改",loginEmail:r.url})
                        }else{
                            this.setState({isDisabled:true,btnName:"修改"})
                        }
                    }else{
                        this.setState({showTips:2})
                    }
                }.bind(this);
                ajax.error = function(){
                    this.setState({showTips:2});
                };
                $.ajax(ajax);
            }
        }
    }
    goToEmail(){
        this.setState({showTips:0});
        window.open("http://"+this.state.loginEmail);
    }
    handleCancel(){
        this.setState({showTips:0});
    }
    reLoad(){
        window.location.reload();
    }
    handleChange(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({email:_val});
    }
    handleBlur(){
        window.GLOBAL.checkFun.isEmail($(".accountManage .email"),$(".accountManage .email").siblings(".errorTips"));
    }
    showCheckTips(){
        this.setState({showTips:3})
    }
    checkNewPhone(_obj){
        let parms = {phone:_obj.state.phone,key:_obj.state.key,token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("modfiyPhone",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("modfiyPhone");
        ajax.success=function(r){
            if(r.code){
                this.setState({phone:_obj.state.phone,showTips:0})
            }
        }.bind(this);
        $.ajax(ajax);
    }
    render() {
        let accountManage = window.GLOBAL.pageData.accountManage;
        let tips;
        if(this.state.showTips===1){
            tips = (<Tips onSubmit={this.goToEmail} onCancel={this.handleCancel} text={accountManage.loginEmail}/>)
        }else if(this.state.showTips===2){
            tips = (<Tips onSubmit={this.reLoad} text={accountManage.reLoadTips} btns={[{text:"确定",type:1}]}/>)
        }else if(this.state.showTips===3){
            tips = (<CheckTips onSubmit={this.checkNewPhone} onCancel={this.handleCancel} title={accountManage.checkTips.title} btns={accountManage.checkTips.btns}/>)
        }
        return (
            <div className="accountManage">
                <div className="MainTitle">
                    <h1>{accountManage.title}</h1>
                </div>
                <div className="whiteSpace">
                    <div className="box">
                        <div className="inputLine fl" style={{width:"100%"}}>
                            <div className="fl">用户名:</div>
                            <div className="fl">{this.state.userName}</div>
                        </div>
                        <div className="inputLine fl" style={{width:"100%"}}>
                            <div className="fl">手机号码:</div>
                            <div className="fl">{this.state.phone}</div>
                            <Button className="fl" onClick={this.showCheckTips}>修改</Button>
                        </div>
                        <div className="inputLine fl" style={{width:"100%"}}>
                            <div className="fl">邮箱:</div>
                            <input className="fl email" value={this.state.email} disabled={this.state.isDisabled} onChange={this.handleChange} onBlur={this.handleBlur}/>
                            <Button className="fl" onClick={this.handleClick}>{this.state.btnName}</Button>
                            <div className="fl" className="errorTips"></div>
                        </div>
                    </div>
                </div>
                {tips}
            </div>
        )
    }
}

module.exports = accountManage;