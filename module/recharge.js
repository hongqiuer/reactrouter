/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import { Button } from 'react-bootstrap'
import Input from '../componentUI/js/Input'
import MD5 from "md5"

class recharge extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            status:-1,
            otherMoney:"",
            money:0
        };
        this.setMoney = this.setMoney.bind(this);
        this._setInputVal = this._setInputVal.bind(this);
        this.submit = this.submit.bind(this);
    }
    setMoney(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value*1;
        if(this.state.status!=_val){
            this.setState({status:_val,money:window.GLOBAL.pageData.recharge.status[_val],otherMoney:""});
        }else{
            this.setState({status:-1,money:0,otherMoney:""});
        }
    }
    _setInputVal(_obj){
        if(_obj.state.v){
            this.setState({status:100,money:_obj.state.v*1,otherMoney:_obj.state.v*1});
        }else if(this.state.status===100){
            this.setState({status:-1,money:0,otherMoney:""});
        }
    }
    submit(){
        let parms = {
            money:this.state.money,
            token:window.GLOBAL.token,
            key1:MD5(this.state.money+window.GLOBAL.token+"devPortal")
        };
        window.GLOBAL.ajaxMap.setParms("postToPay",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("postToPay");
        ajax.success=function(r){
            if(r.code){
                window.location.href = r.url;
            }
        }.bind(this);
        $.ajax(ajax);
    }
    render() {
        let recharge = window.GLOBAL.pageData.recharge;
        return (
            <div>
                {this.props.children?(this.props.children):(<div className="recharge">
                    <div className="MainTitle">
                        <h1>{recharge.title}</h1>
                    </div>
                    <div className="whiteSpace">
                        <ul className="bigCircleProgress">
                            <li className="bigCircle select">
                                <div className="text">输入<br/>充值金额</div>
                                <div className="num">1</div>
                                <div className="circleSmall">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </li>
                            <li className="bigCircle">
                                <div className="text">进行支付</div>
                                <div className="num">2</div>
                                <div className="circleSmall">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </li>
                            <li className="bigCircle">
                                <div className="text">充值成功</div>
                                <div className="num">3</div>
                            </li>
                        </ul>
                        <div className="inputLine">
                            <span>充值金额：</span>
                            {recharge.status.map((item,i) => (
                                <button key={i} onClick={this.setMoney} className={this.state.status===i?"btn btn-other select":"btn btn-other"} value={i}>{item}元</button>
                            ))}
                        </div>
                        <div className="inputLine">
                            <span>其他金额：</span>
                            <Input className="moneyInput" type="positiveInt" v={this.state.otherMoney} onBlur={this._setInputVal} maxValue={recharge.moneyInput.maxValue} maxLength={recharge.moneyInput.maxLength} placeholder={recharge.moneyInput.placeholder}/>
                        </div>
                        <div className="inputLine">
                            <span>支付金额：<font color="red">{this.state.money}.00</font>元</span>
                        </div>
                        <div className="inputLine center">
                            <Button bsStyle="primary" onClick={this.submit} disabled={this.state.status===-1?true:false}>确认支付</Button>
                        </div>
                    </div>
                </div>)}
            </div>
        )
    }
}
module.exports = recharge;