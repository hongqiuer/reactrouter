/**
 * Created by hqer on 2016/12/8.
 */
import React from 'react'
import ReactDOM from 'react-dom'
import { Link,browserHistory } from 'react-router'
import { Button,Row,Col } from 'react-bootstrap'
import Tips from '../componentUI/js/tips'



class Dec extends React.Component{
    componentDidMount(){
        let _el = ReactDOM.findDOMNode(this);
        let text = this.props.data.text.replace(/《/g, "<").replace(/》/g, ">").replace(/&#39;/g, "\"");
        $(_el).find(".showLink").html(text);
    }
    render(){
        return(
            <div className={this.props.className}>
                <span>{this.props.data.type}: </span>
                <sapn className="showLink"></sapn>
            </div>
        )
    }
};

class myServer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            role: "",
            status: "",
            count:[],
            ability:[],
            tabIndex: 0,
            levelIndex:[0,0],
            showTips:-1
        };
        this._getInfo = this._getInfo.bind(this);
        this._getCount = this._getCount.bind(this);
        this._getAbility = this._getAbility.bind(this);
        this.handleTabClick = this.handleTabClick.bind(this);
        this.handleLevelClick = this.handleLevelClick.bind(this);
        this.handleShowTips = this.handleShowTips.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSetTest = this.handleSetTest.bind(this);
        this.handleTurnPage = this.handleTurnPage.bind(this);
        this.handleTurnOtherPage = this.handleTurnOtherPage.bind(this);
    }
    componentDidMount() {
        this._getInfo();
        this._getCount();
        this._getAbility();
    }
    _getInfo() {
        window.GLOBAL.ajaxMap.setParm("myServerInfo",'token',window.GLOBAL.token);
        let params = window.GLOBAL.ajaxMap.getAjaxParms("myServerInfo");
        params.success=function(data){
            this.setState({
                username: data.username,
                role: data.role,
                status: data.status,
                showTips:data.isFirst===1?1:this.state.showTips
            });
        }.bind(this);
        $.ajax(params);
    }
    _getCount() {
        let params = window.GLOBAL.ajaxMap.getAjaxParms("myServerCount");
        params.success=function(data){
            this.setState({
                count: data.data
            });
        }.bind(this);
        $.ajax(params);
    }
    _getAbility() {
        let params = window.GLOBAL.ajaxMap.getAjaxParms("myServerAbility");
        params.success=function(data){
            this.setState({
                ability: data.data
            });
        }.bind(this);
        $.ajax(params);
    }
    handleTabClick(event) {
        let _index = event.srcElement?event.srcElement.value:event.target.value;
        let _list = Array.from(this.state.levelIndex, (x) => 0);
        this.setState({
            tabIndex: _index*1,
            levelIndex:_list
        });
    }
    handleLevelClick(event){
        let _i = event.srcElement?event.srcElement.value:event.target.value;
        let _v = this.state.levelIndex;
        if(_v[_i]===0){
            _v[_i] = 1;
        }else if(_v[_i]===1){
            _v[_i] = 0;
        }
        this.setState({levelIndex: _v});
    }
    handleShowTips(){
        this.setState({showTips:0});
    }
    handleSetTest(event){
        //    触发立即试用
        let _eaId = event.srcElement?event.srcElement.value:event.target.value;
        let parms = {type:0,eaId:_eaId,token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("openAbility",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("openAbility");
        ajax.success=function(data){
            if(data.code){
                this._getAbility();
            }
        }.bind(this);
        $.ajax(ajax);
    }
    handleTurnPage(event){
        //    触发正式开通
        let _eaId = event.srcElement?event.srcElement.value:event.target.value;
        let parms = {type:1,eaId:_eaId, token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("openAbility",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("openAbility");
        ajax.success=function(data){
            if(data.code){
                window.location.href= data.url;
            }
        }.bind(this);
        $.ajax(ajax);
    }
    handleTurnOtherPage(event){
        //    触发立即使用
        let _eaId = event.srcElement?event.srcElement.value:event.target.value;
        let parms = {type:2,eaId:_eaId, token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("openAbility",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("openAbility");
        ajax.success=function(data){
            if(data.code){
                window.location.href= data.url;
            }
        }.bind(this);
        $.ajax(ajax);
    }
    goToCertification(){
        browserHistory.push(window.GLOBAL.pageData.leftMenu.item3.child[2].url);
    }
    handleCancel(){
        this.setState({showTips:-1});
    }
    render() {
        let myServer = window.GLOBAL.pageData.myServer;
        let tips;
        if(this.state.showTips===0){
            tips = (<Tips {...myServer.Unautherized} onSubmit={this.goToCertification} onCancel={this.handleCancel}/>)
        }else if(this.state.showTips===1){
            tips = (<Tips {...myServer.firstLogin} onSubmit={this.goToCertification} onCancel={this.handleCancel}/>)
        }
        let count = [];
        {this.state.count.map((item,i) => {
            count.push(<div className={"countArea fl type"+this.state.count.length} style={{width:((100/this.state.count.length)+"%")}} key={i}>
                <div className={myServer.logoType[item.logoType]}></div>
                <div className="fl line">
                    <a title={item.num}><span className={item.logoType===0?"money":"num"}>{item.num}</span></a>
                    <span>{item.unit}</span>
                </div>
                <div className="fl line">
                    {item.text}
                </div>
            </div>)
        })}
        let abilityArr = this.state.ability.filter(function(item){
            //tabIndex: 0->全部 1->正在试用 2->开通成功 /<--->/ status: 0->无状态 1->正在试用 2->开通成功 3->冻结
            if(this.state.tabIndex == 0) return true;
            return (item.status+0) == this.state.tabIndex;
        }.bind(this));

        let abilityList0 = [];
        let abilityList1 = [];
        abilityArr.map((item,i)=>{
            if(item.level===0){
                let desList = [];
                let btnList = [];
                if(item.des.length){
                    item.des.map((des,j)=>{
                        desList.push(<Dec className="dec fl" key={j} data={des}/>);
                    });
                }
                if(item.btns.length){
                    item.btns.map((btn,j)=>{
                        if(this.state.status==="未认证"&&btn.text==="正式开通"){
                            btnList.push(<Button key={j} onClick={this.handleShowTips}>{btn.text}</Button>)
                        }else if(item.status===3){
                            btnList.push(<Button key={j} disabled>{btn.text}</Button>)
                        }
                        else if(btn.type===3&&btn.text==="购买套餐"){
                            btnList.push(<Link key={j} role="button" to={btn.url} className="btn btn-warning">{btn.text}</Link>)
                        }else if(btn.type===3&&btn.text==="查看订单"){
                            btnList.push(<Link key={j} role="button" to={btn.url} className="btn btn-danger">{btn.text}</Link>)
                        }else if(btn.type===1&&btn.text==="立即试用"){
                            btnList.push(<Button key={j} bsStyle="success" onClick={this.handleSetTest} value={btn.url}>{btn.text}</Button>)
                        }else if(btn.type===1&&btn.text==="正式开通"){
                            btnList.push(<Button key={j} bsStyle="primary" onClick={this.handleTurnPage} value={btn.url}>{btn.text}</Button>)
                        }
                    });
                }
                abilityList0.push(<li className="item" key={i}>
                    <img className="logo" src={"/img/"+item.eaId+".png"}/>
                    <div className="content">
                        <div className="title3 fl">{item.name}</div>
                        {item.status===0?"":(
                            <div className={"fl icon type"+item.status}>
                                <div className="iconBg"></div>
                                <div className="iconText">{myServer.iconText[item.status]}</div>
                            </div>
                        )}
                        {desList}
                        <div className="btnList fl">
                            {btnList}
                        </div>
                    </div>
                </li>);
            }else if(item.level===1){
                let desList = [];
                let btnList = [];
                if(item.des.length){
                    item.des.map((des,j)=>{
                        desList.push(<Dec className="dec fl" key={j} data={des}/>);
                    });
                }
                if(item.btns.length){
                    item.btns.map((btn,j)=>{
                        if(btn.type===1&&btn.text==="立即使用"){
                            btnList.push(<Button key={j} bsStyle="success" onClick={this.handleTurnOtherPage} value={btn.url}>{btn.text}</Button>)
                        }
                    });
                }
                abilityList1.push(<li className="item" key={i}>
                    <img className="logo fl" src={"/img/"+item.eaId+".png"}/>
                    <div className="content fl">
                        <div className="title3 fl">{item.name}</div>
                        {item.status===0?"":(
                            <div className="icon fl">
                                <div className="iconBg"></div>
                                <div className="iconText">{myServer.iconText[item.status]}</div>
                            </div>
                        )}
                        {desList}
                        <div className="btnList fl">
                            {btnList}
                        </div>
                    </div>
                </li>);
            }
        });

        return (
            <div className="myServer">
                <div className="MainTitle">
                    <h1>{myServer.title}</h1>
                </div>
                <div className="whiteSpace">
                    <div className="fl" style={{width:"30%"}}>
                        <div className="fl imgArea"><img src={myServer.img} /></div>
                        <div className="fl userNameArea" style={{marginLeft:"12px",maxWidth:"150px"}}>
                            <a title={this.state.username}><div className="userName">{this.state.username}</div></a>
                            <div>当前角色：{this.state.role}</div>
                            <div>状态：{this.state.status}</div>
                        </div>
                    </div>
                    <div className="fl" style={{width:"70%"}}>
                        {count}
                    </div>
                </div>
                <div className="whiteSpace">
                    <h1>能力管理</h1>
                    <div className="serverManageType">
                        <Button value={0} bsStyle={this.state.tabIndex===0?"primary":"link"} onClick={this.handleTabClick}>全部</Button>
                        <Button value={1} bsStyle={this.state.tabIndex===1?"primary":"link"} onClick={this.handleTabClick}>正在试用</Button>
                        <Button value={2} bsStyle={this.state.tabIndex===2?"primary":"link"} onClick={this.handleTabClick}>开通成功</Button>
                    </div>
                    <div style={{display:abilityList0.length===0?"none":"block"}}>
                        <Button className="itemTitle" bsStyle="warning" value={0} onClick={this.handleLevelClick}>
                            <span className="fl">{myServer.level[0]}</span>
                            <span className={this.state.levelIndex[0]===1?"fr glyphicon glyphicon-chevron-up":"fr glyphicon glyphicon-chevron-down"}></span>
                        </Button>
                        <ul className="abilityList" style={{display:this.state.levelIndex[0]===1?"none":"block"}}>{abilityList0}</ul>
                    </div>
                    <div style={{display:abilityList1.length===0?"none":"block"}}>
                        <Button className="itemTitle" bsStyle="warning" value={1} onClick={this.handleLevelClick}>
                            <span className="fl">{myServer.level[1]}</span>
                            <span className={this.state.levelIndex[1]===1?"fr glyphicon glyphicon-chevron-up":"fr glyphicon glyphicon-chevron-down"}></span>
                        </Button>
                        <ul className="abilityList" style={{display:this.state.levelIndex[1]===1?"none":"block"}}>{abilityList1}</ul>
                    </div>
                </div>
                {tips}
            </div>
        );
    }
}




module.exports = myServer;
