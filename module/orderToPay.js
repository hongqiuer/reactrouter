/**
 * Created by hqer on 2017/2/9.
 */
import React from 'react'
import { Link } from 'react-router'
import { Table,Button } from 'react-bootstrap'
import Tips from '../componentUI/js/tips'
import MD5 from "md5"

class orderToPay extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            ability:"",
            data:[],
            validity:"",
            totalNorms:"",
            totalCost:0,
            payWays:[],
            payWay:-1,
            showTips:false
        };
        this.setPayWay = this.setPayWay.bind(this);
        this.submit = this.submit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    componentDidMount(){
        let parms = {
            orderId:this.props.params.orderId,
            token:window.GLOBAL.token
        };
        window.GLOBAL.ajaxMap.setParms("orderItemDetail",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("orderItemDetail");
        ajax.success=function(r){
            let _data = [];
            let _totalNorms = 0;
            if(Object.prototype.toString.call(r.data.name)==='[object Array]'){
                r.data.name.forEach((x,i) =>{
                    _data.push({name:x,norms:r.data.norms[i],price:r.data.price[i]+"元",num:r.data.num[i].replace(/[^0-9]/ig,"")});
                    _totalNorms = _totalNorms + parseInt(r.data.norms[i])*(r.data.num[i].replace(/[^0-9]/ig,""));
                });
                _totalNorms = _totalNorms + r.data.norms[0].replace(/[0-9]/ig,"");
            }else{
                _data.push({name:r.data.name,norms:r.data.norms,price:r.data.price+"元",num:r.data.num.replace(/[^0-9]/ig,"")});
                _totalNorms = _totalNorms + r.data.norms.replace(/[0-9]/ig,"");
            }
            this.setState({
                ability:r.data.ability,
                validity:r.validity,
                data:_data,
                totalCost:r.cost,
                totalNorms:_totalNorms
            });
            let parms1 = {
                orderId:this.props.params.orderId,
                token:window.GLOBAL.token
            };
            window.GLOBAL.ajaxMap.setParms("getPayWay",parms1);
            let ajax1 = window.GLOBAL.ajaxMap.getAjaxParms("getPayWay");
            ajax1.success=function(r){
                this.setState({
                    payWays:r.data
                });
            }.bind(this);
            $.ajax(ajax1);
        }.bind(this);
        $.ajax(ajax);
    }
    setPayWay(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val;
        if(!_eT.value){
            _val = $(_eT).parents("li.payItem").attr("value")*1;
        }else{
            _val = _eT.value*1;
        }
        this.setState({payWay:_val});
    }
    submit(){
        if(this.state.payWay===-1){
            this.setState({showTips:true});
        }else{
            let parms = {
                orderId:this.props.params.orderId,
                cost:this.state.totalCost,
                payWayId:this.state.payWays[this.state.payWay].payWayId,
                token:window.GLOBAL.token,
                key1:MD5(this.props.params.orderId+this.state.totalCost+this.state.payWays[this.state.payWay]+window.GLOBAL.token+"devPortal")
            };
            window.GLOBAL.ajaxMap.setParms("postOrderToPay",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("postOrderToPay");
            ajax.success=function(r){
                if(r.code){
                    window.location.href=r.url;
                }
            }.bind(this);
            $.ajax(ajax);
        }
    }
    handleCancel(){
        this.setState({showTips:false})
    }
    render() {
        let orderToPay = window.GLOBAL.pageData.orderToPay;
        return (
            <div className="order">
                <div className="whiteSpace">
                    <ul className="bigCircleProgress">
                        <li className="bigCircle select">
                            <div className="text">选择套餐</div>
                            <div className="num">1</div>
                        </li>
                        <li className="bigCircle select">
                            <div className="text">确认支付</div>
                            <div className="num">2</div>
                        </li>
                        <li className="bigCircle">
                            <div className="text">支付成功</div>
                            <div className="num">3</div>
                        </li>
                    </ul>
                    <div className="title">订单详情</div>
                    <div className="inputLine">所属能力: {this.state.ability}</div>
                    <div className="inputLine">套餐详情:</div>
                    <div className="inputLine">
                        <Table striped responsive className="basicList" style={{borderTop:"1px solid #ddd"}}>
                            <thead>
                            <tr>
                                {orderToPay.listTitle.map((item,i) => (
                                    <th className={"th"+i} key={i}>{item}</th>
                                ))}
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.data.map((element,index) => (
                                <tr className={index%2===0?"bgGray":""} key={index}>
                                    {orderToPay.showKey.map((item,i) => (
                                        <td key={i} className={"th"+i}>
                                            <div className="ShowTxt">{element[item]}</div>
                                        </td>
                                    ))}
                                </tr>
                            ))}
                            </tbody>
                        </Table>
                    </div>
                    <div className="inputLine">
                        有效期: {this.state.validity}
                    </div>
                    <div className="inputLine">
                        总计: {this.state.totalNorms}  {this.state.totalCost}元
                    </div>
                    <div className="title">选择支付方式</div>
                    <ul className="payList">
                        {this.state.payWays.map((element,index) => (
                            element.payName==="账户余额"?(
                                element.userMoeny < this.state.totalCost?(
                                    <li key={index} className="payItem gray fl" style={{width:"100%"}}>
                                        <Button className="circle fl" disabled></Button>
                                        <div className="icon-account fl"></div>
                                        <div className="fl">{element.payName} {element.userMoeny}元</div>
                                        <div className="fr"><font color="red">{element.userMoeny < 0?"余额为负,请先 ":"余额不足,请先 "}</font><Link to="/recharge">充值</Link> </div>
                                    </li>
                                ):(
                                    <li key={index} style={{width:"100%"}} className={this.state.payWay===index?"payItem select fl":"payItem fl"} onClick={this.setPayWay} value={index}>
                                        <Button className={this.state.payWay===index?"circle active fl":"circle fl"}></Button>
                                        <div className="icon-account fl"></div>
                                        <div className="fl">{element.payName} {element.userMoeny}元</div>
                                        <div className="fr">支付<font color="red">{this.state.totalCost}</font>元</div>
                                    </li>
                                )
                            ):(
                                <li key={index} style={{width:"100%"}} className={this.state.payWay===index?"payItem select fl":"payItem fl"} onClick={this.setPayWay} value={index}>
                                    <Button className={this.state.payWay===index?"circle active fl":"circle fl"}></Button>
                                    <div className="icon_Third_party fl"></div>
                                    <div className="fl">{element.payName}</div>
                                </li>
                            )
                        ))}
                    </ul>
                    <div className="inputLine">
                        <Button onClick={this.submit}>确认支付</Button>
                    </div>
                </div>
                {this.state.showTips?<Tips {...orderToPay.tips} onCancel={this.handleCancel}/>:""}
            </div>
        )
    }
}
module.exports = orderToPay;