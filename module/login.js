/**
 * Created by hqer on 2016/12/8.
 */
import React from 'react'
import { Button } from 'react-bootstrap'
import { Link,browserHistory } from 'react-router'
import CheckBox from '../componentUI/js/checkBox'
import MD5 from "md5"

class login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            s:false,
            imgUrl:window.GLOBAL.pageData.checkTips.img+"?"+new Date().getTime(),
            imgCode:"",
            userName:"",
            passWord:""
        };
        this.handleClick = this.handleClick.bind(this);
        this.reloadImg = this.reloadImg.bind(this);
        this.setUserName = this.setUserName.bind(this);
        this.checkUserName = this.checkUserName.bind(this);
        this.setPassWord = this.setPassWord.bind(this);
        this.checkPassWord = this.checkPassWord.bind(this);
        this.setImgCode = this.setImgCode.bind(this);
        this.checkImgCode = this.checkImgCode.bind(this);
        this.submit = this.submit.bind(this);
        this._getKey = this._getKey.bind(this);
    }
    componentWillMount(){
        if(!window.GLOBAL.promise["getKey"]){
            window.GLOBAL.promise["getKey"] = $.Deferred();
        }
    }
    handleClick(){
        if(this.state.s){
            this.setState({s:false});
        }else{
            this.setState({s:true});
        }
    }
    reloadImg(){
        let t = new Date().getTime();
        this.setState({imgUrl:window.GLOBAL.pageData.checkTips.img+"?" + t,imgCode:""});
    }
    setUserName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({userName:_val});
    }
    checkUserName(){
        window.GLOBAL.checkFun.isNotEmpty($(".loginArea .userName"),$(".loginArea .userName").siblings(".errorTips"),"* 用户名不能为空");
    }
    setPassWord(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({passWord:_val});
    }
    checkPassWord(){
        window.GLOBAL.checkFun.isNotEmpty($(".loginArea .passWord"),$(".loginArea .passWord").siblings(".errorTips"),"* 密码不能为空");
    }
    setImgCode(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({imgCode:_val});
    }
    checkImgCode(){
        window.GLOBAL.checkFun.isNotEmpty($(".loginArea .imgCode"),$(".loginArea .imgCode").siblings(".errorTips"),"* 验证码不能为空");
    }
    _getKey(){
        window.GLOBAL.ajaxMap.setParm("getRsaKey","token",window.GLOBAL.token);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("getRsaKey");
        ajax.success=function(r){
            window.GLOBAL.promise["getKey"].resolve(r);
        }.bind(this);
        $.ajax(ajax);
    }
    submit(){
        let isOk = true;
        if(this.state.userName.indexOf("@")>0){
            isOk = window.GLOBAL.checkFun.isEmail($(".loginArea .userName"),$(".loginArea .userName").siblings(".errorTips"),"* 用户名或密码错误");
        }else{
            isOk = window.GLOBAL.checkFun.isUserName($(".loginArea .userName"),$(".loginArea .userName").siblings(".errorTips"),"* 用户名或密码错误");
        }
        if(!window.GLOBAL.checkFun.isPassWord($(".loginArea .passWord"),$(".loginArea .passWord").siblings(".errorTips"),"* 用户名或密码错误")){
            isOk = false;
        }
        if(!window.GLOBAL.checkFun.isImgCode($(".loginArea .imgCode"),$(".loginArea .imgCode").siblings(".errorTips"))){
            isOk = false;
        }
        if(isOk){
            this._getKey();
            $.when(window.GLOBAL.promise["getKey"]).done(function(r){
                if(r.key){
                    setMaxDigits(130);
                    let key = new RSAKeyPair("10001","",r.key);
                    let parms = {
                        userName:encryptedString(key,encodeURIComponent(this.state.userName)),
                        passWord:encryptedString(key,encodeURIComponent(this.state.passWord)),
                        imgCode:this.state.imgCode,
                        isSave:this.state.isSave?"1":"",
                        token:window.GLOBAL.token,
                        key1:MD5(this.state.userName+this.state.passWord+this.state.imgCode+"devPortal")
                    };
                    window.GLOBAL.ajaxMap.setParms("login",parms);
                    let ajax = window.GLOBAL.ajaxMap.getAjaxParms("login");
                    ajax.success=function(r){
                        if(r.code == 2){
                            browserHistory.replace(window.GLOBAL.pageData.login.goToWorkBench)
                        }else if(r.code == 1){
                            this.setState({imgCode:""});
                            $(".loginArea .imgCode").siblings(".errorTips").html("* 验证码错误");
                        }else{
                            this.setState({userName:"",passWord:""});
                            $(".loginArea .userName").siblings(".errorTips").html("* 用户名或密码错误");
                            $(".loginArea .passWord").siblings(".errorTips").html("* 用户名或密码错误");
                        }
                    }.bind(this);
                    $.ajax(ajax);
                }
            }.bind(this))
        }
    }
    render() {
        let login = window.GLOBAL.pageData.login;
        let checkTips = window.GLOBAL.pageData.checkTips;
        return (
            <div className="pageLevel1 loginBg">
                <div className="header">
                    <Link to="">
                        <div className="icon-logo logo fl"></div>
                        <div className="logoName fl">中国移动杭州研发中心 | 贯众开放平台</div>
                    </Link>
                </div>
                <div className="pageLevel2" style={{"minHeight":($(window).height()-130)+"px"}}>
                    <div className="loginArea">
                        <div className="line fl"></div>
                        <div className="m1 fl">登录</div>
                        <div className="line fl"></div>
                        <div className="inputLine fl">
                            <input style={{width:"100%"}} type="text" placeholder={login.userName.placeholder} value={this.state.userName} className="userName" onChange={this.setUserName} onBlur={this.checkUserName}/>
                            <div className="errorTips"></div>
                        </div>
                        <div className="inputLine fl">
                            <input style={{width:"100%"}} type="password" placeholder={login.passWord.placeholder} value={this.state.passWord} className="passWord" onChange={this.setPassWord} onBlur={this.checkPassWord}/>
                            <div className="errorTips"></div>
                        </div>
                        <div className="inputLine fl">
                            <input style={{width:"158px"}} type="text" className="fl imgCode" placeholder={checkTips.imgCode.placeholder} value={this.state.imgCode} onChange={this.setImgCode} onBlur={this.checkImgCode} maxLength={checkTips.imgCode.maxLength}/>
                            <a onClick={this.reloadImg}>
                                <img className="imgCodeShow fr" src={this.state.imgUrl} width="80" height="40"/>
                                <div className="imgCodeBtn">
                                    <div className="icon-refresh"></div>
                                </div>
                            </a>
                            <div className="errorTips"></div>
                        </div>
                        <div className="checkBoxLine fl">
                            <CheckBox name="isSave" s={this.state.s} onClick={this.handleClick}/>
                            <span>记住密码</span>
                        </div>
                        <Button block bsStyle="primary" className="fl" onClick={this.submit}>登录</Button>
                        <div className="otherLink fl">
                            <Link className="link" to="#">免费注册</Link> | <Link className="link" to="#">忘记密码</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
login.propTypes = {
    isSave: React.PropTypes.bool.isRequired
};
let saveDate = {isSave:false};
if(localStorage&&localStorage["isSave"]){
    saveDate.isSave = true;
}
login.defaultProps = {
    isSave:saveDate.isSave
};
module.exports = login;