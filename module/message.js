/**
 * Created by hqer on 2016/12/14.
 */
import React from 'react'
import Reflux from 'reflux'
Reflux.defineReact(React);

import { findDOMNode } from 'react-dom'
import { Link } from 'react-router'

import { Button,Table } from 'react-bootstrap'
import CheckBox from '../componentUI/js/checkBox'
import PageArea from '../componentUI/js/pageArea'
import Tips from '../componentUI/js/tips'

import '../store/message'

class ListTd extends React.Component{
    constructor(props){
        super(props);
        this.state = {s:false};
        this.handleClick = this.handleClick.bind(this);
        this.setMessageInfo = this.setMessageInfo.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.isInverse===2&&this.props.isInverse!=2){
            this.setState({s:false});
        }else if(newProps.isInverse!=2&&newProps.isInverse!=this.props.isInverse&&newProps.showKeyName === "delCheck"){
            this.handleClick();
        }else if(newProps.data!=this.props.data){
            this.setState({s:false});
        }
    }
    handleClick(){
        if(this.state.s){
            window.GLOBAL.action.message.del(this.props.data);
            this.setState({s:false});
        }else{
            window.GLOBAL.action.message.add(this.props.data);
            this.setState({s:true});
        }
    }
    setMessageInfo(){
        window.GLOBAL.action.message.saveInfo();
    }
    render(){
        let dom;
        if(this.props.showKeyName === "title"){
            dom = (
                <Link title={this.props.data.text} to={"/messageDetail"+this.props.data.url} onClick={this.setMessageInfo}>
                    <div className={this.props.isRead?"icon-unRead":"icon-readed"}></div>
                    <div className="ShowTxt">{this.props.data.text}</div>
                </Link>
            );
        }else if(this.props.showKeyName === "delCheck"){
            dom =(<CheckBox name="delCheck" s={this.state.s} v={this.state.s?this.props.data:""} onClick={this.handleClick}/>);
        }else{
            dom = (<div className="ShowTxt">{this.props.data}</div>);
        }

        return(
            <td className={this.props.className}>
                {dom}
            </td>
        )
    }
}
class ListTr extends React.Component{
    render(){
        return(
            <tr className={this.props.className}>
                {this.props.showKey.map((item,i) =>(
                    <ListTd data={this.props.data[item]} showKeyName={item} className={"th"+i} key={i} isInverse={this.props.isInverse} isRead={this.props.data['isRead']}></ListTd>
                ))}
            </tr>
        )
    }
}
class DelMessage extends React.Component{
    constructor(props){
        super(props);
        this.state = {s:0};
        this.Inverse = this.Inverse.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.data!=this.props.data){
            this.setState({s:2});
        }
    }
    Inverse(){
        if(this.state.s === 1){
            this.setState({s:0});
        }else{
            this.setState({s:1});
        }
    }
    render() {
        let data = [];
        if(this.props.data.length){
            this.props.data.map((element,index) => {
                data.push(<ListTr data={element} key={index} showKey={this.props.showKey} className={index%2===0?"bgGray":""} isInverse={this.state.s}></ListTr>)
            })
        }else{
            data.push(<tr key={0}><td className="noData" colSpan={this.props.title.length}>{this.props.noDataTips}</td></tr>);
        }
        let title=[];
        let state = false;
        if(this.state.s===1){
            state = true;
        }
        {this.props.title.map((item,i) => {
            if(item === "delAllCheck"){
                title.push(<th className={"th"+i} key={i}><div className="ShowTxt"><CheckBox name="delAllCheck" s={state} onClick={this.Inverse}/></div></th>);
            }else{
                title.push(<th className={"th"+i} key={i}><div className="ShowTxt">{item}</div></th>);
            }
        })}
        return (
            <div className="delList">
                <Table striped hover responsive className="basicList">
                    <thead>
                    <tr>
                        {title}
                    </tr>
                    </thead>
                    <tbody>
                    {data}
                    </tbody>
                </Table>
            </div>
        )
    }
}
DelMessage.propTypes = {
    title: React.PropTypes.array.isRequired,
    showKey: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired,
    noDataTips: React.PropTypes.string.isRequired
};
DelMessage.defaultProps = {
    title:[],
    showKey:[],
    data:[],
    noDataTips:"暂无数据"
};

class Message extends Reflux.Component{
    constructor(props) {
        super(props);
        this.state = {
            data:this.props.data,
            showTips:false
        };
        this.store = window.GLOBAL.store.message;
        this.handleSelect = this.handleSelect.bind(this);
        this.handleDelAll = this.handleDelAll.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleShowTips = this.handleShowTips.bind(this);
    }
    componentDidMount(){
        window.GLOBAL.action.message.init();
        window.GLOBAL.ajaxMap.setParm("message",'pageNow',this.state.pageNow);
        window.GLOBAL.ajaxMap.setParm("message",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("message");
        parms.success=function(r){
            this.setState({totalPage:r.totalPage,data:r.data});
        }.bind(this);
        $.ajax(parms);
    }
    handleSelect(pageNow){
        window.GLOBAL.ajaxMap.setParm("message",'pageNow',pageNow);
        window.GLOBAL.ajaxMap.setParm("message",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("message");
        parms.success=function(r){
            this.setState({pageNow:pageNow,totalPage:r.totalPage,data:r.data,showTips:false,delId:[],showDel:false});
        }.bind(this);
        $.ajax(parms);
    }
    handleDelAll(){
        window.GLOBAL.ajaxMap.setParm("messageDel",'delId',JSON.stringify(this.state.delId));
        window.GLOBAL.ajaxMap.setParm("messageDel",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("messageDel");
        parms.success=function(r){
            if(r.code){
                window.GLOBAL.userInfo.get(function(){
                    this.handleSelect(this.state.pageNow);
                    let unRead = JSON.parse(sessionStorage['userInfo']).unRead;
                    window.GLOBAL.action.header.setUnRead(unRead);
                    window.GLOBAL.action.workBench.setUnRead(unRead);
                }.bind(this));
            }
        }.bind(this);
        $.ajax(parms);
    }
    handleCancel(){
        this.setState({showTips:false});
    }
    handleShowTips(){
        this.setState({showTips:true});
    }
    render() {
        let dom;
        let userInfo = JSON.parse(sessionStorage["userInfo"]);
        if(!this.props.children){
            let delBtn;
            if(this.state.showDel){
                delBtn = (<Button className="DelAllBtn" block onClick={this.handleShowTips}>批量删除</Button>);
            }
            let tips;
            if(this.state.showTips){
                tips = (<Tips onSubmit={this.handleDelAll} onCancel={this.handleCancel} text={"确认删除已选中的"+this.state.delId.length+"个站内信消息?"}/>)
            }
            dom =(<div className="myApps">
                <div className="MainTitle">
                    <h1>{window.GLOBAL.pageData.message.title}</h1>
                    <span>{userInfo.unRead}条未读消息</span>
                </div>
                <div className="whiteSpace">
                    <DelMessage
                        title={this.props.title}
                        data={this.state.data}
                        showKey={this.props.showKey}
                    />
                    <div>
                        {delBtn}
                        <PageArea
                            totalPage={this.state.totalPage}
                            maxPage={3} pageNow={this.state.pageNow}
                            onSelect={this.handleSelect}
                            onSearch={this.handleSelect}
                        />
                    </div>
                </div>
                {tips}
            </div>);
        }
        return (
            <div>
                {dom}
                {this.props.children}
            </div>
        )
    }
}
Message.propTypes = {
    title: React.PropTypes.array.isRequired,
    showKey: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired
};
Message.defaultProps = {
    title:window.GLOBAL.pageData.message.listTitle,
    showKey:window.GLOBAL.pageData.message.showKey,
    data:[]
};
module.exports = Message;