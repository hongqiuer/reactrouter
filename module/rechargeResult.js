/**
 * Created by hqer on 2017/2/8.
 */
import React from 'react'
import { Link } from 'react-router'
import { Button } from 'react-bootstrap'

class rechargeResult extends React.Component{
    render() {
        let recharge = window.GLOBAL.pageData.recharge;
        return (
            <div className="rechargeResult">
                <div className="MainTitle">
                    <h1>{recharge.title}</h1>
                </div>
                <div className="whiteSpace">
                    <ul className="bigCircleProgress">
                        <li className="bigCircle select">
                            <div className="text">输入充值金额</div>
                            <div className="num">1</div>
                        </li>
                        <li className="bigCircle select">
                            <div className="text">进行支付</div>
                            <div className="num">2</div>
                        </li>
                        <li className="bigCircle select">
                            <div className="text">{this.props.location.query.r==1?"充值成功":"充值失败"}</div>
                            <div className="num">3</div>
                        </li>
                    </ul>
                    {
                        this.props.location.query.r==1?(
                            <div className="resultPay">
                                <div className="center">
                                    <div className="icon-payResultSuccess"></div>
                                    您已经成功充值{this.props.location.query.m}元！
                                    <div className="text1">您可以进行以下操作:</div>
                                    <div className="text2">
                                        <p>进入 <Link className="link" to="/myAccount">我的账户</Link> 查看当前账户余额</p>
                                        <p>点击 <Link className="link" to="/recharge">充值&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Link> 继续为账户充值</p>
                                    </div>
                                </div>
                            </div>
                        ):(
                            <div className="resultPay">
                                <div className="center">
                                    <div className="icon-payResultFailure"></div>
                                    抱歉,您未完成支付！请稍后再试。
                                    <div className="text1">您可以进行以下操作:</div>
                                    <div className="text2">
                                        <p>进入 <Link className="link" to="/myAccount">我的账户</Link> 查看当前账户余额</p>
                                        <p>点击 <Link className="link" to="/recharge">充值&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Link> 继续为账户充值</p>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }
}
module.exports = rechargeResult;