/**
 * Created by hqer on 2017/1/16.
 */
import React from 'react'
import { Link,browserHistory } from 'react-router'
import { Button,Table,ButtonToolbar,DropdownButton,Dropdown,MenuItem} from 'react-bootstrap'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import moment from 'moment'
import ReactHighcharts from 'react-highcharts'
import PageArea from '../componentUI/js/pageArea'

class ListTd extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    render(){
        let dom;
        if(this.props.showKeyName==="btns"){
            dom = [];
            if(this.props.data.length){
                {this.props.data.map((item,i) => {
                    dom.push(<Link to={{ pathname: "/statisticsDetail/"+this.props.eaId, query:{ date: item.url } }} onClick={this.setMessageInfo} key={i}>{item.text}</Link>)
                })}
            }else{
                dom.push(<span key={0}>--</span>)
            }
        }else{
            dom = (<div className="ShowTxt">{this.props.data}</div>);
        }
        return(
            <td className={this.props.className}>
                {this.props.showKeyName==="btns"?(
                    this.props.data.length?(
                        this.props.data.map((item,i) => (
                            <Link className="green" to={{ pathname: "/statisticsDetail/"+this.props.eaId, query:{ date: item.url } }} onClick={this.setMessageInfo} key={i}>{item.text}</Link>
                        ))
                    ):(
                        <span key={0}>--</span>
                    )
                ):(
                    this.props.data
                )}
            </td>
        )
    }
}
class ListTr extends React.Component{
    render(){
        return(
            <tr className={this.props.className}>
                {this.props.showKey.map((item,i) => (
                    <ListTd data={this.props.data[item]} showKeyName={item} className={"th"+i} key={i} eaId={this.props.eaId}></ListTd>
                ))}
            </tr>
        )
    }
}
class ListBasic extends React.Component{
    render() {
        return (
            <Table striped hover responsive className="basicList">
                <thead>
                <tr>
                    {this.props.title.map((item,i) => (
                        <th className={"th"+i} key={i}>{item}</th>
                    ))}
                </tr>
                </thead>
                <tbody>
                {this.props.data.length?(
                    this.props.data.map((element,index) => (
                        <ListTr data={element} key={index} showKey={this.props.showKey} className={index%2===0?"bgGray":""} eaId={this.props.eaId}/>
                    ))
                ):(
                    <tr key={0}><td className="noData" colSpan={this.props.title.length}>{this.props.noDataTips}</td></tr>
                )}
                </tbody>
            </Table>
        )
    }
}
ListBasic.propTypes = {
    title: React.PropTypes.array.isRequired,
    showKey: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired,
    noDataTips: React.PropTypes.string.isRequired
};
ListBasic.defaultProps = {
    title:[],
    showKey:[],
    data:[],
    noDataTips:"暂无数据"
};


class statistics extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            apiKey:"",
            dateType:0,
            dateValue:"",
            showHighcharts:0,
            pageNow:this.props.pageNow,
            totalPage:this.props.totalPage,
            apiKeyList:[],
            countList:[],
            dataList:[],
            recordData:[],
            isSearch:false
        };
        this._getInfo = this._getInfo.bind(this);
        this._getRecord =this._getRecord.bind(this);
        this.selectSearchItem = this.selectSearchItem.bind(this);
        this.setDateType = this.setDateType.bind(this);
        this.handleEvent = this.handleEvent.bind(this);
        this.showHighcharts = this.showHighcharts.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.params.eaId!=this.props.params.eaId){
            let parms ={
                eaId:newProps.params.eaId,
                apiKey:"",
                dateType:0,
                dateValue:"",
                showHighcharts:0,
                isSearch:false
            };
            this._getInfo(parms);
            parms.pageNow = 1;
            this._getRecord(parms);
        }
    }
    componentDidUpdate(prevProps, prevState){
        let parms ={
            eaId:this.props.params.eaId,
            apiKey:"",
            dateType:0,
            dateValue:""
        };
        if(this.state.isSearch&&prevState.apiKey!=this.state.apiKey){
            parms.apiKey = this.state.apiKey;
            this._getInfo(parms);
            parms.pageNow = 1;
            this._getRecord(parms);
        }else if(this.state.isSearch&&prevState.dateType!=this.state.dateType&&this.state.dateType!=2){
            parms.dateType = this.state.dateType;
            this._getInfo(parms);
            parms.pageNow = 1;
            this._getRecord(parms);
        }else if(this.state.isSearch&&this.state.dateType===2&&prevState.dateValue!=this.state.dateValue){
            parms.dateType = this.state.dateType;
            parms.dateValue = this.state.dateValue;
            this._getInfo(parms);
            parms.pageNow = 1;
            this._getRecord(parms);
        }else if(this.state.isSearch&&prevState.pageNow!=this.state.pageNow){
            parms.pageNow = this.state.pageNow;
            this._getRecord(parms);
        }
    }
    componentDidMount(){
        let parms ={
            eaId:this.props.params.eaId,
            apiKey:this.state.apiKey,
            dateType:this.state.dateType,
            dateValue:this.state.dateValue
        }
        this._getInfo(parms);
        parms.pageNow = this.state.pageNow;
        this._getRecord(parms);
    }
    _getInfo(parms){
        parms.token = window.GLOBAL.token;
        window.GLOBAL.ajaxMap.setParms("getAbilityStatistics",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("getAbilityStatistics");
        ajax.success=function(r){
            if(r.code===0){
                browserHistory.replace('/statisticsNoData/'+parms.eaId);
            }else if(parms.apiKey===""&&r.apiKeyList.length){
                this.setState({
                    eaId:parms.eaId,
                    apiKey:r.apiKeyList[0].id,
                    dateType:parms.dateType,
                    dateValue:parms.dateValue,
                    apiKeyList:r.apiKeyList,
                    countList:r.countList,
                    dataList:r.dataList,
                    isSearch:false
                });
            }else{
                this.setState({
                    eaId:parms.eaId,
                    apiKey:parms.apiKey,
                    dateType:parms.dateType,
                    dateValue:parms.dateValue,
                    apiKeyList:r.apiKeyList,
                    countList:r.countList,
                    dataList:r.dataList,
                    isSearch:false
                });
            }
        }.bind(this);
        $.ajax(ajax);
    }
    _getRecord(parms){
        window.GLOBAL.ajaxMap.setParms("getAbilityRecord",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("getAbilityRecord");
        ajax.success=function(r){
            this.setState({
                pageNow:parms.pageNow,
                totalPage:r.totalPage,
                recordData:r.data,
                isSearch:false
            });
        }.bind(this);
        $.ajax(ajax);
    }
    selectSearchItem(eventKey){
        let _i = eventKey*1;
        if(this.state.apiKeyList[_i].id!=this.state.apiKey){
            this.setState({apiKey:this.state.apiKeyList[_i].id,isSearch:true})
        }
    }
    setDateType(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value*1;
        this.setState({dateType:_val,isSearch:true})
    }
    handleEvent(event, picker){
        if(event.type==="apply"){
            let FStartData = moment(picker.endDate).subtract(1,"days");
            let isBefore = moment(picker.startDate).isBefore(FStartData, 'day');
            if(isBefore){
                this.setState({
                    dateValue: picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'),
                    isSearch:true
                });
                $(".dateRangeBox").siblings(".errorTips").html("");
            }else{
                $(".dateRangeBox").siblings(".errorTips").html("* 查询最短时间不得少于3天!");
            }
        }
    }
    showHighcharts(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value*1;
        this.setState({showHighcharts:_val,isSearch:true})
    }
    handleSelect(pageNow){
        this.setState({pageNow:pageNow,isSearch:true});
    }
    render() {
        let statistics = window.GLOBAL.pageData.statistics;
        let dropDownValue="";
        this.state.apiKeyList.map((item,i) => {
            if(item.id===this.state.apiKey){
                dropDownValue = item.text;
            }
        });
        let dateRangePickerData ={
            dateLimit:{days:30},
            startDate:this.state.startDate ? moment(this.state.startDate) : moment().subtract(2,"days"),
            endDate:this.state.endDate ? moment(this.state.endDate) : moment(),
            maxDate:moment(),
            locale: window.GLOBAL.pageData.dateRangePickerLocale
        };
        let config = window.GLOBAL.pageData.highchartsConfig;
        if(this.state.dataList.length&&this.state.dataList[this.state.showHighcharts]){
            let _rotation = 0;
            if(this.state.dataList[this.state.showHighcharts].item.categories.length>7){
                _rotation = 45;
            }
            config.xAxis={
                categories: this.state.dataList[this.state.showHighcharts].item.categories,
                labels: {
                    step:1,
                    rotation:_rotation
                }
            };
            config.series = this.state.dataList[this.state.showHighcharts].item.series;
            config.tooltip = {
                shared: true,
                formatter:function(){
                    var _str = this.x + "<br>";
                    $.each(this.points, function(_i) {
                        _str = _str + this.series.name +": "+ this.y +" 条<br>";
                    });
                    return _str;
                }
            };
        }
        return (
            <div>
                {this.props.children?(this.props.children):(
                    <div className="statistics">
                        <div className="MainTitle">
                            <ButtonToolbar>
                            <h1>
                                {statistics.title}
                                {this.state.apiKeyList.length?(
                                    <Dropdown id="apikey">
                                        <Dropdown.Toggle>
                                            <span className="text" style={{minWidth:"140px"}}>{dropDownValue}</span>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            {this.state.apiKeyList.map((item,i) => (
                                                <MenuItem eventKey={i} key={i} onSelect={this.selectSearchItem}>{item.text}</MenuItem>
                                            ))}
                                        </Dropdown.Menu>
                                    </Dropdown>
                                ):""}
                            </h1>
                            </ButtonToolbar>
                        </div>
                        <div className="whiteSpace">
                            <ul className="fl radioList">
                                <li className="fl">
                                    <Button className={this.state.dateType===0?"circle active":"circle"} onClick={this.setDateType} value={0}></Button>
                                    <span>近七天</span>
                                </li>
                                <li className="fl">
                                    <Button className={this.state.dateType===1?"circle active":"circle"} onClick={this.setDateType} value={1}></Button>
                                    <span>近30天</span>
                                </li>
                                <li className="fl">
                                    <Button className={this.state.dateType===2?"circle active":"circle"} onClick={this.setDateType} value={2}></Button>
                                    <span>时间区间</span>
                                </li>
                            </ul>
                            <div className="fl dateRangeArea">
                                <DateRangePicker
                                    className="dateRangeBox fl"
                                    opens = "right"
                                    applyClass="btn-primary"
                                    dateLimit={dateRangePickerData.dateLimit}
                                    startDate={dateRangePickerData.startDate}
                                    endDate={dateRangePickerData.endDate}
                                    locale={dateRangePickerData.locale}
                                    maxDate={dateRangePickerData.maxDate}
                                    onEvent={this.handleEvent}>
                                    <input type="text" value={this.state.dateValue} readOnly/>
                                </DateRangePicker>
                                <span className="errorTips fl"></span>
                                <span className="underTips fl">最短查询时间不得少于3天,最长查询时间段不得超过30天</span>
                            </div>
                        </div>
                        <div className="whiteSpace">
                            <h1>总量数据</h1>
                            <div className="fl" style={{width:"100%",textAlign:"center"}}>
                                {this.state.countList.map((item,i) =>(
                                    <div className={"countArea type"+this.state.countList.length} style={{width:((100/this.state.countList.length)+"%")}} key={i}>
                                        <div className={statistics.logoType[item.iconType]}></div>
                                        <div className="lines">
                                            <div className="line">
                                                <a title={item.num}><span className="num">{item.num}</span></a>
                                                <span>{item.unit}</span>
                                            </div>
                                            <div className="line">
                                                {item.text}
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="whiteSpace">
                            <h1>图形数据</h1>
                            {this.state.dataList.length>1?(
                                <ul className="fl showHighchartsArea" style={{width:"100%"}}>
                                    {this.state.dataList.map((item,i) => (
                                        <li className={this.state.showHighcharts===i?"showHighcharts active fl":"showHighcharts fl"} onClick={this.showHighcharts} key={i} value={i}>{item.title}</li>
                                    ))}
                                </ul>
                            ):""}
                            <ReactHighcharts config={config}></ReactHighcharts>
                        </div>
                        <div className="whiteSpace">
                            <ListBasic title={statistics.listData[this.props.params.eaId].listTitle} showKey={statistics.listData[this.props.params.eaId].showKey} data={this.state.recordData} eaId={this.props.params.eaId}/>
                            <PageArea
                                totalPage={this.state.totalPage}
                                maxPage={3}
                                pageNow={this.state.pageNow}
                                onSelect={this.handleSelect}
                                onSearch={this.handleSelect}
                            />
                        </div>
                    </div>

                )}
            </div>
        )
    }
}
let statisticsPageDate = {pageNow:1, totalPage:10};
if(sessionStorage&&sessionStorage['statistics']){
    statisticsPageDate = JSON.parse(sessionStorage['statistics']);
    sessionStorage.removeItem('statistics');
}
statistics.propTypes = {
    pageNow: React.PropTypes.number.isRequired,
    totalPage: React.PropTypes.number.isRequired
};
statistics.defaultProps = {
    pageNow:statisticsPageDate.pageNow,
    totalPage:statisticsPageDate.totalPage
};
module.exports = statistics;