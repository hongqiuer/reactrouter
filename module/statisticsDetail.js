/**
 * Created by hqer on 2017/1/17.
 */
import React from 'react'
import { browserHistory } from 'react-router'
import { Table,Button,Row,Col } from 'react-bootstrap'
import PageArea from '../componentUI/js/pageArea'

class ListTd extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isShowList:false
        };
        this.showList=this.showList.bind(this);
    }
    showList(){
        if(this.state.isShowList){
            this.setState({isShowList:false});
        }else{
            this.setState({isShowList:true});
        }
    }
    render(){
        return(
            <td className={this.props.className}>
                {this.props.showKeyName==="list"?(
                    <div className="ShowTxt">
                        <div className="showLine">
                            {this.props.data.map((item,i) => (
                                <div style={{width:"100%",display:this.state.isShowList||i===0?"block":"none"}} key={i}>{item}</div>
                            ))}
                        </div>
                        <div style={{display:this.props.data.length>1?"block":"none"}} className={this.state.isShowList?"glyphicon glyphicon-chevron-up":"glyphicon glyphicon-chevron-down"} onClick={this.showList}></div>
                    </div>
                ):(
                    this.props.data
                )}
            </td>
        )
    }
}
class ListTr extends React.Component{
    render(){
        return(
            <tr className={this.props.className}>
                {this.props.showKey.map((item,i) => (
                    <ListTd data={this.props.data[item]} showKeyName={item} className={"th"+i} key={i}></ListTd>
                ))}
            </tr>
        )
    }
}
class statisticsDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            pageNow:1,
            totalPage:1
        };
        this.getInfo = this.getInfo.bind(this);
        this.goToBack = this.goToBack.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }
    componentDidMount(){
        this.getInfo(this.state.pageNow);
    }
    goToBack(){
        browserHistory.goBack();
    }
    getInfo(_p){
        let parms = {
            eaId:this.props.params.eaId,
            date:this.props.location.query.date,
            pageNow:_p,
            token:window.GLOBAL.token};
        window.GLOBAL.ajaxMap.setParms("getFailingDetail",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("getFailingDetail");
        ajax.success=function(r){
            this.setState({
                pageNow:_p,
                totalPage:r.totalPage,
                data:r.data
            });
        }.bind(this);
        $.ajax(ajax);
    }
    handleSelect(pageNow){
        this.getInfo(pageNow);
    }
    render() {
        let statistics = window.GLOBAL.pageData.statistics;
        let statisticsDetail = statistics.listData[this.props.params.eaId].detail;
        return (
            <div className="statisticsDetail">
                <div className="MainTitle">
                    <h1>{statistics.failingDetailTitle+"-"+this.props.location.query.date}</h1>
                    <Button className="backBtn" onClick={this.goToBack}>返回</Button>
                </div>
                <div className="whiteSpace">
                    <Table striped hover responsive className="basicList">
                        <thead>
                        <tr>
                            {statisticsDetail.title.map((item,i) => (
                                <th className={"th"+i} key={i}>{item}</th>
                            ))}
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.data.length?(
                            this.state.data.map((element,index) => (
                                <ListTr data={element} key={index} showKey={statisticsDetail.showKey} className={index%2===0?"bgGray":""}/>
                            ))
                        ):(
                            <tr><td className="noData" colSpan={statisticsDetail.title.length}>暂无数据</td></tr>
                        )}
                        </tbody>
                    </Table>
                    <PageArea
                        totalPage={this.state.totalPage}
                        maxPage={3}
                        pageNow={this.state.pageNow}
                        onSelect={this.handleSelect}
                        onSearch={this.handleSelect}
                    />
                </div>
            </div>
        )
    }
}
module.exports = statisticsDetail;