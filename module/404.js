/**
 * Created by hqer on 2016/12/12.
 */
import React from 'react'
class notFind extends React.Component{
    render() {
        return (
            <div>
                <h2>404</h2>
                {this.props.children}
            </div>
        )
    }
}
module.exports = notFind;