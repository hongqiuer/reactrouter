/**
 * Created by hqer on 2017/2/4.
 */
import React from 'react'
import { Link,browserHistory } from 'react-router'
import { Button } from 'react-bootstrap'
import MD5 from "md5"

class smsSignModify extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name:"",
            type:-1,
            text:""
        };
        this.getInfo = this.getInfo.bind(this);
        this.goToBack = this.goToBack.bind(this);
        this.submit = this.submit.bind(this);
        this.checkSignName = this.checkSignName.bind(this);
        this.checkSignText = this.checkSignText.bind(this);
        this.setSignName = this.setSignName.bind(this);
        this.setSignText = this.setSignText.bind(this);
        this.setType = this.setType.bind(this);
    }
    componentWillMount(){
        if(!window.GLOBAL.promise["checkSignName"]){
            window.GLOBAL.promise["checkSignName"] = $.Deferred();
        }
    }
    componentWillUnmount(){
        if(window.GLOBAL.promise&&window.GLOBAL.promise["checkSignName"]){
            delete window.GLOBAL.promise["checkSignName"];
        }
    }
    componentDidMount(){
        this.getInfo(this.props.params.smsSignId);
    }
    getInfo(_id){
        window.GLOBAL.ajaxMap.setParm("smsSignatureDetail",'signatureId',_id);
        window.GLOBAL.ajaxMap.setParm("smsSignatureDetail",'token',window.GLOBAL.token);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("smsSignatureDetail");
        parms.success=function(r){
            this.setState({
                name:r.name,
                type:r.type,
                text:r.other
            });
        }.bind(this);
        $.ajax(parms);
    }
    goToBack(){
        browserHistory.goBack()
    }
    submit(){
        let isOk = true;
        if(this.state.type===2&&!window.GLOBAL.checkFun.isNotEmpty($(".smsSignAdd .smsSignText"),$(".smsSignAdd .smsSignText").siblings(".errorTips"))){
            isOk = false;
        }
        if(isOk){
            window.GLOBAL.promise["checkSignName"] = $.Deferred();
            this.checkSignName();
            $.when(window.GLOBAL.promise["checkSignName"]).done(function(r1){
                if(r1.code!=401){
                    let parms = {
                        name:this.state.name,
                        type:this.state.type,
                        des:this.state.text,
                        token:window.GLOBAL.token,
                        key1:MD5(this.state.name+this.state.type+this.state.text+window.GLOBAL.token+"devPortal")
                    };
                    window.GLOBAL.ajaxMap.setParms("addSignature",parms);
                    let ajax = window.GLOBAL.ajaxMap.getAjaxParms("addSignature");
                    ajax.success=function(r){
                        if(r.code){
                            browserHistory.push("/smsSign");
                        }
                    }.bind(this);
                    $.ajax(ajax);
                }
            }.bind(this))
        }
    }
    checkSignName(){
        let bool = window.GLOBAL.checkFun.isSignName($(".smsSignAdd .smsSignName"),$(".smsSignAdd .smsSignName").siblings(".errorTips"));
        if(bool){
            let parms = {signatureId:"",name:this.state.name};
            window.GLOBAL.ajaxMap.setParms("checkSignature",parms);
            let ajax = window.GLOBAL.ajaxMap.getAjaxParms("checkSignature");
            ajax.success=function(r){
                window.GLOBAL.promise["checkSignName"].resolve(r);
                if(r.code == 401){
                    $(".smsSignAdd .smsSignName").siblings(".errorTips").html("* 签名名称不唯一");
                }
            }.bind(this);
            $.ajax(ajax);
        }
    }
    checkSignText(){
        window.GLOBAL.checkFun.isNotEmpty($(".smsSignAdd .smsSignText"),$(".smsSignAdd .smsSignText").parent().siblings(".errorTips"))
    }
    setSignName(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({name:_val});
    }
    setSignText(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({text:_val});
    }
    setType(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value*1;
        this.setState({type:_val})
    }
    render() {
        let smsSignAdd = window.GLOBAL.pageData.smsSign.smsSignAdd;
        return (
            <div className="smsSignAdd">
                <div className="MainTitle">
                    <h1>{smsSignAdd.title}</h1>
                    <Button className="backBtn" onClick={this.goToBack}>返回</Button>
                </div>
                <div className="whiteSpace">
                    <div className="inputLine">
                        <span className="block fl">签名：</span>
                        <input className="smsSignName fl" type="text" maxLength={smsSignAdd.name.maxLength} onChange={this.setSignName} value={this.state.name} onBlur={this.checkSignName}/>
                        <span className="errorTips fl"></span>
                        <div className="fl underTips">* 建议使用公司或产品名称,长度为2-8个字符</div>
                        <div className="fl underTips">* 支持汉字\数字\英文,不能为纯数字\数字英文组合,长度为2-8个字符</div>
                    </div>
                    <div className="inputLine">
                        <span className="block fl">应用用途：</span>
                        <ul className="fl radioList">
                            <li className="fl">
                                <Button className={this.state.type===0?"circle active":"circle"} onClick={this.setType} value={0}></Button>
                                <span>自有产品或业务名</span>
                            </li>
                            <li className="fl">
                                <Button className={this.state.type===1?"circle active":"circle"} onClick={this.setType} value={1}></Button>
                                <span>他人产品或业务名</span>
                            </li>
                            <li className="fl">
                                <Button className={this.state.type===2?"circle active":"circle"} onClick={this.setType} value={2}></Button>
                                <span>其它</span>
                            </li>
                        </ul>
                        <div className="textArea fl" style={{display:this.state.type===2?"block":"none"}} >
                            <div className="heightAuto">{this.state.text}</div>
                            <textarea className="smsSignText fl" maxLength={smsSignAdd.text.maxLength} onChange={this.setSignText} onBlur={this.checkSignText} value={this.state.text} placeholder={smsSignAdd.text.placeholder}/>
                        </div>
                        {this.state.type===2?(<span className="errorTips fl"></span>):""}
                    </div>
                    <br/>
                    <Button className="submit" bsStyle="primary" onClick={this.submit}>提交</Button>
                </div>
            </div>
        )
    }
}
module.exports = smsSignModify;