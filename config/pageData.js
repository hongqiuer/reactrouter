/**
 * Created by hqer on 16/12/7.
 */
module.exports = {
    footer:{
        gsjj:{
            text:"公司简介",
            url:"/About"
        },
        lxwm:{
            text:"联系我们",
            url:"/ContactUs"
        },
        ghs:{
            text:"浙公网安备33011002010472号",
            url:"/ghs",
            img:"/img/ghs.png"
        },
        txbq:{
            text:"杭研开平签[2015]4号 中国移动通信版权所有"
        }
    },
    header:{
        logo:{
            text:"中国移动杭州研发中心 | 贯众开放平台"
        },
        devDoc:{
            text:"开发者文档",
            child:[
                {text:"开发者指南",url:"/companion"},
                {text:"REST API",url:"/restApi"},
                {text:"SDK下载",url:"/sdk"},
                {text:"服务协议",url:"/agreement"},
                {text:"常见问题",url:"/FQA"}
            ]
        },
        ability:{
            text:"能力服务",
            child:[
                {text:"多方语音",url:"/manyVoice"},
                {text:"消息通知",url:"/pushSms"}
            ]
        },
        myServer:{
            text:"我的工作台",
            url:"/myServer"
        },
        exitUrl:"/",
        unLoginBtns:[
            {
                text:"注册",
                url:"/registration"
            },
            {
                text:"登入",
                url:"/Login"
            }
        ]
    },
    leftMenu:{
        item0: {
            text:"能力管理",
            child:[
                {key:"myServer", text:"我的工作台", url:"/myServer"},
                {key:"myApps", text:"我的应用", url:"/myApps",ischeck:true},
                {key:"testPhone", text:"测试号码", url:"/testPhone",ischeck:true},
                {key:"smsSign", text:"短信签名", url:"/smsSign",ischeck:true},
                {key:"smsTemplate", text:"短信模板", url:"/smsTemplate",ischeck:true},
                {key:"voiceTemplate", text:"语音模板", url:"/voiceTemplate",ischeck:true}
            ]
        },
        item1:{
            text:"能力统计",
            child:[
                {key:"smsTemplateStatistics", text:"模板短信", url:"/statistics/001"},
                {key:"smsVerificationStatistics",text:"短信验证码",url:"/statistics/002"},
                {key:"voiceVerificationStatistics",text:"语音验证码",url:"/statistics/003"},
                {key:"voiceNoticeStatistics",text:"语音通知",url:"/statistics/004"},
                {key:"fullDuplexSpeechStatistics",text:"双向语音呼叫",url:"/statistics/005"},
                {key:"manyVoiceNoticeStatistics",text:"双向语音呼叫",url:"/statistics/006"}
            ]
        },
        item2:{
            text:"账户管理",
            child:[
                {key:"myAccount", text:"我的账户", url:"/myAccount"},
                {key:"recharge", text:"充值", url:"/recharge"},
                {key:"orderManage", text:"订单管理", url:"/orderManage",isShow:true}
            ]
        },
        item3:{
            text:"用户中心",
            child:[
                {key:"accountManage", text:"账号管理", url:"/accountManage"},
                {key:"changePWD", text:"密码更改", url:"/changePWD"},
                {key:"certification", text:"认证信息", url:"/certification"},
                {key:"message", text:"站内信息", url:"/message",isShow:true}
            ]
        }
    },
    checkTips:{
        img:"/img/tm.png",
        phone:{
            maxLength:11,
            placeholder:"请输入真实的手机号码"
        },
        imgCode:{
            maxLength:4,
            placeholder:"请输入图形验证码"
        },
        smsCode:{
            maxLength:6,
            placeholder:"请输入您获取到的验证码"
        }
    },
    login:{
        bgImg:"/img/loginBg.png",
        userName:{
            placeholder:"用户名/邮箱/手机号码"
        },
        passWord:{
            placeholder:"密码"
        },
        goToWorkBench:"/myServer"
    },
    orderNew:{
        listTitle:['','套餐名称','套餐规格','套餐单价','数量'],
        showKey:['packageId','name','norms','price','num'],
        numInput:{
            maxLength:3,
            maxValue:999
        },
        tips:{className:"center",text:"请选择套餐!",btns:[{text:"确定",type:0}]}
    },
    orderToPay:{
        listTitle:['套餐名称','套餐规格','套餐单价','数量'],
        showKey:['name','norms','price','num'],
        tips:{className:"center",text:"请选择支付方式!",btns:[{text:"确定",type:0}]}
    },
    myServer:{
        title:"统计数据",
        img:"/img/user.gif",
        logoType:[
            "icon-user_money icon",
            "icon-user_message icon",
            "icon-user_call icon",
            "icon-user_message icon",
            "icon-user_message icon",
            "icon-user_message icon"
        ],
        iconText:[
            "",
            "正在试用",
            "开通成功",
            "冻结"
        ],
        level:[
            "精品服务",
            "特色服务"
        ],
        Unautherized:{
            className:"left",
            text:"您还未完成开发者身份认证,认证后方可订购收费能力,是否进行认证审核?",
            btns:[{text:"去认证",type:1},{text:"取消",type:0}]
        },
        firstLogin:{
            className:"left",
            title:"欢迎登录贯众开放平台!",
            text:"平台用户信息认证审核通过后,方可正式开通能力,是否立即开始认证?",
            btns:[{text:"去认证",type:1},{text:"取消",type:0}]
        }
    },
    myApps:{
        title:"我的应用",
        listTitle:['delAllCheck','应用名称','状态','创建时间','调用参数','操作'],
        showKey:['delCheck','title','status','date','parms','btns'],
        hid:"··················",
        reSecretKey:{className:"center",text:"点击重置按钮后,Secret Key会重新生成."},
        delAll:{className:"left",text:"您正在进行应用删除操作.删除后应用下的API key和secret key将不能继续调用能力."},
        detailTitle:"应用详情",
        myAppsModify:{
            title:"修改应用",
            name:{
                maxLength:15
            },
            text:{
                maxLength:256
            }
        },
        status:["有效", "冻结", "注销"],
        myAppsAdd:{
            title:"创建应用",
            name:{
                maxLength:15
            },
            text:{
                maxLength:256
            }
        }
    },
    testPhone:{
        title:"测试号码配置",
        maxList:10,
        maxLength:11,
        delPhone:{className:"center",text:"确认删除该测试号码?"},
        unAdd:{className:"center",text:"最多只能添加10个测试号码!",btns:[{text:"确认",type:0}]},
        checkTips:{
            title:"添加号码",
            btns:[{text:"新增",type:1},{text:"取消",type:0}]
        }
    },
    smsSign:{
        title:"短信签名",
        listTitle:['delAllCheck','签名名称','签名状态','签名用途','申请时间','操作'],
        showKey:['delCheck','name','status','des','time','btns'],
        showNem:{className:"left",text:"您的短信签名数量已达到上限,请先删除旧签名再进行添加.",btns:[{text:"确定",type:0}]},
        delSign:{className:"center",text:"确认删除该模板?"},
        smsSignAdd:{
            title:"新建签名",
            name:{
                maxLength:8
            },
            text:{
                maxLength:25,
                placeholder:"'其他'是指代理他人产品或业务名,不超过25个字符"
            }
        },
        smsSignModify:{
            title:"修改签名",
            name:{
                maxLength:8
            },
            text:{
                maxLength:25,
                placeholder:"'其他'是指代理他人产品或业务名,不超过25个字符"
            }
        }
    },
    smsTemplate:{
        title:"短信模板配置",
        listTitle:['delAllCheck','模板名称','模板ID','模板类型','状态','最近更新时间','操作'],
        showKey:['delCheck','name','appId','templateType','status','time','btns'],
        delTemplate:{className:"center",text:"确认删除该模板?"},
        cancelTemplate:{className:"center",text:"该模板正在等待管理员审核,确认撤回吗?"},
        searchItem:[{text:"模板名称",value:0},{text:"模板ID",value:1}],
        type:[{text:"短信通知",value:0},{text:"短信验证码",value:1}],
        status:[{text:"待提交",value:0},{text:"审核中",value:1},{text:"审核通过",value:2},{text:"审核不通过",value:3}],
        smsTemplateDetail:{
            title:"短信模板详情"
        },
        smsTemplateAdd:{
            title:"新建短信模板",
            name:{
                maxLength:15
            },
            content:{
                maxLength:[512,50],
                parms:[6,1],
                underTips:[
                    "* 请按照示例编写模板内容,模板参数用'{}'分隔标注,单条模板短信模板最多512个字符,最多包含6个参数.<br/>示例:尊敬的{param1}用户,您的验证码为{param2},本验证码{param3}分钟有效,感谢您的使用!",
                    "* 请按照示例编写模板内容,模板参数用'{}'分隔标注,单条短信验证码最多50个字符,最多包含1个参数.<br/>* 短信验证码有效时间为10分钟,建议在短信验证码模板内容添加内容:'有效时间为10分钟.'<br/>示例:尊敬的用户,您的验证码为{code},有效时间为10分钟!"
                ]
            },
            parmsType:[{text:"string",value:"string"},{text:"int",value:"int"}],
            parmsLength:{
                maxLength:2,
                maxValue:20
            },
            parmsDes:{
                maxLength:150
            },
            scene:{
                maxLength:300
            },
            tips:[
                {className:"left",text:"参数个数必须和模板内容中参数个数保持一致.",btns:[{text:"确定",type:0}]},
                {className:"left",text:"您提交的短信内容包含敏感词,提交失败,请修改短信内容后,重新提交!",btns:[{text:"确定",type:0}]},
                {className:"left",text:"您已提交短信模板,管理员正在审核中,请耐心等待!",btns:[{text:"确定",type:1}]}
            ]
        },
        smsTemplateModify:{
            title:"修改短信模板"
        }
    },
    voiceTemplate:{
        title:"语音模板配置",
        listTitle:['delAllCheck','模板名称','模板ID','状态','最近更新时间','操作'],
        showKey:['delCheck','name','appId','status','time','btns'],
        delTemplate:{className:"center",text:"确认删除该模板?"},
        cancelTemplate:{className:"center",text:"该模板正在等待管理员审核,确认撤回吗?"},
        searchItem:[{text:"模板名称",value:0},{text:"模板ID",value:1}],
        status:[{text:"待提交",value:0},{text:"审核中",value:1},{text:"审核通过",value:2},{text:"审核不通过",value:3}],
        voiceTemplateDetail:{
            title:"语音模板详情"
        },
        voiceTemplateAdd:{
            title:"新建语音模板",
            name:{
                maxLength:15
            },
            content:{
                maxLength:200,
                parms:10,
                underTips:"* 请按照示例编写模板内容,模板参数用'{}'分隔标注,单条语音通知模板最多200个字符,最多包含10个参数.<br/>示例:尊敬的{param1}用户,您的验证码为{param2},本验证码{param3}分钟有效,感谢您的使用!"
            },
            parmsType:[{text:"string",value:"string"},{text:"number",value:"number"},{text:"datetime",value:"datetime"},{text:"url",value:"url"},{text:"int",value:"int"}],
            parmsLength:{
                maxLength:3,
                maxValue:100
            },
            parmsDes:{
                maxLength:150
            },
            scene:{
                maxLength:300
            },
            tips:[
                {className:"left",text:"参数个数必须和模板内容中参数个数保持一致.",btns:[{text:"确定",type:0}]},
                {className:"left",text:"您已提交单向语音模板,管理员正在审核中,请耐心等待!",btns:[{text:"确定",type:1}]}
            ]
        },
        smsTemplateModify:{
            title:"修改语音模板"
        }
    },
    statistics:{
        title:"统计数据",
        failingDetailTitle:"失败详情",
        logoType:[
            "icon-user_money icon",
            "icon-user_message icon",
            "icon-user_call icon",
            "icon-user_message icon",
            "icon-user_message icon",
            "icon-user_message icon"
        ],
        listData:{
            "001":{
                listTitle:["时间","发送总量(条)","发送成功量(条)","操作"],
                showKey:["date","item0","item1","btns"],
                detail:{
                    title:["发送时间","接收号码","模板ID","失败原因"],
                    showKey:["time","item0","item1","des"]
                }
            },
            "002":{
                listTitle:["时间","发送总量(条)","发送成功量(条)","操作"],
                showKey:["date","item0","item1","btns"],
                detail:{
                    title:["发送时间","接收号码","模板ID","失败原因"],
                    showKey:["time","item0","item1","des"]
                }
            },
            "003":{
                listTitle:["时间","发送总量(条)","发送成功量(条)","操作"],
                showKey:["date","item0","item1","btns"],
                detail:{
                    title:["发送时间","接收号码","模板ID","失败原因"],
                    showKey:["time","item0","item1","des"]
                }
            },
            "004":{
                listTitle:["时间","拨打总量(条)","接通总量(条)","通话时长","操作"],
                showKey:["date","item0","item1","item2","btns"],
                detail:{
                    title:["发送时间","接收号码","模板ID","失败原因"],
                    showKey:["time","item0","item1","des"]
                }
            },
            "005":{
                listTitle:["时间","拨打总量(条)","接通总量(条)","通话时长","操作"],
                showKey:["date","item0","item1","item2","btns"],
                detail:{
                    title:["发起时间","主叫号码","接收号码","失败原因"],
                    showKey:["time","item0","item1","des"]
                }
            },
            "006":{
                listTitle:["时间","拨打总量(条)","接通总量(条)","通话时长","操作"],
                showKey:["date","item0","item1","item2","btns"],
                detail:{
                    title:["发起时间","参会号码","失败原因"],
                    showKey:["time","list","des"]
                }
            }
        }
    },
    myAccount:{
        title:"我的账户",
        record:{
            series: [{
                name: "消费总金额额",
                data:[
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0},
                    {y: 0, zExceedConsume: 0, zAccountConsume: 0, zOrderConsume: 0}
                ]
            }],
            categories: ["00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00","00-00"]
        },
        downloadUrl:"#",
        horizon:["全部"],
        status:[{text:"全部",value:0},{text:"成功",value:1},{text:"进行中",value:2},{text:"失败",value:3},{text:"关闭",value:4}],
        paymentType:[{text:"全部",value:0},{text:"账户余额",value:1},{text:"第三方支付",value:2}],
        listTitle:['交易编号','交易时间','消费明细','交易方式','账户金额','交易状态'],
        showKey:['paymentSn','tradeTime','operateType','paymentType','money','status']
    },
    recharge:{
        title:"充值",
        status:[30, 50, 100, 200, 300, 400, 500],
        moneyInput:{
            maxLength:9,
            maxValue:100000000,
            placeholder:"请输入整数金额"
        },
        success:{},
        failed:{}
    },
    orderManage:{
        title:"订单管理",
        status:[{text:"有效",value:0},{text:"待支付",value:1},{text:"即将失效",value:2},{text:"失效",value:3},{text:"冻结",value:4}],
        listTitle:['套餐名称','套餐规格','单价(元)','数量(个)','实际消费(元)','所属能力','操作'],
        showKey:['name','norms','price','num','cost','ability','btns'],
        delOrderItem:{className:"center",text:"确认该订单从列表中删除吗?"},
        orderItemDetail:{
            title:"订单详情",
            listTitle:['套餐名称','套餐规格','单价(元)','数量(个)','所属能力','计费类型','计费方式'],
            showKey:['name','norms','price','num','ability','type','way']
        }
    },
    accountManage:{
        title:"账号信息",
        loginEmail:"系统已向您填写的邮箱发送邮件,登录修改后的邮箱,点击邮箱内激活链接,即可成功修改邮箱地址!",
        reLoadTips:"网络异常,请勿重复提交!",
        checkTips:{
            title:"修改绑定手机号(用于接收平台的相关通知)",
            btns:[{text:"修改",type:1},{text:"取消",type:0}]
        }
    },
    changePWD:{
        title:"更改密码",
        success:{className:"center",text:"恭喜您,您的密码修改成功!",btns:[{text:"确定",type:0}]},
        failed:{className:"center",text:"修改密码失败!",btns:[{text:"确定",type:0}]},
        oldPWD:{
            maxLength:20
        },
        newPWD:{
            maxLength:20
        },
        reNewPWD:{
            maxLength:20
        }
    },
    certification:{
        title:"开发者信息认证",
        type:[
            {tips:"审核中",isSHowRadio:false,showType:1,btn:[{text:"撤销",value:"6_0"},{text:"撤销",value:"6_1"},{text:"撤销",value:"6_2"}]},
            {tips:"审核通过",isSHowRadio:false,showType:1,btn:[{text:"升级到公司或政府机关",value:"8_1"},{text:"修改管理员信息",value:"7_1"},{text:"修改管理员信息",value:"7_2"}]},
            {tips:"审核不通过",isSHowRadio:true,showType:0,btn:[{text:"确认",value:""},{text:"确认",value:""},{text:"确认",value:""}]},
            {tips:"审核中",isSHowRadio:false,showType:1,btn:["",{text:"撤销",value:"7_1"},{text:"撤销",value:"7_2"}]},
            {tips:"未认证",isSHowRadio:true,showType:0,btn:[{text:"确认",value:""},{text:"确认",value:""},{text:"确认",value:""}]},
            {tips:"审核不通过",isSHowRadio:false,showType:2,btn:["",{text:"确认",value:""},{text:"确认",value:"submit"}]},
            {tips:"待提交",isSHowRadio:true,showType:0,btn:[{text:"确认",value:""},{text:"确认",value:""},{text:"确认",value:""}]},
            {tips:"待提交",isSHowRadio:false,showType:2,btn:["",{text:"确认",value:""},{text:"确认",value:""}]},
            {tips:"待提交",isSHowRadio:true,showType:0,btn:["",{text:"确认",value:""},{text:"确认",value:""}]}
        ],
        caType:[{text:"身份证",value:"身份证"}],
        companyType:[
            {text:"国有企业",value:"国有企业"},
            {text:"民营企业",value:"民营企业"},
            {text:"私营企业",value:"私营企业"},
            {text:"外商独资",value:"外商独资"},
            {text:"中外合资",value:"中外合资"},
            {text:"股份制企业",value:"股份制企业"},
            {text:"其他",value:"其他"}
        ],
        trade:[
            {text:"软件业",value:"软件业"},
            {text:"文化娱乐业",value:"文化娱乐业"},
            {text:"制造业",value:"制造业"},
            {text:"服务业",value:"服务业"},
            {text:"金融业",value:"金融业"},
            {text:"其他",value:"其他"}
        ],
        scale:[
            {text:"1-50人",value:"1-50人"},
            {text:"50-100人",value:"50-100人"},
            {text:"100-200人",value:"100-200人"},
            {text:"200人以上",value:"200人以上"}
        ],
        name:{
            maxLength:6
        },
        companyName:{
            maxLength:50
        },
        address:{
            maxLength:1000,
            placeholder:"建议您如实填写详细公司地址,例如街道名称,门牌号码,楼层和房间号等信息."
        },
        license:{
            maxLength:18
        },
        region:{
            placeholder:"请选择省/市/区",
            level:"District"
        },
        idCardImgUpload:{
            baseUrl:"http://localhost:8888/upload.action",
            accept:"image/png,image/gif,image/jpg",
            maxSize:2*1048576,
            idCardImgUrl:""
        },
        licenseImgUpload:{
            baseUrl:"http://localhost:8888/upload.action",
            accept:"image/png,image/gif,image/jpg",
            maxSize:2*1048576,
            licenseImgUrl:""
        }
    },
    message:{
        title:"站内消息",
        listTitle:['delAllCheck','标题','发件人','时间'],
        showKey:['delCheck','title','sender','date'],
        img:"/img/user.gif"
    },
    dateRangePickerLocale:{
        format: 'YYYY-MM-DD',
        separator: " - ",
        applyLabel: "确定",
        cancelLabel: "取消",
        fromLabel: "起始时间",
        toLabel: "结束时间'",
        customRangeLabel: "自定义",
        weekLabel: "W",
        daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
        monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        firstDay: 1
    },
    highchartsConfig:{
        credits:{
            enabled:false
        },
        title: {
            text: '',
            x: -20
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            tickmarkPlacement:'on',
            lineColor: '#cccccc',
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            labels: {
                step:1,
                rotation:0
            }
        },
        yAxis: {
            lineWidth: 0,
            title: {
                text:""
            },
            min: 0
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            borderWidth: 0
        },
        series: [{
            data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4]
        }],
        colors: ['#ffac62', '#4ecb9f', '#90ed7d', '#f7a35c', '#8085e9',
            '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1']
    },
    FileUploadOptions:{

    }
};