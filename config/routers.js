/**
 * Created by hqer on 16/12/7.
 */
function checkLogin(nextState, replace){
    if(sessionStorage&&sessionStorage["userInfo"]){
        let userInfo = JSON.parse(sessionStorage["userInfo"]);
        if(userInfo&&userInfo.isLogin){
            replace('/');
        }
    }
}
function checkLogOut(nextState, replace){
    if(sessionStorage&&!sessionStorage["userInfo"]){
        replace('/Login');
    }
}
function checkPermission(nextState, replace){
    if(sessionStorage&&sessionStorage["userInfo"]){
        let userInfo = JSON.parse(sessionStorage["userInfo"]);
        if(userInfo&&userInfo.isLogin&&userInfo.permission){
            let r = false;
            for (let i of userInfo.permission) {
                let list = window.GLOBAL.permission[i];
                for(let j of list){
                    //console.log(nextState.location.pathname.substr(1).split("/")[0]+"---------"+j)
                    if(nextState.location.pathname.substr(1).split("/")[0]=== j){
                        //console.log(i + " is find");
                        r = true;
                        break;
                    }
                }
            }
            if(!r){
                replace('/404');
            }
        }
    }else{
        replace('/Login');
    }
}
export default {
    component: require('../module/footer'),
    childRoutes: [
        { path: '/Login',
            onEnter:checkLogin,
            getComponent: (nextState, cb) => {
                require.ensure([], (require) => {
                    cb(null, require('../module/login'))
                })
            }
        },
        { path: '/',
            component: require('../module/header'),
            indexRoute: {
                getComponent: (nextState, cb) => {
                    return require.ensure([], (require) => {
                        cb(null, require('../module/index'))
                    })
                }
            },
            childRoutes: [
                { path: '/index',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/index'))
                        })
                    }
                },
                { path: '/About',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/About'))
                        })
                    }
                },
                { path: '/companion',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/companion'))
                        })
                    }
                },
                { path: '/restApi',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/restApi'))
                        })
                    }
                },
                { path: '/sdk',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/sdk'))
                        })
                    }
                },
                { path: '/agreement',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/agreement'))
                        })
                    }
                },
                { path: '/FQA',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/FQA'))
                        })
                    }
                },
                { path: '/order',
                    onEnter:checkLogOut,
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/order'))
                        })
                    }
                },
                { path: '/orderNew',
                    onEnter:checkLogOut,
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/orderNew'))
                        })
                    }
                },
                { path: '/orderToPay/:orderId',
                    onEnter:checkLogOut,
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/orderToPay'))
                        })
                    }
                },
                { path: '/workbench',
                    onEnter:checkLogOut,
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/workbench'))
                        })
                    },
                    indexRoute: {
                        getComponent: (nextState, cb) => {
                            return require.ensure([], (require) => {
                                cb(null, require('../module/myServer'))
                            })
                        }
                    },
                    childRoutes: [
                        { path: '/myServer',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/myServer'))
                                })
                            }
                        },
                        { path: '/myApps',
                            onEnter:checkPermission,
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/myApps'))
                                })
                            },
                            childRoutes:[
                                { path: '/myAppsDetail/:appsId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/myAppsDetail'))
                                        })
                                    }
                                },
                                { path: '/myAppsAdd',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/myAppsAdd'))
                                        })
                                    }
                                },
                                { path: '/myAppsModify/:appsId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/myAppsModify'))
                                        })
                                    }
                                }
                            ]
                        },
                        { path: '/testPhone',
                            onEnter:checkPermission,
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/testPhone'))
                                })
                            }
                        },
                        { path: '/smsSign',
                            onEnter:checkPermission,
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/smsSign'))
                                })
                            },
                            childRoutes:[
                                { path: '/smsSignAdd',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/smsSignAdd'))
                                        })
                                    }
                                },
                                { path: '/smsSignModify/:smsSignId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/smsSignModify'))
                                        })
                                    }
                                }
                            ]
                        },
                        { path: '/smsTemplate',
                            onEnter:checkPermission,
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/smsTemplate'))
                                })
                            },
                            childRoutes:[
                                { path: '/smsTemplateDetail/:smsTemplateId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/smsTemplateDetail'))
                                        })
                                    }
                                },
                                { path: '/smsTemplateAdd',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/smsTemplateAdd'))
                                        })
                                    }
                                },
                                { path: '/smsTemplateModify/:smsTemplateId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/smsTemplateModify'))
                                        })
                                    }
                                }
                            ]
                        },
                        { path: '/voiceTemplate',
                            onEnter:checkPermission,
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/voiceTemplate'))
                                })
                            },
                            childRoutes:[
                                { path: '/voiceTemplateDetail/:voiceTemplateId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/voiceTemplateDetail'))
                                        })
                                    }
                                },
                                { path: '/voiceTemplateAdd',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/voiceTemplateAdd'))
                                        })
                                    }
                                },
                                { path: '/voiceTemplateModify/:voiceTemplateId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/voiceTemplateModify'))
                                        })
                                    }
                                }
                            ]
                        },
                        { path: '/statistics/:eaId',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/statistics'))
                                })
                            },
                            childRoutes:[
                                { path: '/statisticsDetail/:eaId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/statisticsDetail'))
                                        })
                                    }
                                },
                                { path: '/statisticsNoData/:eaId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/statisticsNoData'))
                                        })
                                    }
                                }
                            ]
                        },
                        { path: '/myAccount',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/myAccount'))
                                })
                            }
                        },
                        { path: '/recharge',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/recharge'))
                                })
                            },
                            childRoutes:[
                                { path: '/rechargeResult',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/rechargeResult'))
                                        })
                                    }
                                }
                            ]
                        },
                        { path: '/orderManage',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/orderManage'))
                                })
                            },
                            childRoutes:[
                                { path: '/orderItemDetail/:orderId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/orderItemDetail'))
                                        })
                                    }
                                }
                            ]
                        },
                        { path: '/accountManage',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/accountManage'))
                                })
                            }
                        },
                        { path: '/changePWD',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/changePWD'))
                                })
                            }
                        },
                        { path: '/certification',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/certification'))
                                })
                            }
                        },
                        { path: '/message',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/message'))
                                })
                            },
                            childRoutes:[
                                { path: '/messageDetail/:messageId',
                                    getComponent: (nextState, cb) => {
                                        require.ensure([], (require) => {
                                            cb(null, require('../module/messageDetail'))
                                        })
                                    }
                                }
                            ]

                        },
                        { path: '/errorPage',
                            getComponent: (nextState, cb) => {
                                require.ensure([], (require) => {
                                    cb(null, require('../module/errorPage'))
                                })
                            }
                        }
                    ]
                },
                { path: '*',
                    getComponent: (nextState, cb) => {
                        require.ensure([], (require) => {
                            cb(null, require('../module/404'))
                        })
                    }
                }
            ]
        }
    ]
}