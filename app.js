import React from 'react'
import { render } from 'react-dom'
import { browserHistory, Router } from 'react-router'
import { Alert } from 'react-bootstrap'

import "./config/global"

import routes from './config/routers'

$(document).ajaxComplete(function(event,xhr,options){
    //let sessionOut = xhr.getResponseHeader("sessionOut");
    //let token = xhr.getResponseHeader("token");
    //let uploadToken = xhr.getResponseHeader("uploadToken");
    //if(sessionOut&&sessionOut*1){
    //    window.location.reload();
    //}
    //if(token){
    //    window.GLOBAL.token = token;
    //}
    //if(uploadToken){
    //    window.GLOBAL.uploadToken = uploadToken;
    //}
    window.GLOBAL.token = Math.random();
    window.GLOBAL.uploadToken = Math.random();
});
if(!sessionStorage){
    render((
        <Alert bsStyle="danger">
            <strong>你的浏览器版本太低,请更新到最高版本!</strong>
        </Alert>
    ), document.getElementById('app'));
}else{
    window.GLOBAL.userInfo.get(function(){
        render((
            <Router
                history={browserHistory}
                routes={routes}
            />
        ), document.getElementById('app'));
    });
}
