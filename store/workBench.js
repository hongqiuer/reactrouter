/**
 * Created by hqer on 2016/12/15.
 */
import React from 'react'
import Reflux from 'reflux'

Reflux.defineReact(React);

import '../actions/workBench'

class workBenchStore extends Reflux.Store
{
    constructor() {
        super();
        this.listenables = window.GLOBAL.action.workBench;
        this.state = {
            unRead:0,
            unPayOrder:0
        };
    }
    onGetUnRead(){
        if(sessionStorage&&sessionStorage["userInfo"]){
            let r = JSON.parse(sessionStorage["userInfo"]);
            this.setState({unRead:r.unRead});
        }
    }
    onSetUnRead(_num){
        this.setState({unRead:_num});
    }
    onGetUnPayOrder(){
        window.GLOBAL.ajaxMap.setParm("getUnPayOrder",'r',Math.random());
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("getUnPayOrder");
        parms.success=function(r){
            this.setState({unPayOrder:r.num});
        }.bind(this);
        $.ajax(parms);
    }
    onSetUnPayOrder(_num){
        this.setState({unPayOrder:_num});
    }
}
window.GLOBAL.store.workBench = workBenchStore;