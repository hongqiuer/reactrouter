/**
 * Created by hqer on 2017/1/23.
 */
import React from 'react'
import Reflux from 'reflux'

Reflux.defineReact(React);

import "../actions/certification"

class certificationStore extends Reflux.Store
{
    constructor() {
        super();
        this.listenables = window.GLOBAL.action.certification;
        this.state = {
            devType:0,
            type:4,//type0表示提交审核中，1表示审核通过，2表示审核未通过，3表示修改审核中，4表示未认证，5表示修改审核不通过,
            name:"",
            region:"",
            caType:"",
            idCard:"",
            idCardImg:"",
            companyName:"",
            address:"",
            companyType:"",
            trade:"",
            scale:"",
            license:"",
            licenseImg:"",
            govName:""
        };
    }
    onInit(){
        window.GLOBAL.ajaxMap.setParm("getUserInfo","token",window.GLOBAL.token);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("getUserInfo");
        ajax.success=function(r){
            this.setState({
                type:r.type,
                devType:r.devType,
                name:r.name,
                region:r.region,
                caType:r.caType,
                idCard:r.idCard,
                idCardImg:r.idCardImg,
                companyName:r.companyName,
                address:r.address,
                companyType:r.companyType,
                trade:r.trade,
                scale:r.scale,
                license:r.license,
                licenseImg:r.licenseImg,
                govName:r.govName
            });
        }.bind(this);
        $.ajax(ajax);
    }
    onSetType(_type,_devType){
        if(typeof _type === "number" && typeof _devType === "number"){
            this.setState({type:_type,devType:_devType});
        }else if(typeof _type === "number"){
            this.setState({type:_type});
        }else if(typeof _devType === "number"){
            this.setState({devType:_devType});
        }
    }
}
window.GLOBAL.store.certification = certificationStore;