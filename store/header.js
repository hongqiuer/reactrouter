/**
 * Created by hqer on 2016/12/8.
 */
import React from 'react'
import Reflux from 'reflux'

Reflux.defineReact(React);

import "../actions/header"

class headerStore extends Reflux.Store
{
    constructor() {
        super();
        this.listenables = window.GLOBAL.action.header;
        this.state = {log:0,select:3,unRead:0,userName:"",userId:"",permission:[]};
    }
    onGetLoginState(){
        if(sessionStorage&&sessionStorage["userInfo"]){
            let r = JSON.parse(sessionStorage["userInfo"]);
            this.setState({log:1,userId:r.userId,userName:r.userName,unRead:r.unRead,permission:r.permission});
        }
    }
    onSetUnRead(_num){
        this.setState({unRead:_num});
    }
    onSelect(_i){
        this.setState({select:_i});
    }
    onLogOut() {
        window.GLOBAL.ajaxMap.setParm("exitLogin","userId",this.state.userId);
        let parms = window.GLOBAL.ajaxMap.getAjaxParms("exitLogin");
        parms.success=function(r){
            if(r.isExit){
                sessionStorage.removeItem("userInfo");
                window.location.href = window.GLOBAL.pageData.header.exitUrl;
            }
        }.bind(this);
        $.ajax(parms);
    }
}
window.GLOBAL.store.header = headerStore;