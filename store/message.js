/**
 * Created by hqer on 2016/12/29.
 */
import React from 'react'
import Reflux from 'reflux'

Reflux.defineReact(React);

import "../actions/message"

let MessagePageDate = {pageNow:1, totalPage:10};
if(sessionStorage&&sessionStorage['message']){
    MessagePageDate = JSON.parse(sessionStorage['message']);
    sessionStorage.removeItem('message');
}
class messageStore extends Reflux.Store
{
    constructor() {
        super();
        this.listenables = window.GLOBAL.action.message;
        this.state = {pageNow:MessagePageDate.pageNow, totalPage:MessagePageDate.totalPage,delId:[],showDel:false};
    }
    onInit(){
        this.setState({delId:[],showDel:false});
    }
    onDel(_id){
        let delId = this.state.delId;
        for (let i = 0; i < delId.length; i++) {
            if (delId[i] == _id){
                delId.splice(i, 1);
            }
        }
        if(delId.length){
            this.setState({delId:delId,showDel:true});
        }else{
            this.setState({delId:delId,showDel:false});
        }
    }
    onAdd(_id){
        let delId = this.state.delId;
        delId.push(_id);
        let delId1 = [...new Set(delId)];
        if(delId1.length){
            this.setState({delId:delId1,showDel:true});
        }else{
            this.setState({delId:delId1,showDel:false});
        }
    }
    onSaveInfo(){
        sessionStorage['message']=JSON.stringify({pageNow:this.state.pageNow,totalPage:this.state.totalPage});
    }
}
window.GLOBAL.store.message = messageStore;