/**
 * Created by hqer on 2017/1/19.
 */
import React from 'react'
import Reflux from 'reflux'

Reflux.defineReact(React);

import "../actions/orderManage"

let orderManagePageDate = {pageNow:1, totalPage:10};
if(sessionStorage&&sessionStorage['orderManage']){
    orderManagePageDate = JSON.parse(sessionStorage['orderManage']);
    sessionStorage.removeItem('orderManage');
}
class orderManageStore extends Reflux.Store
{
    constructor() {
        super();
        this.listenables = window.GLOBAL.action.orderManage;
        this.state = {
            pageNow:orderManagePageDate.pageNow,
            totalPage:orderManagePageDate.totalPage,
            delId:[],
            showTips:false,
            closeId:[]
        };
    }
    onInit(){
        console.log("init====="+JSON.stringify(this.state));
        this.setState({delId:[],showTips:false,closeItem:[]});
    }
    onSaveInfo(){
        sessionStorage['orderManage']=JSON.stringify({pageNow:this.state.pageNow,totalPage:this.state.totalPage});
    }
    onShowTips(_id){
        this.setState({delId:[_id],showTips:true});
    }
    onCloseItem(_id){
        this.setState({closeId:[_id]});
    }
}
window.GLOBAL.store.orderManage = orderManageStore;