var webpack = require('webpack');

module.exports = {
   entry: {
      vendors: ['react','react-dom','react-router','reflux','react-bootstrap','react-bootstrap-daterangepicker','moment','react-highcharts'],
      index:'./app.js'
   },

   output: {
      path:'./static',
      filename: '[name].js',
      chunkFilename: '[id].chunk.js',
      publicPath: '/static/'
   },

   devServer: {
      inline: true,
      port: 8888
   },

   module: {
      loaders: [ {
         test: /\.js?$/,
         exclude: /node_modules/,
         loader: 'babel',

         query: {
            presets: ['es2015', 'react']
         }
      },
         {
            test: /\.json$/,
            loader: 'json-loader'
         },
      {
          test: /\.css$/,loader: 'style-loader!css-loader'
      }]
   },
   plugins: [
      new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
/*      new webpack.optimize.UglifyJsPlugin({
         compress: {
            warnings: false
         }
      })*/
   ]
};
