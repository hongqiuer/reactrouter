/**
 * Created by hqer on 2017/1/23.
 */
import React from 'react'
import Reflux from 'reflux'
Reflux.defineReact(React);

window.GLOBAL.action.certification = Reflux.createActions(["init","setType"]);