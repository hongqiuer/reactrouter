/**
 * Created by hqer on 2016/12/29.
 */
import React from 'react'
import Reflux from 'reflux'

Reflux.defineReact(React);

window.GLOBAL.action.message = Reflux.createActions(["del","add","saveInfo","init"]);